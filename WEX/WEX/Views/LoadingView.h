//
//  LoadingView.h
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/27.
//  Copyright © 2017年 Ta Duong Ngoc. All rights reserved.
//

#import <UIKit/UIKit.h>

extern CGSize const SIZE_LOADING;

@interface LoadingView : UIView

- (instancetype)initWithFrame:(CGRect)frame;
- (void)setupConstraints;
- (void)loading;
- (void)cancel;
- (void)applicationDidEnterBackground;
- (void)applicationWillEnterForeground;

@end
