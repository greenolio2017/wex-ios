//
//  Const.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>

// Thread
#define GET_ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)


// Color
#define COLOR_RED [UIColor colorWithRed:220/255.0f green:0/255.0f blue:5/255.0f alpha:1]
#define COLOR_RED_ALPHA [UIColor colorWithRed:220/255.0f green:0/255.0f blue:5/255.0f alpha:0.5f]
#define COLOR_GREEN [UIColor colorWithRed:5.0/255.0f green:100/255.0f blue:0.0f alpha:1]
#define COLOR_GREEN_ALPHA [UIColor colorWithRed:5.0/255.0f green:100/255.0f blue:0.0f alpha:0.2f]
#define COLOR_BLUE [UIColor colorWithRed:25/255.0f green:172/255.0f blue:208/255.0f alpha:1]
#define COLOR_BLACK_COMMON [UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1]
#define COLOR_BLACK_COMMON_ALPHA [UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:0.5]
#define COLOR_WHITE [UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1]

#define IS_IPAD_DEVICE   ([(NSString *)[UIDevice currentDevice].model hasPrefix:@"iPad"])

extern double const TRADE_FEE;

// API URLs
extern NSString *const WEX_INFO_URL;
extern NSString *const WEX_TICKER_URL;
extern NSString *const WEX_DEPTH_URL;
extern NSString *const WEX_TRADES_URL;
extern NSString *const WEX_TAPI_URL;
extern NSString *const WEX_WEB_URL;
extern NSString *const NONCE;
extern NSString *const METHOD_TRADE;
extern NSString *const METHOD_GET_INFO;
extern NSString *const METHOD_ACTIVE_ORDERS;
extern NSString *const METHOD_CANCEL_ORDER;
extern NSString *const METHOD_CANCEL_ORDER_ALL;
extern NSString *const METHOD_TRADE_HISTORY;

// NOTIFICATION
extern NSString *const TOOGLE_LEFT_MENU;
extern NSString *const MARKETS_MANAGER_UPDATE_MARKETS;
extern NSString *const TICKER_MANAGER_UPDATE_TICKERS;
extern NSString *const DEPTH_MANAGER_UPDATE_DEPTH;
extern NSString *const PAIRS_CHANGED;
extern NSString *const GRAPH_MANAGER_UPDATE_DATA;
extern NSString *const TRADE_HISTORY_MANAGER_UPDATE_TRADES;
extern NSString *const TRADE_INFO_UPDATE;
//extern NSString *const PRESENT_TRADE_VC_FROM_TABLE_CELL;
extern NSString *const TRADE_ORDER_CREATED;
extern NSString *const TRADE_ORDER_CREATED_FROM_TRADE_VC;
extern NSString *const ORDER_ACTIVE_ORDERS;
extern NSString *const ORDER_CANCEL_ORDER;
extern NSString *const TRADE_HISTORY_UPDATE;
extern NSString *const NOTIFICATION_UPDATE_FAVORITE;

// Layouts
extern float const PAGE_MENU_HEIGHT;

// User defaults
extern NSString *const USERDEFAULT_FIRST_STARTED;
extern NSString *const USERDEFAULT_KEY_API_KEY;
extern NSString *const USERDEFAULT_KEY_SECRET_KEY;
extern NSString *const USERDEFAULT_KEY_PASSCODE;
extern NSString *const USERDEFAULT_KEY_PASSCODE_TYPE;
extern NSString *const USERDEFAULT_KEY_FAVORITE_LIST;

// iAP
extern NSString *const kIdentifierApple;
extern NSString *const kIdentifierBlackberry;
extern NSString *const kIdentifierOrange;
extern NSString *const kIdentifierPear;
extern NSString *const kIdentifierTomato;


// Currency pairs
//extern NSString *const PAIRS[36];
extern NSString *const DEFAULT_PAIR;


