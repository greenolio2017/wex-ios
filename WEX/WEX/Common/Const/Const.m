//
//  Const.m
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "Const.h"
#import <UIKit/UIKit.h>
#import "MarketsManager.h"

double const TRADE_FEE = 0.002;

// API URLs
NSString *const WEX_INFO_URL = @"https://wex.nz/api/3/info/";
NSString *const WEX_TICKER_URL = @"https://wex.nz/api/3/ticker/";
NSString *const WEX_DEPTH_URL = @"https://wex.nz/api/3/depth/";
NSString *const WEX_TRADES_URL = @"https://wex.nz/api/3/trades/";
NSString *const WEX_TAPI_URL = @"https://wex.nz/tapi";
NSString *const WEX_WEB_URL = @"https://wex.nz/exchange/";
NSString *const NONCE = @"Nonce";
NSString *const METHOD_TRADE = @"Trade";
NSString *const METHOD_GET_INFO = @"getInfo";
NSString *const METHOD_ACTIVE_ORDERS = @"ActiveOrders";
NSString *const METHOD_CANCEL_ORDER = @"CancelOrder";
NSString *const METHOD_CANCEL_ORDER_ALL = @"CancelOrders";
NSString *const METHOD_TRADE_HISTORY = @"Trades";

// NOTIFICATION
NSString *const TOOGLE_LEFT_MENU  = @"ToogleLeftMenu";
NSString *const MARKETS_MANAGER_UPDATE_MARKETS  = @"MarketManagerUpdateMarkets";
NSString *const TICKER_MANAGER_UPDATE_TICKERS  = @"TickerManagerUpdateTickers";
NSString *const GRAPH_MANAGER_UPDATE_DATA  = @"GraphManagerUpdateData";
NSString *const PAIRS_CHANGED  = @"PairChanged";
NSString *const DEPTH_MANAGER_UPDATE_DEPTH  = @"DepthManagerUpdateDepth";
NSString *const TRADE_HISTORY_MANAGER_UPDATE_TRADES  = @"TradesManagerUpdateTrades";
NSString *const TRADE_INFO_UPDATE  = @"TradeInfoUpdate";
//NSString *const PRESENT_TRADE_VC_FROM_TABLE_CELL = @"PresentTradeVCFromTableCell";
NSString *const TRADE_ORDER_CREATED  = @"TradeOrderCreated";
NSString *const TRADE_ORDER_CREATED_FROM_TRADE_VC  = @"TradeOrderCreatedFromTradeVC";
NSString *const ORDER_ACTIVE_ORDERS  = @"OrderActiveOrders";
NSString *const ORDER_CANCEL_ORDER  = @"OrderCancelOrder";
NSString *const TRADE_HISTORY_UPDATE  = @"TradeHistoryUpdate";

NSString *const NOTIFICATION_UPDATE_FAVORITE  = @"NotificationUpdateFavorite";

float const PAGE_MENU_HEIGHT = 30;

// UserDefault
NSString *const USERDEFAULT_FIRST_STARTED = @"UserDefaultFirstStared";
NSString *const USERDEFAULT_KEY_API_KEY = @"UserDefaultApiKey";
NSString *const USERDEFAULT_KEY_SECRET_KEY = @"UserDefaultSecretKey";
NSString *const USERDEFAULT_KEY_PASSCODE = @"UserDefaultPasscode";
NSString *const USERDEFAULT_KEY_PASSCODE_TYPE = @"UserDefaultPasscodeType";

NSString *const USERDEFAULT_KEY_FAVORITE_LIST = @"UserDefaultFavoriteList";

// iAP
NSString *const kIdentifierApple       = @"com.greenolio.app.ios.WEXTrade.iap.premiumversion";
NSString *const kIdentifierBlackberry  = @"de.tum.in.www1.sgdws13.BuyFruit.Blackberry";
NSString *const kIdentifierOrange      = @"de.tum.in.www1.sgdws13.BuyFruit.Orange";
NSString *const kIdentifierPear        = @"de.tum.in.www1.sgdws13.BuyFruit.Pear";
NSString *const kIdentifierTomato      = @"de.tum.in.www1.sgdws13.BuyFruit.Tomato";

// Currency pairs
NSString *const PAIRS[] = {
    @"btc_usd",    @"btc_rur",    @"btc_eur",    @"ltc_btc",    @"ltc_usd",
    @"ltc_rur",    @"ltc_eur",    @"nmc_btc",    @"nmc_usd",    @"nvc_btc",
    @"nvc_usd",    @"usd_rur",    @"eur_usd",    @"eur_rur",    @"ppc_btc",
    @"ppc_usd",    @"dsh_btc",    @"dsh_usd",    @"dsh_rur",    @"dsh_eur",
    @"dsh_ltc",    @"dsh_eth",    @"eth_btc",    @"eth_usd",    @"eth_eur",
    @"eth_ltc",    @"eth_rur",    @"bch_usd",    @"bch_btc",    @"bch_rur",
    @"bch_eur",    @"bch_ltc",    @"bch_eth",    @"bch_dsh",    @"zec_btc",
    @"zec_usd"};

NSString *const DEFAULT_PAIR = @"btc_usd";

