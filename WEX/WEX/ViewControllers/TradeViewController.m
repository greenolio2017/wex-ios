//
//  TradeViewController.m
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "TradeViewController.h"
#import "LoggingCommon.h"
#import "CommonUtil.h"
#import "TickerManager.h"
#import "Const.h"
#import "TradeManager.h"
#import "OrderCreatedNotificationViewController.h"
#import "ProgressDialogUtil.h"
#import "LoggingCommon.h"
#import "FruitIAPHelper.h"
#import "PremiumViewController.h"
#import "MarketsManager.h"
#import "DepthManager.h"
#import "FirebaseAnalytics.h"
#import "WEX-Swift.h"

@interface TradeViewController () <UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UIView *tradeView;

@property (weak, nonatomic) IBOutlet UIView *buyRegionView;

@property (weak, nonatomic) IBOutlet UIView *sellRegionView;
@property (weak, nonatomic) IBOutlet UIView *zoomingView;


@property (weak, nonatomic) IBOutlet UILabel *yourBalanceBuyLabel;
@property (weak, nonatomic) IBOutlet UILabel *lowestAskLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountDesTextBuy;
@property (weak, nonatomic) IBOutlet UILabel *priceDesTextBuy;
@property (weak, nonatomic) IBOutlet UITextField *priceBuy;
@property (weak, nonatomic) IBOutlet UITextField *quantityBuy;
@property (weak, nonatomic) IBOutlet UILabel *totalBuy;
@property (weak, nonatomic) IBOutlet UILabel *feeBuy;
@property (weak, nonatomic) IBOutlet UILabel *buyPriceUnit;


@property (weak, nonatomic) IBOutlet UILabel *yourBalanceSellLabel;
@property (weak, nonatomic) IBOutlet UILabel *highestBidlabel;
@property (weak, nonatomic) IBOutlet UILabel *amountDesTextSell;
@property (weak, nonatomic) IBOutlet UILabel *priceDesTextSell;
@property (weak, nonatomic) IBOutlet UITextField *priceSell;
@property (weak, nonatomic) IBOutlet UITextField *quantitySell;
@property (weak, nonatomic) IBOutlet UILabel *totalSell;
@property (weak, nonatomic) IBOutlet UILabel *feeSell;
@property (weak, nonatomic) IBOutlet UILabel *sellPriceUnit;


@property (nonatomic) double priceBuyValue;
@property (nonatomic) double quantityBuyValue;
@property (nonatomic) double priceSellValue;
@property (nonatomic) double quantitySellValue;

@property (nonatomic) NSString *askCurrencyStr;
@property (nonatomic) NSString *bidCurrencyStr;
@property (nonatomic) NSString *askCurrencyStrForDisplaying;
@property (nonatomic) NSString *bidCurrencyStrForDisplaying;

@property (nonatomic) TickerManager *tickerManager;
@property (nonatomic) TradeManager *tradeManager;

@property (nonatomic) UIGestureRecognizer *tapper;

@end

@implementation TradeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    KeyboardAvoiding.avoidingView = self.tradeView;
    
    self.tapper = [[UITapGestureRecognizer alloc]
                   initWithTarget:self action:@selector(handleSingleTap:)];
    self.tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:self.tapper];
    
    // Call API to get balance update for label
    [[ProgressDialogUtil sharedManager] showLoading:self.view alpha:0.0];
    [[TradeManager sharedManager] updateAccountInfo];
    
    // Observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDataTicker) name:TICKER_MANAGER_UPDATE_TICKERS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateInfoLabel) name:TRADE_INFO_UPDATE object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tradeOrderCreated:) name:TRADE_ORDER_CREATED object:nil];
    
    self.titleLabel.text = [CommonUtil pairStringToStringDisplay: self.currentPair];
    
    self.askCurrencyStr = [CommonUtil askCurrencyFromPairString:self.currentPair];
    self.bidCurrencyStr = [CommonUtil bidCurrencyFromPairString:self.currentPair];
    
    self.askCurrencyStrForDisplaying = [CommonUtil askCurrencyFromPairStringForDisplaying:self.currentPair];
    self.bidCurrencyStrForDisplaying = [CommonUtil bidCurrencyFromPairStringForDisplaying:self.currentPair];
    
    self.tickerManager = [TickerManager sharedManager];
    self.tradeManager = [TradeManager sharedManager];
    
    [self updateDataTicker];
    
    if ([self.sellAmount isEqualToString:@"0"]) {
        self.segmentedControl.selectedSegmentIndex = 0;
        self.buyRegionView.hidden = NO;
        self.sellRegionView.hidden = YES;
    }
    else {
        self.segmentedControl.selectedSegmentIndex = 1;
        self.buyRegionView.hidden = YES;
        self.sellRegionView.hidden = NO;
    }
    
    self.priceBuyValue = [self.priceAsk doubleValue];
    self.priceSellValue = [self.priceBid doubleValue];
    
    self.quantityBuyValue = [self.buyAmount doubleValue];
    self.quantitySellValue = [self.sellAmount doubleValue];
    
    // update labels
    self.yourBalanceBuyLabel.text  = [self getBidCurrentBalance];
    self.priceDesTextBuy.text = [@"Price per" stringByAppendingString:self.askCurrencyStrForDisplaying];
    self.priceDesTextSell.text = [@"Price per" stringByAppendingString:self.askCurrencyStrForDisplaying];
    
    self.yourBalanceSellLabel.text  = [self getAskCurrentBalance];
    self.amountDesTextBuy.text = [@"Amount" stringByAppendingString:self.askCurrencyStrForDisplaying];
    self.amountDesTextSell.text = [@"Amount" stringByAppendingString:self.askCurrencyStrForDisplaying];
    
    self.buyPriceUnit.text = self.bidCurrencyStrForDisplaying;
    self.sellPriceUnit.text = self.bidCurrencyStrForDisplaying;
    
    // tap gesture
    self.yourBalanceBuyLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGestureBuyBalance = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(balanceBuyLabelTap)];
    tapGestureBuyBalance.delegate=self;
    [self.yourBalanceBuyLabel addGestureRecognizer:tapGestureBuyBalance];
    
    self.yourBalanceSellLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGestureSellBalance = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(balanceSellLabelTap)];
    tapGestureSellBalance.delegate=self;
    [self.yourBalanceSellLabel addGestureRecognizer:tapGestureSellBalance];
    
    self.lowestAskLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGestureLowestAsk = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureLowestAsk)];
    tapGestureLowestAsk.delegate=self;
    [self.lowestAskLabel addGestureRecognizer:tapGestureLowestAsk];
    
    self.highestBidlabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGestureHighestBid = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureHighestBid)];
    tapGestureHighestBid.delegate=self;
    [self.highestBidlabel addGestureRecognizer:tapGestureHighestBid];
    
    // update fields auto
    [self updatePriceAndQuantity];
    [self updateTotalAndFee];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
     [[FirebaseAnalytics sharedManager] logEventWithName:FIR_DISPLAY_SCREEN_BUY_SELL_INFO parameters:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) updateDataTicker {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        TickerInfo *tickerInfo = [self.tickerManager getTickerInfo:self.currentPair];
        
        if (!tickerInfo) {
            return;
        }
        
        self.lowestAskLabel.text = [[CommonUtil dealWithDoubleWithFiveDecimal:[tickerInfo.buy doubleValue]] stringByAppendingString:self.bidCurrencyStrForDisplaying];
        self.highestBidlabel.text = [[CommonUtil dealWithDoubleWithFiveDecimal:[tickerInfo.sell doubleValue]] stringByAppendingString:self.bidCurrencyStrForDisplaying];
    });
}

- (void) updateInfoLabel {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ProgressDialogUtil sharedManager] hideLoading];
        self.yourBalanceBuyLabel.text  = [self getBidCurrentBalance];
        self.yourBalanceSellLabel.text  = [self getAskCurrentBalance];
    });
}

- (NSString *)getAskCurrentBalance {
    NSString *ret = @"";
    // get the ask currency
    double fund = [[self.tradeManager.funds objectForKey:self.askCurrencyStr] doubleValue];
    ret = [[CommonUtil dealWithDoubleWithEightDecimal:fund] stringByAppendingString:self.askCurrencyStrForDisplaying];
    return ret;
}

- (NSString *)getBidCurrentBalance {
    NSString *ret = @"";
    double fund = [[self.tradeManager.funds objectForKey:self.bidCurrencyStr] doubleValue];
    ret = [[CommonUtil dealWithDoubleWithEightDecimal:fund] stringByAppendingString:self.bidCurrencyStrForDisplaying];
    return ret;
}

- (void) updateTotalAndFee {
    double totalBuy = self.priceBuyValue * self.quantityBuyValue;
    totalBuy = [self calculateTotalValueBuy];
    self.totalBuy.text = [[CommonUtil dealWithDoubleWithEightDecimal:totalBuy] stringByAppendingString:self.bidCurrencyStrForDisplaying];
    self.feeBuy.text = [[CommonUtil dealWithDoubleWithEightDecimal:(self.quantityBuyValue * TRADE_FEE)] stringByAppendingString:self.askCurrencyStrForDisplaying];
    
    double totalSell = self.priceSellValue * self.quantitySellValue;
    totalSell = [self calculateTotalValueSell];
    self.totalSell.text = [[CommonUtil dealWithDoubleWithEightDecimal:totalSell] stringByAppendingString:self.bidCurrencyStrForDisplaying];
    self.feeSell.text = [[CommonUtil dealWithDoubleWithEightDecimal:totalSell * TRADE_FEE] stringByAppendingString:self.bidCurrencyStrForDisplaying];
}

- (double) calculateTotalValueBuy {
    NSMutableArray *askArray = [[NSMutableArray alloc] initWithArray:[DepthManager sharedManager].depthInfo.asks];
    double remainingAmount = self.quantityBuyValue;
    double totalBuy = 0;
    
    // buy all the lower price in orders
    for (int i = 0; i < [askArray count]; i++) {
        double askPrice = [[[askArray objectAtIndex:i] objectAtIndex:0] doubleValue];
        double askAmount = [[[askArray objectAtIndex:i] objectAtIndex:1] doubleValue];
        
        if (self.priceBuyValue > askPrice) {
            if (remainingAmount >= askAmount) {
                totalBuy = totalBuy + askAmount * askPrice;
                remainingAmount = remainingAmount - askAmount;
            }
            else {
                totalBuy = totalBuy + remainingAmount * askPrice;
                remainingAmount = 0;
                break;
            }
        }
        else {
            break;
        }
    }
    
    // buy remaining with exactly price
    if (remainingAmount > 0) {
        totalBuy = totalBuy + remainingAmount * self.priceBuyValue;
    }
    
    return totalBuy;
}

- (double) calculateTotalValueSell {
    NSMutableArray *bidArray = [[NSMutableArray alloc] initWithArray:[DepthManager sharedManager].depthInfo.bids];
    double remainingAmount = self.quantitySellValue;
    double totalSell = 0;
    
    // sell with upper price in orders
    for (int i = 0; i < [bidArray count]; i++) {
        double bidPrice = [[[bidArray objectAtIndex:i] objectAtIndex:0] doubleValue];
        double bidAmount = [[[bidArray objectAtIndex:i] objectAtIndex:1] doubleValue];
        
        if (self.priceSellValue < bidPrice) {
            if (remainingAmount >= bidAmount) {
                totalSell = totalSell + bidAmount * bidPrice;
                remainingAmount = remainingAmount - bidAmount;
            }
            else {
                totalSell = totalSell + remainingAmount * bidPrice;
                remainingAmount = 0;
                break;
            }
        }
        else {
            break;
        }
    }
    
    // self remaining with exactly price
    if (remainingAmount > 0) {
        totalSell = totalSell + remainingAmount * self.priceSellValue;
    }
    
    return totalSell;
}

- (void) updatePriceAndQuantity {
    
    self.quantityBuy.text = [CommonUtil dealWithDoubleWithEightDecimal:self.quantityBuyValue];
    self.quantitySell.text = [CommonUtil dealWithDoubleWithEightDecimal:self.quantitySellValue];
    
    self.priceSell.text = [CommonUtil dealWithDoubleWithFiveDecimal:self.priceSellValue];
    self.priceBuy.text = [CommonUtil dealWithDoubleWithFiveDecimal:self.priceBuyValue];
}

-(void) tradeOrderCreated:(NSNotification *) notification
{
    LogTrace("IN");
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        [dict setValue:@"buy" forKey:@"type"];
        [dict setValue:[self.priceBuy.text stringByAppendingString:self.bidCurrencyStrForDisplaying] forKey:@"price"];
        [dict setValue:[self.quantityBuy.text stringByAppendingString:self.askCurrencyStrForDisplaying] forKey:@"quantity"];
        [dict setValue:self.totalBuy.text forKey:@"total"];
    }
    else {
        [dict setValue:@"sell" forKey:@"type"];
        [dict setValue:[self.priceSell.text stringByAppendingString:self.bidCurrencyStrForDisplaying] forKey:@"price"];
        [dict setValue:[self.quantitySell.text stringByAppendingString:self.askCurrencyStrForDisplaying] forKey:@"quantity"];
        [dict setValue:self.totalSell.text forKey:@"total"];
    }
//    NSString *orderId = [dict valueForKey:@"order_id"];
    
    [self dismissViewControllerAnimated:NO completion:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:TRADE_ORDER_CREATED_FROM_TRADE_VC object:self userInfo:dict];
        });
    }];
}

#pragma Gesture tap
- (void)balanceBuyLabelTap {
    double balanceBuy = [[self.tradeManager.funds objectForKey:self.bidCurrencyStr] doubleValue];
    if (self.priceBuyValue > 0) {
        // subtraction 0.0000001 as like in website
        self.quantityBuyValue = balanceBuy / self.priceBuyValue - 0.0000001;
        if (self.quantityBuyValue < 0) {
            self.quantityBuyValue = 0;
        }
        self.quantityBuy.text = [CommonUtil dealWithDoubleWithEightDecimal:self.quantityBuyValue];
        [self updateTotalAndFee];
    }
    else {
        // show dialog
    }
}

- (void)balanceSellLabelTap {
    double quantitySell = [[self.tradeManager.funds objectForKey:self.askCurrencyStr] doubleValue];
    self.quantitySellValue = quantitySell;
    self.quantitySell.text = [CommonUtil dealWithDoubleWithEightDecimal:self.quantitySellValue];
    [self updateTotalAndFee];
}

- (void)tapGestureLowestAsk {
    self.priceBuyValue = [self.lowestAskLabel.text doubleValue];
    self.priceBuy.text = [CommonUtil dealWithDoubleWithFiveDecimal:self.priceBuyValue];
    [self updateTotalAndFee];
}

- (void)tapGestureHighestBid {
    self.priceSellValue = [self.highestBidlabel.text doubleValue];
     self.priceSell.text = [CommonUtil dealWithDoubleWithFiveDecimal:self.priceSellValue];
    [self updateTotalAndFee];
}

- (IBAction)segmentedControlTouchUpInside:(id)sender {
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        self.buyRegionView.hidden = NO;
        self.sellRegionView.hidden = YES;
        
    }
    else {
        self.buyRegionView.hidden = YES;
        self.sellRegionView.hidden = NO;
    }
}

- (IBAction)segmentedControlValueChanged:(id)sender {
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        self.buyRegionView.hidden = NO;
        self.sellRegionView.hidden = YES;
        
    }
    else {
        self.buyRegionView.hidden = YES;
        self.sellRegionView.hidden = NO;
    }
}

- (IBAction)closeButtonTouchUpInside:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)priceBuyEditChanged:(id)sender {
    self.priceBuyValue = [self.priceBuy.text doubleValue];
    [self updateTotalAndFee];
}
- (IBAction)quantityBuyEditChanged:(id)sender {
    self.quantityBuyValue = [self.quantityBuy.text doubleValue];
    [self updateTotalAndFee];
}
- (IBAction)priceSellEditChanged:(id)sender {
    self.priceSellValue = [self.priceSell.text doubleValue];
    [self updateTotalAndFee];
}
- (IBAction)quantitySellEditChanged:(id)sender {
    self.quantitySellValue = [self.quantitySell.text doubleValue];
    [self updateTotalAndFee];
}



- (IBAction)touchUpInsideBuyButton:(id)sender {
    //if ([[FruitIAPHelper sharedInstance] productPurchased:kIdentifierApple]) {
        [[ProgressDialogUtil sharedManager] showLoading:self.buyRegionView alpha:0.2f];
        [self.tradeManager buy:self.currentPair withRate: [CommonUtil dealWithDoubleWithFiveDecimal: self.priceBuyValue] withAmount:[CommonUtil dealWithDoubleWithEightDecimal: self.quantityBuyValue]];
    
    [[FirebaseAnalytics sharedManager] logEventWithName:FIR_ACTION_TOUCH_EVENT_ON_BUY_BUTTON parameters:nil];
//    }
//    else {
//        PremiumViewController *premiumVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"premiumVC"];
//        [self presentViewController:premiumVC animated:YES completion:nil];
//    }
}

- (IBAction)touchUpInsideSellButton:(id)sender {
    //if ([[FruitIAPHelper sharedInstance] productPurchased:kIdentifierApple]) {
    [[ProgressDialogUtil sharedManager] showLoading:self.sellRegionView alpha:0.2f];
    [self.tradeManager sell:self.currentPair withRate:[CommonUtil dealWithDoubleWithFiveDecimal: self.priceSellValue] withAmount:[CommonUtil dealWithDoubleWithEightDecimal: self.quantitySellValue]];
    
    [[FirebaseAnalytics sharedManager] logEventWithName:FIR_ACTION_TOUCH_EVENT_ON_SELL_BUTTON parameters:nil];
    
//    }
//    else {
//        PremiumViewController *premiumVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"premiumVC"];
//        [self presentViewController:premiumVC animated:YES completion:nil];
//    }
}


// dissmiss keyboard
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

@end
