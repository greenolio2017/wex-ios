//
//  WexApiHandler.m
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "WexApiHandler.h"
#import <CommonCrypto/CommonHMAC.h>
#import "Const.h"
#import "TradeApiProtocol.h"
#import "ReachabilityUtil.h"
#import "DialogUtil.h"

@implementation WexApiHandler

@synthesize api_key;
@synthesize secret_key;

- (id)init {
    self = [super init];
    if (self) {
        [self setupInitialValues];
    }
    return self;
}

- (void)setupInitialValues {
    if ([[NSUserDefaults standardUserDefaults] stringForKey:USERDEFAULT_KEY_API_KEY]  &&
        [[NSUserDefaults standardUserDefaults] stringForKey:USERDEFAULT_KEY_SECRET_KEY]) {
        api_key = [[NSUserDefaults standardUserDefaults] stringForKey:USERDEFAULT_KEY_API_KEY];
        secret_key = [[NSUserDefaults standardUserDefaults] stringForKey:USERDEFAULT_KEY_SECRET_KEY];
    }
    else {
        api_key = @"";
        secret_key = @"";
    }
}


- (void)getResponseFromServerForPost:(NSDictionary *)postDictionary withApi:(NSString *)api {
    
    if (![ReachabilityUtil canNetworkConnected]) {
        [DialogUtil showDialogCannotConnectNetwork];
        return;
    }
    
    NSString *post;
    int i = 0;
    for (NSString *key in [postDictionary allKeys]) {
        NSString *value = [postDictionary objectForKey:key];
        if (i==0)
            post = [NSString stringWithFormat:@"%@=%@", key, value];
        else
            post = [NSString stringWithFormat:@"%@&%@=%@", post, key, value];
        i++;
    }
    post = [NSString stringWithFormat:@"%@&nonce=%@", post, getNonce()];
    
    
    NSString *signedPost = hmacForKeyAndData(secret_key, post);
    
    NSURL *url = [NSURL URLWithString:WEX_TAPI_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:api_key forHTTPHeaderField:@"key"];
    [request setValue:signedPost forHTTPHeaderField:@"sign"];
    [request setValue:@"w1n42rNT8ki6Y6s" forHTTPHeaderField:@"X-App-Id"];
    [request setHTTPBody:[post dataUsingEncoding: NSUTF8StringEncoding]];
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if ([api isEqualToString:@"info"]) {
        [self.tradeDelegate updateInfo: data];
        }
        else if ([api isEqualToString:@"buy"]) {
            [self.tradeDelegate buy: data];
        }
        else if ([api isEqualToString:@"sell"]) {
            [self.tradeDelegate sell: data];
        }
        else if ([api isEqualToString:@"ActiveOrders"]) {
            [self.orderDelegate activeOrders: data];
        }
        else if ([api isEqualToString:@"OrderInfo"]) {
            [self.orderDelegate orderInfo: data];
        }
        else if ([api isEqualToString:@"CancelOrder"]) {
            [self.orderDelegate cancelOrderDelegate: data];
        }
        else if ([api isEqualToString:METHOD_CANCEL_ORDER_ALL]) {
            [self.orderDelegate cancelOrderAllDelegate: data];
        }
        else if ([api isEqualToString:@"TradeHistory"]) {
            [self.tradeHistoryDelegate tradeHistoryUpdate: data];
        }
    }] resume];
    
}

- (void)getResponseFromPublicServerUrl:(NSString *)urlString withHandler: (void (^) (NSData *response))handler {
    
    if (![ReachabilityUtil canNetworkConnected]) {
        [DialogUtil showDialogCannotConnectNetwork];
        handler(nil);
        return;
    }
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:url
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                if (error) {
                    [self getResponseFromPublicServerUrl:urlString withHandler:handler];
                }
                else {
                    handler(data);
                }
                
            }] resume];
}

NSString *getNonce() {
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss xxxx"];
    NSTimeInterval timeStamp = [[NSDate date] timeIntervalSinceDate:[formater dateFromString:@"2012-04-18 00:00:01 +0600"]];
    int currentNonce = [NSNumber numberWithDouble: timeStamp].intValue;
    NSString *nonceString = [NSString stringWithFormat:@"%i",currentNonce];
    return nonceString;
}

NSString *hmacForKeyAndData(NSString *key, NSString *data) {
    const char *cKey = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [data cStringUsingEncoding:NSASCIIStringEncoding];
    unsigned char cHMAC[CC_SHA512_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA512, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    NSMutableString *hashString = [NSMutableString stringWithCapacity:sizeof(cHMAC) * 2];
    for (int i = 0; i < sizeof(cHMAC); i++) {
        [hashString appendFormat:@"%02x", cHMAC[i]];
    }
    return hashString;
}

@end
