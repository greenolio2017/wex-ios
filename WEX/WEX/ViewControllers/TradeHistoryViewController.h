//
//  ActiveOrdersViewController.h
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/29.
//  Copyright © 2017年 Ta Duong Ngoc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TradeHistoryViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic) NSString *orderIdDisplayed;
@end
