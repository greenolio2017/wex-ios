//
//  PremiumViewController.m
//  WEX
//
//  Created by Ta Duong Ngoc on 12/28/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "PremiumViewController.h"
#import "FruitIAPHelper.h"
#import "Const.h"
#import "LoggingCommon.h"
#import "ProgressDialogUtil.h"

@interface PremiumViewController ()
@property (nonatomic, strong) NSArray *products;
@property (weak, nonatomic) IBOutlet UIView *buyView;
@property (weak, nonatomic) IBOutlet UILabel *infomationLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (weak, nonatomic) IBOutlet UIView *okView;
@property (weak, nonatomic) IBOutlet UIButton *okViewCloseButton;
@property (nonatomic, strong) NSNumberFormatter *priceFormatter;

@end

@implementation PremiumViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.priceFormatter = [[NSNumberFormatter alloc] init];
    [self.priceFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [self.priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    
    [[ProgressDialogUtil sharedManager] showLoading:self.view alpha:0];
    
    [self reload];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
    [super viewWillAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)reload
{
    self.buyView.hidden = YES;
    self.okView.hidden = YES;
    [[ProgressDialogUtil sharedManager] showLoading:self.view alpha:0];
    self.products = nil;
    [[FruitIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success) {
            self.products = products;
            [self updateLayoutAccordingToProducts];
        }
        else {
            self.products = nil;
            [self updateLayoutAccordingToProducts];
        }
    }];
}

- (void)productPurchased:(NSNotification *)notification
{
    NSString *productIdentifier = notification.object;
    [self.products enumerateObjectsUsingBlock:^(SKProduct *product, NSUInteger idx, BOOL *stop) {
        if ([product.productIdentifier isEqualToString:productIdentifier]) {
            // update product is bought
            if ([product.productIdentifier isEqualToString:kIdentifierApple]) {
                [self updateLayoutAccordingToProducts];
            }
            *stop = YES;
        }
    }];
}

- (void) updateLayoutAccordingToProducts {
    
    BOOL isProductPurchased = NO;
    
    if (self.products == nil || [self.products count] <= 0) {
        self.buyView.hidden = NO;
        self.okView.hidden = YES;
        [[ProgressDialogUtil sharedManager] hideLoading];
        return;
    }
    
    for (SKProduct *product in self.products) {
        if ([product.productIdentifier isEqualToString:kIdentifierApple]) {
            // udpate product is purchased
            [self.priceFormatter setLocale:product.priceLocale];
            self.priceLabel.text = [self.priceFormatter stringFromNumber:product.price];
            if ([[FruitIAPHelper sharedInstance] productPurchased:product.productIdentifier]) {
                self.buyView.hidden = YES;
                self.okView.hidden = NO;
                isProductPurchased = YES;
                [[ProgressDialogUtil sharedManager] hideLoading];
            }
        }
    }
    
    if (!isProductPurchased) {
        self.buyView.hidden = NO;
        self.okView.hidden = YES;
        [[ProgressDialogUtil sharedManager] hideLoading];
    }
}

- (IBAction)restoreButtonTouchUpInside:(id)sender {
    [[ProgressDialogUtil sharedManager] showLoading:self.view alpha:0];
    [[FruitIAPHelper sharedInstance] restoreCompletedTransactions];
}

- (IBAction)usePremiumTouchUpInside:(id)sender {
    [[ProgressDialogUtil sharedManager] showLoading:self.view alpha:0];
    for (SKProduct *product in self.products) {
        if ([product.productIdentifier isEqualToString:kIdentifierApple]) {
            LogTrace(@"Buying %@ ... (buyButtonTapped in FruitTableViewController.m", product.productIdentifier);
            [[FruitIAPHelper sharedInstance] buyProduct:product];
        }
    }
}
- (IBAction)useFreeVersionTouchUpInside:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)okViewCloseButtionTouchUpInside:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
