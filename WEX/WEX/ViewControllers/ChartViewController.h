//
//  ChartViewController.h
//  WEX
//
//  Created by Ta Duong Ngoc on 12/15/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChartViewController : UIViewController
    @property float widthForChart;
    @property float heightForChart;
    @property NSString* currentPair;
@end
