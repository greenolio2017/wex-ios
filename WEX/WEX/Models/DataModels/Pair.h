//
//  Pair.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pair : NSObject

@property (nonatomic) NSString *askCurrency;
@property (nonatomic) NSString *bidCurrency;

- (instancetype)initWithPairString: (NSString *) pairString;

@end
