//
//  MarketInfo.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MarketInfo : NSObject
@property (nonatomic) NSString * pairString;

@property (nonatomic) NSNumber * decimal_places;
@property (nonatomic) NSNumber * min_price;
@property (nonatomic) NSNumber * max_price;
@property (nonatomic) NSNumber * min_amount;
@property (nonatomic) NSNumber * hidden;
@property (nonatomic) NSNumber * fee;

- (instancetype)initWithPairString: (NSString *) pairString
                    decimal_places: (NSNumber *) decimal_places
                         min_price: (NSNumber *) min_price
                         max_price: (NSNumber *) max_price
                        min_amount: (NSNumber *) min_amount
                            hidden: (NSNumber *) hidden
                               fee: (NSNumber *) fee;

//- (void)updateTickerInfo: (NSNumber *) decimal_places
//               min_price: (NSNumber *) min_price
//               max_price: (NSNumber *) max_price
//              min_amount: (NSNumber *) min_amount
//                  hidden: (NSNumber *) hidden
//                     fee: (NSNumber *) fee;

@end
