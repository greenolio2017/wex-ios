//
//  DialogUtil.h
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/29.
//  Copyright © 2017年Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DialogUtil : NSObject
+ (void)showDialogCannotConnectNetwork;
+ (void)showDialogWithError: (NSString *) title withError: (NSString *) error withButtonName: (NSString *) buttonName buttonHandler:(void (^)(void))buttonHandler;
+ (void)dismissDialog;
@end
