//
//  PublicTradesViewController.h
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/29.
//  Copyright © 2017年 Ta Duong Ngoc. All rights reserved.
//

#import "PublicTradesViewController.h"
#import "TradesTableViewCell.h"
#import "CommonUtil.h"
#import "Const.h"
#import "ProgressDialogUtil.h"
#import "TradesManager.h"
#import "LoggingCommon.h"

@interface PublicTradesViewController ()
@property (weak, nonatomic) IBOutlet UITableView *publicTradesTableView;

@property NSMutableArray *tradesInfo;

@end

@implementation PublicTradesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateCurrentPair:) name:PAIRS_CHANGED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTrades) name:TRADE_HISTORY_MANAGER_UPDATE_TRADES object:nil];
    
    // tableView setup
    [self.publicTradesTableView registerNib:[UINib nibWithNibName:@"TradesTableViewCell" bundle:nil] forCellReuseIdentifier:@"TradesTableViewCell"];
    self.publicTradesTableView.tableFooterView = [[UIView alloc] init];
    
    // Init data
    [self updateTrades];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) updateCurrentPair:(NSNotification *) notification
{
    LogTrace("IN");
    [self.publicTradesTableView setContentOffset:CGPointZero animated:YES];
    LogTrace("OUT");
}

-(void)updateTrades{
    LogTrace("IN");
    dispatch_async(dispatch_get_main_queue(), ^{
        self.tradesInfo = [[NSMutableArray alloc] initWithArray:[TradesManager sharedManager].tradesInfo];
        if ([self.tradesInfo count] > 0) {
            [self.publicTradesTableView reloadData];
        }
    });
    LogTrace("OUT");
}

#pragma mark <UITableViewDelegate>
/**
 ----------------------------------------------------------------------------------------------------
 * @brief heightForRowAtIndexPath
 ----------------------------------------------------------------------------------------------------
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 36;
}

/**
 ----------------------------------------------------------------------------------------------------
 * @brief didSelectRowAtIndexPath
 ----------------------------------------------------------------------------------------------------
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([TradesManager sharedManager] != nil && [self.tradesInfo count] > indexPath.row) {
        NSDictionary *tradeInfo = [self.tradesInfo objectAtIndex:indexPath.row];
        NSNumber *price = [tradeInfo objectForKey:@"price"];
        NSNumber *amount = [tradeInfo objectForKey: @"amount"];
        
        NSString *priceStr = [CommonUtil dealWithDoubleWithFiveDecimal:[price doubleValue]];
        NSString *quantityStr = [CommonUtil dealWithDoubleWithEightDecimal:[amount doubleValue]];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setValue: priceStr forKey:@"price"];
        [dict setValue: quantityStr forKey:@"quantity"];
        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [[NSNotificationCenter defaultCenter] postNotificationName:PRESENT_TRADE_VC_FROM_TABLE_CELL object:self userInfo:dict];
//        });
    }
    
}

#pragma mark <UITableViewDataSource>
/**
 ----------------------------------------------------------------------------------------------------
 * @brief numberOfSectionsInTableView
 ----------------------------------------------------------------------------------------------------
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


/**
 ----------------------------------------------------------------------------------------------------
 * @brief numberOfRowsInSection
 ----------------------------------------------------------------------------------------------------
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 50;
}


/**
 ----------------------------------------------------------------------------------------------------
 * @brief cellForRowAtIndexPath
 ----------------------------------------------------------------------------------------------------
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TradesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TradesTableViewCell"];
    if (cell == nil) {
        cell = [[TradesTableViewCell alloc] init];
    }
    if ([TradesManager sharedManager] != nil && [self.tradesInfo count] > indexPath.row) {
        NSDictionary *tradeInfo = [self.tradesInfo objectAtIndex:indexPath.row];
        NSString *type = [tradeInfo objectForKey:@"type"];
        NSNumber *price = [tradeInfo objectForKey:@"price"];
        NSNumber *amount = [tradeInfo objectForKey: @"amount"];
        NSNumber *timestamp = [tradeInfo objectForKey:@"timestamp"];
        
        cell.price.text = [CommonUtil dealWithDoubleWithFiveDecimal:[price doubleValue]];
        cell.quantity.text = [CommonUtil dealWithDoubleWithEightDecimal:[amount doubleValue]];
        
        if ([type isEqual: @"bid"]) {
            cell.type.text = @"BUY";
            [self updateTextColor:cell withColor:COLOR_GREEN];
            
        }
        else {
            cell.type.text = @"SELL";
            [self updateTextColor:cell withColor:COLOR_RED];
        }
        
        
        NSTimeInterval timeInterval=[timestamp doubleValue];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
        NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
        [dateformatter setLocale:[NSLocale currentLocale]];
        [dateformatter setDateFormat:@"HH:mm:ss"];
        NSString *dateString=[dateformatter stringFromDate:date];
        cell.date.text = dateString;
    }
    return cell;
}

-(void) updateTextColor: (TradesTableViewCell*) cell withColor:(UIColor*) color {
    cell.price.textColor = color;
    cell.quantity.textColor = color;
    cell.type.textColor = color;
    cell.date.textColor = color;
    
}

@end
