//
//  AboutUsViewController.m
//  WEX
//
//  Created by Ngoc on 6/15/18.
//  Copyright © 2018 Ta Duong Ngoc. All rights reserved.
//

#import "AboutUsViewController.h"

@interface AboutUsViewController ()

@end

@implementation AboutUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)twitterButtonTouchUpInside:(id)sender {
    [self openURL:@"https://twitter.com/wexnz"];
}

- (IBAction)facebookButtonTouchUpInside:(id)sender {
    [self openURL:@"https://www.facebook.com/wexnzofficial"];
}

- (IBAction)telegramButtonTouchUpInside:(id)sender {
    [self openURL:@"https://t.me/wexrus"];
}

- (IBAction)supportButtonTouchUpInside:(id)sender {
    [self openURL:@"https://support.wex.nz"];
}

- (void)openURL: (NSString*) urlStr {
    NSURL *url = [NSURL URLWithString:urlStr];
    [[UIApplication sharedApplication] openURL:url];
}


@end
