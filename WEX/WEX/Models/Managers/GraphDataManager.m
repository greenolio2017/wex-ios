//
//  GraphDataManager.m
//  WEX
//
//  Created by Ta Duong Ngoc on 11/16/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "GraphDataManager.h"
#import "LoggingCommon.h"
#import "Const.h"

@implementation GraphDataManager

{
    NSTimer *tracker;
}


+ (GraphDataManager *)sharedManager {
    static GraphDataManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    self = [super init];
    self.currentChartData = @"####graph_data_filled_here####";
    self.currentPair = @"btc_usd";
    return self;
}

- (void)getHTMLStringFromURL: (NSString*) pair {
    self.currentPair = pair;
    dispatch_async(kBgQueue, ^{
        // Display old chart only
        NSString *url = [[WEX_WEB_URL stringByAppendingString:pair] stringByAppendingString:@"?old_charts=1"];
        NSURL *urlRequest = [NSURL URLWithString:url];
        NSError *err = nil;
        
        NSString *html = [NSString stringWithContentsOfURL:urlRequest encoding:NSUTF8StringEncoding error:&err];
        LogTrace(@"%@", html);
        
        if(err)
        {
            html = @"";
        }
        
        NSString *chartData = [self getChartDataFromHTMLString:html];
        
        NSError* error = nil;
        NSString *path = [[NSBundle mainBundle] pathForResource: @"graph" ofType: @"html"];
        NSString *res = [NSString stringWithContentsOfFile: path encoding:NSUTF8StringEncoding error: &error];
        
        if ([chartData isEqualToString:self.currentChartData]) {
            return;
        }
        
        self.currentChartData = chartData;
        self.currentHTMLString = [res stringByReplacingOccurrencesOfString:@"####graph_data_filled_here####" withString:self.currentChartData];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:
             GRAPH_MANAGER_UPDATE_DATA object:nil userInfo:nil];
        });
        
        LogTrace(@"currentHTMLString: %@", self.currentHTMLString);
    });
}

- (NSString *)getChartDataFromHTMLString: (NSString *)htmlString {
    LogTrace(@"IN");
    NSRange searchFromRange = [htmlString rangeOfString:@"var data = google.visualization.arrayToDataTable"];
    NSRange searchToRange = [htmlString rangeOfString:@"]], true);"];
    NSString *subString = @"";
    if (searchFromRange.location != NSNotFound && searchToRange.location != NSNotFound &&
        searchFromRange.length > 0 && searchToRange.length > 0)
    
    {
        subString = [htmlString substringWithRange:NSMakeRange(searchFromRange.location, searchToRange.location + searchToRange.length - searchFromRange.location)];
    }
    
    return subString;
    LogTrace(@"OUT");
}

- (void)setTimerActive: (BOOL) isStart {
    if (isStart){
        tracker = [NSTimer scheduledTimerWithTimeInterval:10.0f
                                                   target:self
                                                 selector:@selector(updateManagerData)
                                                 userInfo:nil repeats:YES];
        
    } else {
        [tracker invalidate];
    }
}

- (void)updateManagerData {
    dispatch_async(kBgQueue, ^{
        [self getHTMLStringFromURL: self.currentPair];
    });
}

@end
