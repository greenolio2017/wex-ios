//
//  ActiveOrdersViewController.m
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/29.
//  Copyright © 2017年 Ta Duong Ngoc. All rights reserved.
//

#import "ActiveOrdersViewController.h"
#import "ActiveOrderTableViewCell.h"
#import "OrderManager.h"
#import "OrderInfo.h"
#import "CommonUtil.h"
#import "Const.h"
#import "ProgressDialogUtil.h"

@interface ActiveOrdersViewController ()
@property (weak, nonatomic) IBOutlet UITableView *orderListTableView;
@property (weak, nonatomic) IBOutlet UILabel *noOrderLabel;

@property NSMutableArray<NSString *> *orderIds;

@end

@implementation ActiveOrdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateListOrders) name:ORDER_ACTIVE_ORDERS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateActiveOrders) name:ORDER_CANCEL_ORDER object:nil];
    
    // Init order ids
    self.orderIds = [[NSMutableArray alloc] init];
    
    // tableView setup
    [self.orderListTableView registerNib:[UINib nibWithNibName:@"ActiveOrderTableViewCell" bundle:nil] forCellReuseIdentifier:@"ActiveOrderTableViewCell"];
    self.orderListTableView.tableFooterView = [[UIView alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateActiveOrders];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)updateListOrders {
    if ([[OrderManager sharedManager].activeOrders count] <= 0) {
        self.orderListTableView.hidden = YES;
        self.noOrderLabel.hidden = NO;
    }
    else {
        self.orderListTableView.hidden = NO;
        self.noOrderLabel.hidden = YES;
    }
    [self.orderListTableView reloadData];
}

- (void)updateActiveOrders {
    [[ProgressDialogUtil sharedManager] showLoading:self.view alpha:0.2f];
    [[OrderManager sharedManager] updateActiveOrders];
}

- (void) apiKeyChanged  {
    [[OrderManager sharedManager].apiHandler setupInitialValues];
}

#pragma mark <UITableViewDelegate>
/**
 ----------------------------------------------------------------------------------------------------
 * @brief heightForRowAtIndexPath
 ----------------------------------------------------------------------------------------------------
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

/**
 ----------------------------------------------------------------------------------------------------
 * @brief didSelectRowAtIndexPath
 ----------------------------------------------------------------------------------------------------
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}

#pragma mark <UITableViewDataSource>
/**
 ----------------------------------------------------------------------------------------------------
 * @brief numberOfSectionsInTableView
 ----------------------------------------------------------------------------------------------------
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


/**
 ----------------------------------------------------------------------------------------------------
 * @brief numberOfRowsInSection
 ----------------------------------------------------------------------------------------------------
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[OrderManager sharedManager].activeOrders count];
}


/**
 ----------------------------------------------------------------------------------------------------
 * @brief cellForRowAtIndexPath
 ----------------------------------------------------------------------------------------------------
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ActiveOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActiveOrderTableViewCell"];
    if (cell == nil) {
        cell = [[ActiveOrderTableViewCell alloc] init];
    }
    
    if ([OrderManager sharedManager].activeOrders == nil ||
        [[OrderManager sharedManager].activeOrders count] <= 0) {
        return cell;
    }
    
    
    OrderInfo *orderInfo = [[OrderManager sharedManager].activeOrders objectAtIndex:indexPath.row];
    cell.pairLabel.text = [CommonUtil pairStringToStringDisplay: orderInfo.pair];
    cell.typeLabel.text = orderInfo.type;
    cell.amountLabel.text = [CommonUtil dealWithDoubleWithEightDecimal:[orderInfo.start_amount doubleValue]];
    cell.priceLabel.text = [CommonUtil dealWithDoubleWithFiveDecimal:[orderInfo.rate doubleValue]];
    
    if ([[cell.typeLabel.text uppercaseString] isEqual: @"SELL"]) {
        cell.typeLabel.text = @"SELL";
        cell.typeLabel.textColor = COLOR_RED;
    }
    else {
        cell.typeLabel.text = @"BUY";
        cell.typeLabel.textColor = COLOR_GREEN;
    }
    
    cell.indexPathCell = indexPath;
    return cell;
}

- (IBAction)backButtonTouchUpInside:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelAllButtonTouchUpInside:(id)sender {
    if ([[OrderManager sharedManager].activeOrders count] <= 0) {
        // do nothing
        return;
    }
    
     [self.orderIds removeAllObjects];
    
    for (OrderInfo *orderInfo in [OrderManager sharedManager].activeOrders) {
        NSString * orderId = orderInfo.orderId;
        [self.orderIds addObject:orderId];
    }
    
    [[OrderManager sharedManager] cancelOrders:self.orderIds];
}

@end
