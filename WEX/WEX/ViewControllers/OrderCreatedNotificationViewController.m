//
//  OrderCreatedNotificationViewController.m
//  WEX
//
//  Created by Ta Duong Ngoc on 11/30/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "OrderCreatedNotificationViewController.h"

@interface OrderCreatedNotificationViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabelValue;
@property (weak, nonatomic) IBOutlet UILabel *priceLabelValue;
@property (weak, nonatomic) IBOutlet UILabel *totalLabelValue;
@property (weak, nonatomic) IBOutlet UIView *blackView;

@end

@implementation OrderCreatedNotificationViewController

- (void)viewDidLoad {
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.blackView addGestureRecognizer:singleFingerTap];
    
    [super viewDidLoad];
    if ([self.type  isEqual: @"buy"]) {
        self.titleLabel.text = @"Buy order is created";
    }
    else {
        self.titleLabel.text = @"Sell order is created";
    }
    
    self.amountLabelValue.text = self.amount;
    self.priceLabelValue.text = self.price;
    self.totalLabelValue.text = self.total;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)closeButtonTouchUpInside:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
