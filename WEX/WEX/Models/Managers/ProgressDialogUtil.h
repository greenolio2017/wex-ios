//
//  ProgressDialogUtil.h
//  WEX
//
//  Created by Ta Duong Ngoc on 12/1/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "LoadingView.h"

@interface ProgressDialogUtil : NSObject
@property (nonatomic) BOOL isLoading;
@property (nonatomic) LoadingView *loadingView;

+ (ProgressDialogUtil *)sharedManager;

- (void)showLoading:(UIView *)parentView alpha: (CGFloat)alphaBackgroundColor;
- (void)hideLoading;


@end
