//
//  AppDelegate.m
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "HeaderViewCell.h"

@interface HeaderViewCell()

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@end

@implementation HeaderViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.exclusiveTouch = YES;
    self.backgroundColor = [UIColor whiteColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setTitleForCell:(NSString *)title {
    if (title == nil || [title isEqualToString:@""]) {
        self.lblTitle.hidden = YES;
    }
    else {
        self.lblTitle.hidden = NO;
        self.lblTitle.text = title;
    }
}

@end
