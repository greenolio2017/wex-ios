//
//  TradesTableViewCell.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/16/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TradesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *quantity;
@property (weak, nonatomic) IBOutlet UILabel *type;


@end
