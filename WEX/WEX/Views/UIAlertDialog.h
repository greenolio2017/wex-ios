//
//  UIAlertDialog.h
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/27.
//  Copyright © 2017年 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIAlertDialog : NSObject

+ (UIAlertDialog *)sharedManager;

- (void)alertWithTitle:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelButtonTitle cancelHandler:(void (^)(void))cancelHandler otherButtonTitle:(NSString *)otherButtonTitle otherHandler:(void (^)(void))otherHandler;
- (void)alertWithTitle:(NSString *)title message:(NSString *)message buttonTitle:(NSString *)buttonTitle buttonHandler:(void (^)(void))buttonHandler;
- (void)alertInputTextWithTitle:(NSString *)title
                        message:(NSString *)message
              cancelButtonTitle:(NSString *)cancelButtonTitle
                  cancelHandler:(void (^)(void))cancelHandler
               otherButtonTitle:(NSString *)otherButtonTitle
                   otherHandler:(void (^)(void))otherHandler
                      optionDic:(NSDictionary *)optionDic;
- (void)show;
- (void)dismiss;
- (BOOL)isShown;

@end
