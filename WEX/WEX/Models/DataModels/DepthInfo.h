//
//  DepthInfo.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/18/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DepthInfo : NSObject
@property (nonatomic) NSMutableArray *asks;
@property (nonatomic) NSMutableArray *bids;
@end
