//
//  GraphDataManager.m
//  WEX
//
//  Created by Ta Duong Ngoc on 11/16/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "GraphNewDataManager.h"
#import "LoggingCommon.h"
#import "Const.h"
#import <UIKit/UIKit.h>

@implementation GraphNewDataManager

+ (GraphNewDataManager *)sharedManager {
    static GraphNewDataManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    self = [super init];
    return self;
}

- (NSString *)getHTMLStringFromURL: (NSString*) pair {
    NSString *pairStr = [[pair stringByReplacingOccurrencesOfString:@"_" withString:@""] uppercaseString];
    //dispatch_async(kBgQueue, ^{
        
        NSError* error = nil;
        NSString *path = @"";
        //if (IS_IPAD_DEVICE)
        //{
        //    path = [[NSBundle mainBundle] pathForResource: @"graph_new_ipad" ofType: @"html"];
        //}
        //else {
            path = [[NSBundle mainBundle] pathForResource: @"graph_new" ofType: @"html"];
        //}
        
        NSString *res = [NSString stringWithContentsOfFile: path encoding:NSUTF8StringEncoding error: &error];
        
        NSString *htmlStr = [res stringByReplacingOccurrencesOfString:@"###PAIR_IS_HERE_BTCUSD###" withString:pairStr];
        
        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [[NSNotificationCenter defaultCenter] postNotificationName:
//             GRAPH_MANAGER_UPDATE_DATA object:nil userInfo:nil];
//        });
        
        LogTrace(@"currentHTMLString: %@", htmlStr);
    //});
    return htmlStr;
}

@end
