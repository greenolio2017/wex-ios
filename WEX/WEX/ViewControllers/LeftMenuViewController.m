//
//  LeftMenuViewController.m
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "MarketsManager.h"
#import "HeaderViewCell.h"
#import "LoggingCommon.h"
#import "ItemMenuTableViewCell.h"
#import "Const.h"
#import "CommonUtil.h"
#import "ApiKeyInputViewController.h"
#import "FundsViewController.h"
#import "ActiveOrdersViewController.h"
#import "TradeHistoryViewController.h"
#import "PremiumViewController.h"
#import "WexNavigationController.h"
#import "PinSettingViewController.h"
#import "AboutUsViewController.h"

#import "FirebaseAnalytics.h"

// for PIN
#import "TOPasscodeViewController.h"
#import "TOPasscodeViewControllerConstants.h"
#import <LocalAuthentication/LocalAuthentication.h>
#import "PinSettingViewController.h"
#import "WexNavigationController.h"
#import "CommonUtil.h"
#import "AppDelegate.h"
#import <LocalAuthentication/LocalAuthentication.h>

typedef NS_ENUM(NSInteger, SelectedItemType) {
    APIKeySelected,
    ActiveOrdersSelected,
    TradeHistorySelected,
    BalancesSelected
};

@interface LeftMenuViewController () <UITableViewDelegate, UITableViewDataSource, TOPasscodeViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UIView *headerTableView;
@property SelectedItemType itemSelected;

@property (nonatomic, strong) LAContext *authContext;
@property (nonatomic, assign) BOOL biometricsAvailable;
@property (nonatomic, assign) BOOL faceIDAvailable;

@end

@implementation LeftMenuViewController


/**
 ----------------------------------------------------------------------------------------------------
 * @brief viewDidLoad
 ----------------------------------------------------------------------------------------------------
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpTableView];
    
    // Init authentication context
    self.authContext = [[LAContext alloc] init];    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

/**
 ----------------------------------------------------------------------------------------------------
 * @brief viewDidAppear
 * @param animated BOOL
 ----------------------------------------------------------------------------------------------------
 */
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

/**
 ----------------------------------------------------------------------------------------------------
 * @brief viewWillDisappear
 * @param animated BOOL
 ----------------------------------------------------------------------------------------------------
 */
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

/**
 ----------------------------------------------------------------------------------------------------
 * @brief didReceiveMemoryWarning
 ----------------------------------------------------------------------------------------------------
 */
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/**
 ----------------------------------------------------------------------------------------------------
 * @brief setUpTable
 ----------------------------------------------------------------------------------------------------
 */
- (void)setUpTableView {
    self.tbviewContent.delegate = self;
    self.tbviewContent.dataSource = self;
    self.tbviewContent.backgroundColor = [UIColor whiteColor];
    [self.tbviewContent registerNib:[UINib nibWithNibName:@"ItemMenuTableViewCell" bundle:nil] forCellReuseIdentifier:@"ItemMenuTableViewCell"];
    [self.tbviewContent registerNib:[UINib nibWithNibName:@"HeaderViewCell" bundle:nil] forCellReuseIdentifier:@"HeaderViewCell"];
    
    self.tbviewContent.separatorColor = [UIColor clearColor];
    self.tbviewContent.tableFooterView = [[[NSBundle mainBundle] loadNibNamed:@"FooterViewCell" owner:self options:nil] firstObject];
    
}
#pragma mark <UITableViewDelegate>
/**
 ----------------------------------------------------------------------------------------------------
 * @brief heightForRowAtIndexPath
 ----------------------------------------------------------------------------------------------------
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

/**
 ----------------------------------------------------------------------------------------------------
 * @brief heightForHeaderInSection
 ----------------------------------------------------------------------------------------------------
 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 20;
}

/**
 ----------------------------------------------------------------------------------------------------
 * @brief heightForFooterInSection
 ----------------------------------------------------------------------------------------------------
 */
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.5;
}

/**
 ----------------------------------------------------------------------------------------------------
 * @brief viewForHeaderInSection
 ----------------------------------------------------------------------------------------------------
 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    HeaderViewCell *headerCell = (HeaderViewCell*)[tableView dequeueReusableCellWithIdentifier:@"HeaderViewCell"];
    if (headerCell == nil) {
        headerCell = [[HeaderViewCell alloc] init];
    }
    NSString *title = @"";
    if (section == 0) {
        title = @"Setting";
    }
    
    if (section == 1) {
        title = @"User Info";
    }
    
    if (section == 2) {
        title = @"Others";
    }
    
    [headerCell setTitleForCell:title];
    return headerCell;
}

/**
 ----------------------------------------------------------------------------------------------------
 * @brief didSelectRowAtIndexPath
 ----------------------------------------------------------------------------------------------------
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {
            
            //Firebase Analytics
            [[FirebaseAnalytics sharedManager] logEventWithName:FIR_ACTION_TOUCH_EVENT_ON_PIN_SETTING parameters:nil];
            
            PinSettingViewController *pinSettingVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"pinSettingVC"];
            WexNavigationController *navController = [[WexNavigationController alloc] initWithRootViewController:pinSettingVC];
            
            [self presentViewController:navController animated:YES completion:nil];
        }
        if (indexPath.row == 1) {
            // Display PIN screen if user did not input it
            if ([CommonUtil isPINCheckAlready] == FALSE) {
                [CommonUtil displayPINScreen:self withContext:self.authContext];
                return;
            }
            self.itemSelected = APIKeySelected;
            [self presentAuthenticatedViewController];
        }
    }
    else if (indexPath.section == 1) {
        // Store current item selected
        if (indexPath.row == 0) {
            self.itemSelected = ActiveOrdersSelected;
        }
        else if (indexPath.row == 1) {
            self.itemSelected = TradeHistorySelected;
        }
        else if (indexPath.row == 2) {
            self.itemSelected = BalancesSelected;
        }
            
        // Display PIN screen if user did not input it
        if ([CommonUtil isPINCheckAlready] == FALSE) {
            [CommonUtil displayPINScreen:self withContext:self.authContext];
            return;
        }
        
        [self presentAuthenticatedViewController];
        
//        else if (indexPath.row == 4) {
//            PremiumViewController *premiumVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"premiumVC"];
//            [self presentViewController:premiumVC animated:YES completion:nil];
//        }
    }
    else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            AboutUsViewController *aboutUsVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"aboutUsVC"];
            WexNavigationController *navController = [[WexNavigationController alloc] initWithRootViewController:aboutUsVC];
            
            [self presentViewController:navController animated:YES completion:nil];
            
            //Firebase Analytics
            [[FirebaseAnalytics sharedManager] logEventWithName:FIR_ACTION_TOUCH_EVENT_ON_ABOUT_US parameters:nil];
        }
    }
    
    // Close left menu
    [[NSNotificationCenter defaultCenter] postNotificationName:TOOGLE_LEFT_MENU object:nil];
}

#pragma mark <UITableViewDataSource>
/**
 ----------------------------------------------------------------------------------------------------
 * @brief numberOfSectionsInTableView
 ----------------------------------------------------------------------------------------------------
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}


/**
 ----------------------------------------------------------------------------------------------------
 * @brief numberOfRowsInSection
 ----------------------------------------------------------------------------------------------------
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 2;
    }
    else if (section == 1) {
        return 3;
    }
    else if (section == 2) {
        return 1;
    }
    else {
        return 0;
    }
}



/**
 ----------------------------------------------------------------------------------------------------
 * @brief cellForRowAtIndexPath
 ----------------------------------------------------------------------------------------------------
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ItemMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ItemMenuTableViewCell"];
    if (cell == nil) {
        cell = [[ItemMenuTableViewCell alloc] init];
    }
    
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
                cell.lbTitle.text =  @"PIN SETTING";
                cell.lblLastPrice.text = @"";
                break;
            case 1:
                cell.lbTitle.text =  @"API KEY SETTING";
                cell.lblLastPrice.text = @"";
                break;
            default:
                break;
        }
    }
    else if (indexPath.section == 1) {
        switch (indexPath.row) {
//            case 0:
//                cell.lbTitle.text =  @"API KEY SETTING";
//                cell.lblLastPrice.text = @"";
//                break;
            case 0:
                cell.lbTitle.text =  @"ACTIVE ORDERS";
                cell.lblLastPrice.text = @"";
                break;
            case 1:
                cell.lbTitle.text =  @"TRADE HISTORY";
                cell.lblLastPrice.text = @"";
                break;
            case 2:
                cell.lbTitle.text =  @"BALANCES";
                cell.lblLastPrice.text = @"";
                break;
//            case 4:
//                cell.lbTitle.text =  @"PREMIUM VERSION";
//                cell.lblLastPrice.text = @"";
//                break;
            default:
                break;
        }
    }
    else if (indexPath.section == 2) {
        switch (indexPath.row) {
            case 0:
                cell.lbTitle.text =  @"ABOUT US";
                cell.lblLastPrice.text = @"";
                break;
            default:
                break;
        }
    }
    return cell;
}

/**
 ----------------------------------------------------------------------------------------------------
 * @brief dealloc
 ----------------------------------------------------------------------------------------------------
 */
- (void)dealloc {
    LogTrace(@"IN");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    LogTrace(@"OUT");
}

- (void) presentAuthenticatedViewController {
    if (self.itemSelected == ActiveOrdersSelected) {
        
        //Firebase Analytics
        [[FirebaseAnalytics sharedManager] logEventWithName:FIR_ACTION_TOUCH_EVENT_ON_ACTIVE_ORDER parameters:nil];
        
        ActiveOrdersViewController *activeOrdersVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"activeOrdersVC"];
        [self presentViewController:activeOrdersVC animated:YES completion:nil];
    }
    else if (self.itemSelected == TradeHistorySelected) {
        
        //Firebase Analytics
        [[FirebaseAnalytics sharedManager] logEventWithName:FIR_ACTION_TOUCH_EVENT_ON_TRADE_HISTORY parameters:nil];
        
        TradeHistoryViewController *TradesVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"tradeHistoryVC"];
        [self presentViewController:TradesVC animated:YES completion:nil];
    }
    else if (self.itemSelected == BalancesSelected) {
        
        //Firebase Analytics
        [[FirebaseAnalytics sharedManager] logEventWithName:FIR_ACTION_TOUCH_EVENT_ON_BALANCE parameters:nil];
        
        FundsViewController *fundsVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"fundsVC"];
        [self presentViewController:fundsVC animated:YES completion:nil];
    }
    else if (self.itemSelected == APIKeySelected) {
        
        //Firebase Analytics
        [[FirebaseAnalytics sharedManager] logEventWithName:FIR_ACTION_TOUCH_EVENT_ON_API_KEY_SETTING parameters:nil];
        
        ApiKeyInputViewController *apiKeyInputVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"apiKeyInputVC"];
        [self presentViewController:apiKeyInputVC animated:YES completion:nil];
    }
    // Close left menu
    [[NSNotificationCenter defaultCenter] postNotificationName:TOOGLE_LEFT_MENU object:nil];
}

#pragma mark - TOPasscode delegate

- (BOOL)passcodeViewController:(TOPasscodeViewController *)passcodeViewController isCorrectCode:(NSString *)code
{
    return [code isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:USERDEFAULT_KEY_PASSCODE]];
}

- (void)didTapCancelInPasscodeViewController:(TOPasscodeViewController *)passcodeViewController {
    [passcodeViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)didInputCorrectPasscodeInPasscodeViewController:(TOPasscodeViewController *)passcodeViewController {
    ((AppDelegate*)(UIApplication.sharedApplication.delegate)).isPINInputed = YES;
    [passcodeViewController dismissViewControllerAnimated:YES completion:nil];
    [self presentAuthenticatedViewController];
}

- (void)didPerformBiometricValidationRequestInPasscodeViewController:(TOPasscodeViewController *)passcodeViewController {
    __weak typeof(self) weakSelf = self;
    NSString *reason = @"Touch ID to continue using this app";
    id reply = ^(BOOL success, NSError *error) {
        
        // Touch ID validation was successful
        // (Use this to dismiss the passcode controller and display the protected content)
        if (success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // Create a new Touch ID context for next time
                [weakSelf.authContext invalidate];
                weakSelf.authContext = [[LAContext alloc] init];
                
                // Dismiss the passcode controller
                [weakSelf dismissViewControllerAnimated:YES completion:nil];
                ((AppDelegate*)(UIApplication.sharedApplication.delegate)).isPINInputed = YES;
            });
            return;
        }
        
        // Actual UI changes need to be made on the main queue
        dispatch_async(dispatch_get_main_queue(), ^{
            [passcodeViewController setContentHidden:NO animated:YES];
        });
        
        // The user hit 'Enter Password'. This should probably do nothing
        // but make sure the passcode controller is visible.
        if (error.code == kLAErrorUserFallback) {
            NSLog(@"User tapped 'Enter Password'");
            return;
        }
        
        // The user hit the 'Cancel' button in the Touch ID dialog.
        // At this point, you could either simply return the user to the passcode controller,
        // or dismiss the protected content and go back to a safer point in your app (Like the login page).
        if (error.code == LAErrorUserCancel) {
            NSLog(@"User tapped cancel.");
            return;
        }
        
        // There shouldn't be any other potential errors, but just in case
        NSLog(@"%@", error.localizedDescription);
    };
    
    [passcodeViewController setContentHidden:YES animated:YES];
    
    [self.authContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics localizedReason:reason reply:reply];
}

@end
