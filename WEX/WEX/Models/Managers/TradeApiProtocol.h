//
//  TradeApiProtocol.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/25/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TradeApiProtocol <NSObject>

@required
- (void)updateInfo: (NSData*) data;

@optional
- (void)buy: (NSData*) data;
- (void)sell: (NSData*) data;

@end

//@interface TradeApiProtocol : NSObject
//
//@end
