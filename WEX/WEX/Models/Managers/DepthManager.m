//
//  DepthManager.m
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "DepthManager.h"
#import "WexApiHandler.h"
#import "Const.h"
#import "LoggingCommon.h"
#import "MarketsManager.h"
#import "DialogUtil.h"

@implementation DepthManager

{
    WexApiHandler *apiHandler;
}

+ (DepthManager *)sharedManager {
    static DepthManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


// Initialization

- (id)init {
    self = [super init];
    
    if (self) {
        apiHandler = [[WexApiHandler alloc] init];
    }
    //self.currentPair = DEFAULT_PAIR;
    self.depthInfo = [[DepthInfo alloc] init];
    self.isLoading = NO;
    [self updateDepth];

    return self;
}

- (void)setTimerActive: (BOOL) isStart {
    if (isStart){
        self.tracker = [NSTimer scheduledTimerWithTimeInterval:2.0f
                                                   target:self
                                                 selector:@selector(updateDepth)
                                                 userInfo:nil repeats:YES];
        
    } else {
        [self.tracker invalidate];
    }
}

- (void)updateDepth {
    LogTrace(@"IN")
    // do nothing if currentPair is nil
    if (!self.currentPair) {
        return;
    }
    // start timer if it is not active
    if (![self.tracker isValid]) {
        [self setTimerActive:YES];
    }
    // do not call API if it is waiting
    if (self.isLoading) {
        return;
    }
    
    self.isLoading = YES;
    
    dispatch_async(kBgQueue, ^{
        NSString * urlString = [[WEX_DEPTH_URL stringByAppendingString:self.currentPair] stringByAppendingString:@"?ignore_invalid=1"];
        [self->apiHandler getResponseFromPublicServerUrl: urlString withHandler:^(NSData *response) {
            self.isLoading = NO;
            if (response == nil) {
                return;
            }
            
            NSError* error;
            NSDictionary* json = [NSJSONSerialization
                                  JSONObjectWithData:response
                                  options:kNilOptions
                                  error:&error];
            
            if (error) {
                [DialogUtil showDialogWithError:@"Error" withError:error.domain withButtonName:@"OK" buttonHandler:^{
                    [DialogUtil dismissDialog];
                }];
//                [self updateDepth];
            }
            else {
                [self updateDepthInfoWithJson: self.currentPair withJson: json];
            }

        }];
    });
    LogTrace(@"OUT");
    
}

- (void)updateDepthInfoWithJson: (NSString *)pairString withJson: (NSDictionary *) json {
    
    NSDictionary* depth = [json objectForKey:pairString];
    self.depthInfo.asks = [depth objectForKey:@"asks"];
    self.depthInfo.bids = [depth objectForKey:@"bids"];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:
         DEPTH_MANAGER_UPDATE_DEPTH object:nil userInfo:nil];
    });
}

@end
