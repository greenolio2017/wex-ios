//
//  MarketsViewController.m
//  WEX
//
//  Created by Ta Duong Ngoc on 1/7/18.
//  Copyright © 2018 Ta Duong Ngoc. All rights reserved.
//

#import "MarketsViewController.h"
#import "MarketItemTableViewCell.h"
#import "TickerManager.h"
#import "MarketsManager.h"
#import "TickerInfo.h"
#import "DetailInfoViewController.h"

#import "CommonUtil.h"
#import "Const.h"

#import "FavoriteManager.h"
#import "FirebaseAnalytics.h"

@interface MarketsViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *noPairLabel;
@property NSString *typeOfMarket;
//@property NSMutableArray <MarketSummariesInfo*> *oldMarkets;
//@property NSMutableArray <MarketSummariesInfo*> *btcMarkets;
//@property NSMutableArray<TickerInfo *> *oldTickerInfoArray;
@property NSMutableArray<TickerInfo *> *tickerInfoArray;

@end

@implementation MarketsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.typeOfMarket = [self.title lowercaseString];
    
    // Observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTickerData) name:TICKER_MANAGER_UPDATE_TICKERS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(favoriteListUpdated) name:NOTIFICATION_UPDATE_FAVORITE object:nil];
    
    // hide noPair in all tab except favorite
    if (![self.typeOfMarket isEqualToString:@"fav"]) {
        self.noPairLabel.hidden = YES;
    }
    
    // tableView setup
    [self.tableView registerNib:[UINib nibWithNibName:@"MarketItemTableViewCell" bundle:nil] forCellReuseIdentifier:@"MarketItemTableViewCell"];
    self.tableView.tableFooterView = [[UIView alloc] init];
    
//    self.oldTickerInfoArray = [[NSMutableArray alloc] init];
    self.tickerInfoArray = [[NSMutableArray alloc] init];
    
    // init data
    [self initDataForTableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 ----------------------------------------------------------------------------------------------------
 * @brief dealloc
 ----------------------------------------------------------------------------------------------------
 */
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) initDataForTableView  {
    
    [self.tickerInfoArray removeAllObjects];
    
    // data for all tab
    if ([self.typeOfMarket isEqualToString:@"all"]) {
        self.tickerInfoArray = [[NSMutableArray alloc] initWithArray:[TickerManager sharedManager].tickerInfoArray];
    }
    // data for Favorite tab
    else if ([self.typeOfMarket isEqualToString:@"fav"]) {
        for (NSString *favoritePair in [FavoriteManager sharedManager].favoriteArray) {
            for (TickerInfo *tickerInfo in [TickerManager sharedManager].tickerInfoArray) {
                if ([tickerInfo.pairString isEqualToString:favoritePair]) {
                    [self.tickerInfoArray addObject:tickerInfo];
                }
            }
        }
        // display noPairLable
        self.noPairLabel.text = @"You didn't add any favorites pairs yet";
        if ([self.tickerInfoArray count] > 0) {
            self.noPairLabel.hidden = YES;
            self.tableView.hidden = NO;
        }
        else {
            self.noPairLabel.hidden = NO;
            self.tableView.hidden = YES;
        }
    }
    // data for other tabs
    else {
        for (TickerInfo *tickerInfo in [TickerManager sharedManager].tickerInfoArray) {
            if ([tickerInfo.pairString containsString: [@"_" stringByAppendingString:self.typeOfMarket]]) {
                [self.tickerInfoArray addObject:tickerInfo];
            }
        }
    }
//
//    self.oldTickerInfoArray = [[NSMutableArray alloc] initWithArray:self.tickerInfoArray];
//    self.tickerInfoArray = marketsTemp;
//
//    if ([self.oldTickerInfoArray count] <= 0) {
//        self.oldTickerInfoArray = [[NSMutableArray alloc] initWithArray:self.tickerInfoArray];
//    }
    
    [self.tableView reloadData];
}

- (void) updateTickerData {
    [self initDataForTableView];
}

- (void) favoriteListUpdated {
//    // update data for Favorite tab only
//    if ([self.typeOfMarket isEqualToString:@"frvt"]) {
//        for (NSString *favoritePair in [FavoriteManager sharedManager].favoriteArray) {
//            for (TickerInfo *tickerInfo in [TickerManager sharedManager].tickerInfoArray) {
//                if ([tickerInfo.pairString isEqualToString:favoritePair]) {
//                    [self.tickerInfoArray addObject:tickerInfo];
//                }
//            }
//        }
//    }
//    // reload table view
//    [self.tableView reloadData];
    [self initDataForTableView];
}

#pragma mark <UITableViewDelegate>
/**
 ----------------------------------------------------------------------------------------------------
 * @brief heightForRowAtIndexPath
 ----------------------------------------------------------------------------------------------------
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

/**
 ----------------------------------------------------------------------------------------------------
 * @brief didSelectRowAtIndexPath
 ----------------------------------------------------------------------------------------------------
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    NSDictionary* userInfo = [NSDictionary dictionaryWithObject:
//                              [NSNumber numberWithInteger:indexPath.row]
//                                                         forKey:@"Index"];
    NSString *pairString = [self.tickerInfoArray objectAtIndex:indexPath.row].pairString;
    [CommonUtil updateCurrentPairInManagers:pairString];
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [[NSNotificationCenter defaultCenter] postNotificationName:
//         PAIRS_CHANGED object:nil userInfo:userInfo];
//    });
//
//    [self dismissViewControllerAnimated:NO completion:nil];
    DetailInfoViewController *mainTabVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"mainTabVC"];
    NSString *pairStr = pairString;
    mainTabVC.currentPair = pairStr;
    [self presentViewController:mainTabVC animated:YES completion:nil];
    
    [[FirebaseAnalytics sharedManager] logEventWithName:FIR_ACTION_TOUCH_EVENT_ON_SELECT_PAIR parameters:nil];
}

#pragma mark <UITableViewDataSource>
/**
 ----------------------------------------------------------------------------------------------------
 * @brief numberOfSectionsInTableView
 ----------------------------------------------------------------------------------------------------
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


/**
 ----------------------------------------------------------------------------------------------------
 * @brief numberOfRowsInSection
 ----------------------------------------------------------------------------------------------------
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.tickerInfoArray count];
}


/**
 ----------------------------------------------------------------------------------------------------
 * @brief cellForRowAtIndexPath
 ----------------------------------------------------------------------------------------------------
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MarketItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MarketItemTableViewCell"];
    if (cell == nil) {
        cell = [[MarketItemTableViewCell alloc] init];
    }
    
    if ([self.tickerInfoArray count] <= 0) {
        return cell;
    }
    
//    TickerInfo *oldTickerInfo = [self.oldTickerInfoArray objectAtIndex:indexPath.row];
    TickerInfo *newTickerInfo = [self.tickerInfoArray objectAtIndex:indexPath.row];
    
    NSString *pairString = newTickerInfo.pairString;
    cell.pair = pairString;
    
    // Pair title
    cell.marketCurrency.text = [CommonUtil pairStringToStringDisplay:pairString];
    
    // Volumn texts
    cell.volumnLabel.text = [@"Vol " stringByAppendingString:[CommonUtil dealWithDoubleWithEightDecimal:[newTickerInfo.vol doubleValue]]];
    
    // Last prices
//    NSString *oldLast = [CommonUtil dealWithDoubleWithEightDecimal:[oldTickerInfo.last doubleValue]];
    NSString *newLast = [CommonUtil dealWithDoubleWithEightDecimal:[newTickerInfo.last doubleValue]];
    [cell.lastPriceLabel setTextNumber: newLast withNewText:newLast withTypeBackground: NO];
    
    // Low price
    NSString *lowPriceStr = [CommonUtil dealWithDoubleWithEightDecimal:[newTickerInfo.low doubleValue]];
    cell.lowLabel.text = lowPriceStr;
    
    // High price
    NSString *hightPriceStr = [CommonUtil dealWithDoubleWithEightDecimal:[newTickerInfo.high doubleValue]];
    cell.highLabel.text = hightPriceStr;
    
    // Favorite icon
    if ([[FavoriteManager sharedManager] isExistInList:pairString]) {
        cell.favoriteImage.image = [UIImage imageNamed:@"ico_favorite_on"];
    }
    else {
        cell.favoriteImage.image = [UIImage imageNamed:@"ico_favorite_off"];
    }
    
    // Change in 24h
//    double changeIn24h = 100* ([marketSummariesInfo.Last doubleValue] - [marketSummariesInfo.PrevDay doubleValue]) / [marketSummariesInfo.PrevDay doubleValue];
//    NSString *change24hStr = [[CommonUtil dealWithDoubleWithTwoDecimal:changeIn24h] stringByAppendingString:@"%"];
//
//    [cell.changePrevLabel setText:change24hStr];
//    if (changeIn24h < 0) {
//        cell.changePrevLabel.backgroundColor = COLOR_RED_ALPHA;
//    }
//    else if (changeIn24h > 0) {
//        cell.changePrevLabel.backgroundColor = COLOR_GREEN_ALPHA;
//    }
//    else {
//        cell.changePrevLabel.backgroundColor = COLOR_BLACK_COMMON;
//    }
    
    return cell;
}

- (IBAction)backButtonTouchUpInside:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}


@end
