//
//  TickerInfo.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TickerInfo : NSObject
@property (nonatomic) NSString * pairString;
@property (nonatomic) NSNumber * high;
@property (nonatomic) NSNumber * low;
@property (nonatomic) NSNumber * avg;
@property (nonatomic) NSNumber * vol;
@property (nonatomic) NSNumber * vol_cur;
@property (nonatomic) NSNumber * last;
@property (nonatomic) NSNumber * buy;
@property (nonatomic) NSNumber * sell;
@property (nonatomic) NSNumber * updated;

- (instancetype)initWithPairString: (NSString *) pairString
                              high: (NSNumber *) high
                               low: (NSNumber *) low
                               avg: (NSNumber *) avg
                               vol: (NSNumber *) vol
                           vol_cur: (NSNumber *) vol_cur
                              last: (NSNumber *) last
                               buy: (NSNumber *) buy
                              sell: (NSNumber *) sell
                           updated: (NSNumber *) updated;

- (void)updateTickerInfo: (NSNumber *) high
                             low: (NSNumber *) low
                             avg: (NSNumber *) avg
                             vol: (NSNumber *) vol
                         vol_cur: (NSNumber *) vol_cur
                            last: (NSNumber *) last
                             buy: (NSNumber *) buy
                            sell: (NSNumber *) sell
                         updated: (NSNumber *) updated;

@end
