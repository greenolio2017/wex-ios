//
//  TradesInfo.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/19/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TradesInfo : NSObject
@property (nonatomic) NSString * type;
@property (nonatomic) NSNumber * price;
@property (nonatomic) NSNumber * amount;
@property (nonatomic) NSNumber * tid;
@property (nonatomic) NSNumber * timestamp;

- (instancetype)initWith: (NSString *) type
                   price: (NSNumber *) price
                  amount: (NSNumber *) amount
                     tid: (NSNumber *) tid
               timestamp: (NSNumber *) timestamp;
    
@end
