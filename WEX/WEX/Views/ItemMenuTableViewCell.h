//
//  TradeInfoViewController.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/16/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemMenuTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblLastPrice;

@end
