//
//  GraphDataManager.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/16/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GraphDataManager : NSObject

@property NSString *currentHTMLString;
@property NSString *currentChartData;
@property NSString *currentPair;

+ (GraphDataManager *)sharedManager;
- (void)getHTMLStringFromURL: (NSString*) pair;
- (void)setTimerActive: (BOOL) isStart;

@end
