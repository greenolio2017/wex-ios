//
//  LoadingView.m
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/27.
//  Copyright © 2017年 Ta Duong Ngoc. All rights reserved.
//

#import "LoadingView.h"
#import <QuartzCore/QuartzCore.h>
#import "LoggingCommon.h"


// const
const CGFloat LOADING_ANIMATION_DURATION = 1.133;

CGSize const SIZE_LOADING = {60, 30};

@interface LoadingView ()

@property (strong, nonatomic) UIImageView *imageLoading;
//@property (nonatomic) BOOL isLoading;

@end

@implementation LoadingView


- (instancetype)initWithFrame:(CGRect)frame {
    LogTrace(@"IN");
    self = [super initWithFrame:frame];
    if (self) {

        self.clipsToBounds = NO;
        
        self.imageLoading = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SIZE_LOADING.width, SIZE_LOADING.height)];
    
        NSArray *arrImageLoading = @[
                                     [UIImage imageNamed:@"loading_01"],
                                     [UIImage imageNamed:@"loading_02"],
                                     [UIImage imageNamed:@"loading_03"],
                                     [UIImage imageNamed:@"loading_04"],
                                     [UIImage imageNamed:@"loading_05"],
                                     [UIImage imageNamed:@"loading_06"],
                                     [UIImage imageNamed:@"loading_07"],
                                     [UIImage imageNamed:@"loading_08"],
                                     [UIImage imageNamed:@"loading_09"],
                                     [UIImage imageNamed:@"loading_10"],
                                     [UIImage imageNamed:@"loading_11"],
                                     [UIImage imageNamed:@"loading_12"],
                                     [UIImage imageNamed:@"loading_13"],
                                     [UIImage imageNamed:@"loading_14"],
                                     [UIImage imageNamed:@"loading_15"],
                                     [UIImage imageNamed:@"loading_16"],
                                     [UIImage imageNamed:@"loading_17"],
                                     [UIImage imageNamed:@"loading_18"],
                                     [UIImage imageNamed:@"loading_19"],
                                     [UIImage imageNamed:@"loading_20"],
                                     [UIImage imageNamed:@"loading_21"],
                                     [UIImage imageNamed:@"loading_22"],
                                     [UIImage imageNamed:@"loading_23"],
                                     [UIImage imageNamed:@"loading_24"],
                                     [UIImage imageNamed:@"loading_25"],
                                     [UIImage imageNamed:@"loading_26"],
                                     [UIImage imageNamed:@"loading_27"],
                                     [UIImage imageNamed:@"loading_28"],
                                     [UIImage imageNamed:@"loading_29"],
                                     [UIImage imageNamed:@"loading_30"],
                                     [UIImage imageNamed:@"loading_31"],
                                     [UIImage imageNamed:@"loading_32"],
                                     [UIImage imageNamed:@"loading_33"]
                                     ];
        
        // load all the frames of our animation
        self.imageLoading.animationImages = arrImageLoading;
        
        // all frames will execute in LOADING_ANIMATION_DURATION seconds
        self.imageLoading.animationDuration = LOADING_ANIMATION_DURATION;
        // repeat the animation forever
        self.imageLoading.animationRepeatCount = 0;
        
        // add the animation view to self
        [self addSubview:self.imageLoading];
        
        self.imageLoading.translatesAutoresizingMaskIntoConstraints = false;
        
        [self addConstraints:@[
                               [NSLayoutConstraint constraintWithItem:self.imageLoading
                                                            attribute:NSLayoutAttributeCenterX
                                                            relatedBy:NSLayoutRelationEqual
                                                               toItem:self
                                                            attribute:NSLayoutAttributeCenterX
                                                           multiplier:1 constant:0],
                               [NSLayoutConstraint constraintWithItem:self.imageLoading
                                                            attribute:NSLayoutAttributeCenterY
                                                            relatedBy:NSLayoutRelationEqual
                                                               toItem:self
                                                            attribute:NSLayoutAttributeCenterY
                                                           multiplier:1 constant:0],
                               [NSLayoutConstraint constraintWithItem:self.imageLoading
                                                            attribute:NSLayoutAttributeWidth
                                                            relatedBy:NSLayoutRelationEqual
                                                               toItem:nil
                                                            attribute:NSLayoutAttributeNotAnAttribute
                                                           multiplier:1 constant:SIZE_LOADING.width],
                               [NSLayoutConstraint constraintWithItem:self.imageLoading
                                                            attribute:NSLayoutAttributeHeight
                                                            relatedBy:NSLayoutRelationEqual
                                                               toItem:nil
                                                            attribute:NSLayoutAttributeNotAnAttribute
                                                           multiplier:1 constant:SIZE_LOADING.height]
                               ]];

        
        //self.isLoading = NO;
        
        [self layoutIfNeeded];
    }
    LogTrace(@"OUT");
    return self;
}


#pragma mark - Constraints

- (void)setupConstraints {
    LogTrace(@"IN");
    
    if (self.superview) {
        NSDictionary *viewsDictionary = @{
                                          @"loadingView" : self
                                          };
        NSArray *loadingViewConstraintH = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[loadingView]|" options:0 metrics:nil views:viewsDictionary];
        NSArray *loadingViewConstraintV = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[loadingView]|" options:0 metrics:nil views:viewsDictionary];
        [self.superview addConstraints:loadingViewConstraintH];
        [self.superview addConstraints:loadingViewConstraintV];
    }
    
    LogTrace(@"OUT");
}


- (void)loading {
    LogTrace(@"IN");
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.imageLoading startAnimating];
    });

    self.hidden = false;
    //self.isLoading = YES;
    LogTrace(@"OUT");
}


- (void)cancel {
    LogTrace(@"IN");
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.imageLoading stopAnimating];
    });
    
    
    self.hidden = true;
    //self.isLoading = NO;
    LogTrace(@"OUT");
}



- (void)applicationDidEnterBackground {
    LogTrace(@"applicationDidEnterBackground");
    [self.imageLoading stopAnimating];
    LogTrace(@"OUT");
}


- (void)applicationWillEnterForeground {
    LogTrace(@"applicationWillEnterForeground");
    //if (self.isLoading) {
        [self.imageLoading startAnimating];
        
    //}
    LogTrace(@"OUT");
}

@end
