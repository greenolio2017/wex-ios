//
//  DepthManager.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DepthInfo.h"

@interface DepthManager : NSObject
@property DepthInfo *depthInfo;
@property BOOL isLoading;
@property NSString *currentPair;
@property NSTimer *tracker;

+ (DepthManager *)sharedManager;

//- (void)setTimerActive: (BOOL) isStart;
- (void)updateDepth;

@end
