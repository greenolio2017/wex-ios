//
//  MarketsManager.m
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "MarketsManager.h"
#import "WexApiHandler.h"
#import "Const.h"
#import "DialogUtil.h"

@implementation MarketsManager
{
    WexApiHandler *apiHandler;
}

+ (MarketsManager*)sharedManager {
    static MarketsManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


// Initialization

- (id)init {
    self = [super init];
    if (self) {
        apiHandler = [[WexApiHandler alloc] init];
    }
    
    self.marketInfoArray = [[NSMutableArray alloc] init];
    
    return self;
}

- (void)updateAllMarkets{
    dispatch_async(kBgQueue, ^{
        NSString * urlString = WEX_INFO_URL;
        [self->apiHandler getResponseFromPublicServerUrl: urlString withHandler:^(NSData *response) {
            
            if (response == nil) {
                [self updateAllMarkets];
                return;
            }
            
            NSError* error;
            NSDictionary* json = [NSJSONSerialization
                                  JSONObjectWithData:response
                                  options:kNilOptions
                                  error:&error];
            
            if (error) {
                [DialogUtil showDialogWithError:@"Error" withError:error.domain withButtonName:@"OK" buttonHandler:^{
                    [DialogUtil dismissDialog];
                }];
                [self updateAllMarkets];
            }
            else {
                [self updateAllMarketsInfoWithJson: json];
            }
        }];
    });
}

- (void)updateAllMarketsInfoWithJson: (NSDictionary *) json {
    
    NSDictionary *pairs = [json objectForKey:@"pairs"];
    
    NSArray *sortedKeys = [[pairs allKeys] sortedArrayUsingSelector: @selector(compare:)];
    
    NSMutableArray<MarketInfo *> *marketInfoArray = [[NSMutableArray alloc] init];
    for (NSString *key in sortedKeys) {
        NSString * pairString = key;
        
        NSDictionary *pair = [pairs objectForKey:key];
        NSNumber * decimal_places = [pair objectForKey:@"decimal_places"];
        NSNumber * min_price = [pair objectForKey:@"min_price"];
        NSNumber * max_price = [pair objectForKey:@"max_price"];
        NSNumber * min_amount = [pair objectForKey:@"min_amount"];
        NSNumber * hidden = [pair objectForKey:@"hidden"];
        NSNumber * fee  = [pair objectForKey:@"fee"];
        
        MarketInfo *marketInfo = [[MarketInfo alloc] initWithPairString:pairString decimal_places:decimal_places min_price:min_price max_price:max_price min_amount:min_amount hidden:hidden fee:fee];
        
        [marketInfoArray addObject:marketInfo];

    }
    
    self.marketInfoArray = marketInfoArray;
    
    dispatch_async(dispatch_get_main_queue(), ^{
//        [[NSNotificationCenter defaultCenter] postNotificationName:
//         MARKETS_MANAGER_UPDATE_MARKETS object:nil userInfo:nil];
        [self.delegate marketsUpdated];
    });
}

@end
