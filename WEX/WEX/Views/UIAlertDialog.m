//
//  UIAlertDialog.m
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/27.
//  Copyright © 2017年 Ta Duong Ngoc. All rights reserved.
//

#import "UIAlertDialog.h"
#import "UICustomAlertWindow.h"

#pragma mark - UIAlertDialog
@interface UIAlertDialog ()

@property (nonatomic) UICustomAlertWindow *alertWindow;
@property (nonatomic) BOOL isShown;

@end

@implementation UIAlertDialog

static UIAlertDialog *instance;

+ (UIAlertDialog *)sharedManager {
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        instance = [[UIAlertDialog alloc] init];
    });
    return instance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.alertWindow = nil;
    }
    self.isShown = NO;
    return self;
}


- (void)alertWithTitle:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelButtonTitle cancelHandler:(void (^)(void))cancelHandler otherButtonTitle:(NSString *)otherButtonTitle otherHandler:(void (^)(void))otherHandler {
    //[self dismiss];
    if (self.isShown) {
        return;
    }

    @synchronized(self) {

        self.alertWindow = [[UICustomAlertWindow alloc] initWithTitle:(title ? title : @"")message:message
            cancelButtonTitle:cancelButtonTitle
            cancelHandler:^() {
                if (cancelHandler) {
                    cancelHandler();
                }

                if (self.alertWindow) {
                    [self.alertWindow dismiss:NO];
                    self.alertWindow = nil;
                }
            }
            otherButtonTitle:otherButtonTitle
            otherHandler:^() {
                if (otherHandler) {
                    otherHandler();
                }

                if (self.alertWindow) {
                    [self.alertWindow dismiss:NO];
                    self.alertWindow = nil;
                }
            }
            windowLevelOffset:WindowLevelOffsetAlert];
    }
}


- (void)alertWithTitle:(NSString *)title message:(NSString *)message buttonTitle:(NSString *)buttonTitle buttonHandler:(void (^)(void))buttonHandler {
    //[self dismiss];
    if (self.isShown) {
        return;
    }

    @synchronized(self) {

        self.alertWindow = [[UICustomAlertWindow alloc] initWithTitle:(title ? title : @"")message:message
                                                    cancelButtonTitle:buttonTitle
                                                        cancelHandler:^() {
                                                            if (buttonHandler) {
                                                                buttonHandler();
                                                            }

                                                            if (self.alertWindow) {
                                                                [self.alertWindow dismiss:NO];
                                                                self.alertWindow = nil;
                                                            }
                                                        }
                                                     otherButtonTitle:nil
                                                         otherHandler:nil
                                                    windowLevelOffset:WindowLevelOffsetAlert];
    }
}


- (void)alertInputTextWithTitle:(NSString *)title
                        message:(NSString *)message
              cancelButtonTitle:(NSString *)cancelButtonTitle
                  cancelHandler:(void (^)(void))cancelHandler
               otherButtonTitle:(NSString *)otherButtonTitle
                   otherHandler:(void (^)(void))otherHandler
                      optionDic:(NSDictionary *)optionDic {
    //[self dismiss];
    if (self.isShown) {
        return;
    }

    @synchronized(self) {
        self.alertWindow = [[UICustomAlertWindow alloc] initWithTitle:(title ? title : @"")message:message
            cancelButtonTitle:cancelButtonTitle
            cancelHandler:^() {
                if (cancelHandler) {
                    cancelHandler();
                }

                if (self.alertWindow) {
                    [self.alertWindow dismiss:NO];
                    self.alertWindow = nil;
                }
            }
            otherButtonTitle:otherButtonTitle
            otherHandler:^() {
                if (otherHandler) {
                    otherHandler();
                }

                if (self.alertWindow) {
                    [self.alertWindow dismiss:NO];
                    self.alertWindow = nil;
                }
            }
            windowLevelOffset:WindowLevelOffsetAlert];

        self.alertWindow.maxTextSize = [optionDic[@"maxTextSize"] integerValue];
        self.alertWindow.minTextSize = [optionDic[@"minTextSize"] integerValue];
        self.alertWindow.appearOtherBtn = [optionDic[@"appearOtherBtn"] boolValue];
        [self.alertWindow setTextField:YES placeHolder:optionDic[@"placeHolder"]];
    }
}


- (void)show {
    self.isShown = YES;
    @synchronized(self) {
        if (self.alertWindow && ![self.alertWindow isShown]) {
            [self.alertWindow show];
        }
    }
}


- (void)dismiss {
    self.isShown = NO;
    @synchronized(self) {
        if (self.alertWindow && [self.alertWindow isShown]) {
            [self.alertWindow dismiss:NO];
            self.alertWindow = nil;
        }
    }
}


- (BOOL)isShown {
    return (self.alertWindow && [self.alertWindow isShown]);
}


- (NSString *)text {
    if (self.alertWindow) {
        return (self.alertWindow.textField ? self.alertWindow.textField.text : @"");
    }
    return @"";
}

@end
