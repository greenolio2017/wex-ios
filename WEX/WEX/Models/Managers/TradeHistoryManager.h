//
//  TradeHistoryManager.m
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/29.
//  Copyright © 2017年 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TradeHistoryApiProtocol.h"
#import "WexApiHandler.h"

@interface TradeHistoryManager : NSObject <TradeHistoryApiProtocol>
@property NSString *currentPair;
@property NSMutableArray *tradeHistories;
@property WexApiHandler *apiHandler;

+ (TradeHistoryManager *)sharedManager;
- (void)updateTradeHistory;

@end
