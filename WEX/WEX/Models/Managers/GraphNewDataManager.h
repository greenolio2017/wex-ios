//
//  GraphNewDataManager.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/16/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>

@interface GraphNewDataManager : NSObject

+ (GraphNewDataManager *)sharedManager;
- (NSString *)getHTMLStringFromURL: (NSString*) pair;

@end
