
//
//  UICustomAlertViewController.h
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/27.
//  Copyright © 2017年 Ta Duong Ngoc. All rights reserved.
//

#ifndef UICustomAlertWindow_h
#define UICustomAlertWindow_h
#import "LoggingCommon.h"
#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>


typedef void (^UIAlertButtonHandler)(void);
static NSInteger const WindowLevelOffsetAlert = 2;
static NSInteger const WindowLevelOffsetAlarm = 1000;
static NSInteger const WindowLevelOffsetCheck = 9;

@interface UICustomAlertWindow : UIWindow <UITextFieldDelegate>

@property (nonatomic) NSInteger maxTextSize;
@property (nonatomic) NSInteger minTextSize;
@property (strong, nonatomic) UITextField *textField;

@property (nonatomic, getter=isAppearOtherBtn) BOOL appearOtherBtn;

- (id)initWithTitle:(NSString *)title
              message:(NSString *)message
    cancelButtonTitle:(NSString *)cancelButtonTitle
      cancelHandler:(void (^)(void))cancelHandler
     otherButtonTitle:(NSString *)otherButtonTitle
       otherHandler:(void (^)(void))otherHandler
    windowLevelOffset:(NSInteger const)windowLevelOffset;
- (void)setTextField:(BOOL)mask placeHolder:(NSString *)placeHolder;
- (void)show;
- (void)dismiss:(BOOL)animated;
- (void)cancel;
- (BOOL)isShown;
- (void)closeWindow;
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

@end

#endif
