//
//  AppDelegate.m
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (nonatomic) NSString *backIconImageName;

@property (assign, nonatomic) BOOL isLoadSuccess;

@end
