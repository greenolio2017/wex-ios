//
//  GuideViewController.m
//  WEX
//
//  Created by Ta Duong Ngoc on 12/1/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "GuideViewController.h"
#import "LoadingViewController.h"
#import "LoggingCommon.h"
#import "Const.h"

@interface GuideViewController ()<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *carouselActiveImageView;

@end

@implementation GuideViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    LogTrace(@"IN");
    self.scrollView.delegate = self;
    self.scrollView.decelerationRate = UIScrollViewDecelerationRateFast;
    LogTrace(@"OUT");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    LogTrace(@"IN");
    LogTrace(@"OUT");
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}


#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    LogTrace(@"IN");
    NSUInteger currentIndex = (NSUInteger)(scrollView.contentOffset.x / scrollView.bounds.size.width);
    NSString *imageName = [NSString stringWithFormat:@"Carousel_%ld", (long)(currentIndex + 1)];
    UIImage *image = [UIImage imageNamed:imageName];
    if (image == nil) {
        LogWarning(@"Error index = %ld", (long)currentIndex);
    }
    else {
        self.carouselActiveImageView.image = image;
    }
    LogTrace(@"OUT");
}

#pragma mark - Touch Event
/**
 -------------------------------------------------------------------
 * @brief touchUpInsideButtonNext
 * @param sender
 -------------------------------------------------------------------
 */
- (IBAction)touchUpInsideStartButton:(id)sender {
    LogTrace(@"IN");
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LoadingViewController *loadingVC = [storyboard instantiateViewControllerWithIdentifier:@"loadingVC"];
    [self presentViewController:loadingVC animated:NO completion:nil];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"STARTED" forKey:USERDEFAULT_FIRST_STARTED];
    
    LogTrace(@"OUT");
}


@end
