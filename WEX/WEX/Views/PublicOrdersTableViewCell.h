//
//  PublicOrdersTableViewCell.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/16/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol PublicOrdersTableViewCellDelegate <NSObject>
@optional
- (void) sellSectionTouchedUp: (UITableViewCell*) cell;
- (void) buySectionTouchedUp: (UITableViewCell*) cell;
@end


@interface PublicOrdersTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *sellAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *sellPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *buyPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *buyAmountLabel;
@property (weak, nonatomic) IBOutlet UIButton *sellSectionButton;
@property (weak, nonatomic) IBOutlet UIButton *buySectionButton;
@property (weak, nonatomic, nullable) id<PublicOrdersTableViewCellDelegate> delegate;


@end
