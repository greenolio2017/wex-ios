# WEX client iOS
iOS client for WEX exchange (WEX.nz)


# Functionalities
Here are the main supported functionalities:

### 1. Pair list
+ Pair list is updated automatically. If server add new pair list. It will be added automatically in this app

### 2. Favorite pair
+ Pair is categorized by BTC, ETH, USD, ALL. A new tab for favorite pairs is added "FVR"

### 3. Public Orders / Public Trade.
+ Monitoring the Orders and Depths for all pair.

### 4. Trade (Buy / Sell)
+ Support buy/sell all trade
+ When user click on the Order table on Sell side, all the orders above it will be summed. And "Buy" screen will be displayed with the value summed.
+ When user click on the Order table on Buy side, all the orders above it will be summed. And "Sell" screen will be displayed with the value summed.

### 5. Active Orders.
+ User can see all current active Orders

### 6. Cancel Orders.
+ User can cancel each order or all current active orders.

### 7. Trade History
+ User can see the history of his trade

### 8. Balances.
+ See the current balances.

### 9. PIN
+ For protect API Key.

### 10. Google analytics
+ Google analytics is integrated to analyze user interaction on app.
+ The following action is logged:

	FIR_DISPLAY_SCREEN_PAIR_LIST = @"display_pair_list_screen";

	FIR_DISPLAY_SCREEN_DETAIL_INFO = @"display_detail_info_screen";

	FIR_DISPLAY_SCREEN_BUY_SELL_INFO = @"display_buy_sell_screen";

	FIR_ACTION_TOUCH_EVENT_ON_PIN_SETTING = @"touched_pin_setting";

	FIR_ACTION_TOUCH_EVENT_ON_API_KEY_SETTING = @"touched_api_key_setting";

	FIR_ACTION_TOUCH_EVENT_ON_ACTIVE_ORDER = @"touched_active_order_menu";

	FIR_ACTION_TOUCH_EVENT_ON_TRADE_HISTORY = @"touched_trade_history_menu";

	FIR_ACTION_TOUCH_EVENT_ON_BALANCE = @"touched_balances_menu";

	FIR_ACTION_TOUCH_EVENT_ON_ABOUT_US= @"touched_about_us_menu";

	FIR_ACTION_TOUCH_EVENT_ON_CHANGE_PAIR_TAB= @"touched_change_pair_tab";

	FIR_ACTION_TOUCH_EVENT_ON_SELECT_PAIR= @"touched_select_a_pair_on_tab";

	FIR_ACTION_TOUCH_EVENT_ON_BUY_SELL_BUTTON= @"touched_buy_sell_button";

	FIR_ACTION_TOUCH_EVENT_ON_CELL_ON_ORDER_TABLE= @"touched_cell_on_order_table";

	FIR_ACTION_TOUCH_EVENT_ON_BUY_BUTTON= @"touched_buy_button";

	FIR_ACTION_TOUCH_EVENT_ON_SELL_BUTTON= @"touched_sell_button";





