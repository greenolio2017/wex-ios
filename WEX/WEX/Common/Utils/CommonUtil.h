//
//  CommonUtil.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/14/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

// for PIN
#import "TOPasscodeViewController.h"
#import "TOPasscodeViewControllerConstants.h"
#import <LocalAuthentication/LocalAuthentication.h>
#import "AppDelegate.h"

@interface CommonUtil : NSObject

+ (NSString *) pairStringToStringDisplay: (NSString*) pairString;

+ (NSString *) askCurrencyFromPairString: (NSString*) pairString;
+ (NSString *) askCurrencyFromPairStringForDisplaying: (NSString*) pairString;

+ (NSString *) bidCurrencyFromPairString: (NSString*) pairString;
+ (NSString *) bidCurrencyFromPairStringForDisplaying: (NSString*) pairString;

+ (NSString *)dealWithDoubleWithFiveDecimal:(double)doubleValue;
+ (NSString *)dealWithDoubleWithEightDecimal:(double)doubleValue;

+ (void) setCornerAndShadow: (UIView *) view withCornerRadius: (float) cornerRadius;

+ (void)updateCurrentPairInManagers: (NSString *)pair;
+ (void)updateApiKeyForAllManagers;

+ (UIViewController*) topMostController;

+ (BOOL) isPINCheckAlready;
+ (void) displayPINScreen: (UIViewController<TOPasscodeViewControllerDelegate>*)viewController withContext: (LAContext *)authContext;

@end
