//
//  ChartViewController.m
//  WEX
//
//  Created by Ta Duong Ngoc on 12/15/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "ChartViewController.h"
#import <WebKit/WebKit.h>
#import "AppDelegate.h"
#import "GraphNewDataManager.h"
#import "Const.h"

@interface ChartViewController ()
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property(nonatomic) WKWebView *wkWebview;
@property float webContentHeight;
@property float webContentViewPortHeight;
@property NSString *htmlString;
@property BOOL isLoaded;
@end

@implementation ChartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isLoaded = NO;
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    // do not reload again
    if (self.isLoaded == YES) {
        return;
    }
    // init wkWebview
    self.wkWebview = [[WKWebView alloc] init] ;
    self.wkWebview.frame = self.contentView.frame;
    self.wkWebview.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.wkWebview.scrollView setScrollEnabled:NO];
    [self.contentView addSubview: self.wkWebview];
    
    [self configWebSize];
    [self loadHtmlString];
    self.isLoaded = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 ----------------------------------------------------------------------------------------------------
 * @brief dealloc
 ----------------------------------------------------------------------------------------------------
 */
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)configWebSize {
    self.webContentHeight = 500 * self.heightForChart / self.widthForChart;
    self.webContentViewPortHeight = self.webContentHeight + 30;
}

- (void)loadHtmlString {
    NSString *htmlStringFromFile = [[GraphNewDataManager sharedManager] getHTMLStringFromURL:self.currentPair];
    self.htmlString = [htmlStringFromFile stringByReplacingOccurrencesOfString:@"###HEIGHT###" withString: [NSString stringWithFormat:@"%f", self.webContentHeight]];
    self.htmlString = [self.htmlString stringByReplacingOccurrencesOfString:@"###HEIGHT_VIEWPORT###" withString: [NSString stringWithFormat:@"%f", self.webContentViewPortHeight]];
    
    [self.wkWebview loadHTMLString:self.htmlString baseURL:nil];
}

@end
