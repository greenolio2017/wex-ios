//
//  WexApiHandler.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TradeApiProtocol.h"
#import "OrderApiProtocol.h"
#import "TradeHistoryApiProtocol.h"

@interface WexApiHandler : NSObject

@property (nonatomic,strong) NSString *api_key;
@property (nonatomic,strong) NSString *secret_key;

@property (nonatomic, weak) id<TradeApiProtocol> tradeDelegate;
@property (nonatomic, weak) id<OrderApiProtocol> orderDelegate;
@property (nonatomic, weak) id<TradeHistoryApiProtocol> tradeHistoryDelegate;

- (void)getResponseFromServerForPost:(NSDictionary *)postDictionary withApi: (NSString*) api;

- (void)getResponseFromPublicServerUrl:(NSString *)urlString withHandler: (void (^) (NSData *response))handler;

- (void)setupInitialValues;

@end
