//
//  AppDelegate.m
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "AppDelegate.h"
#import "Const.h"
#import "LoggingCommon.h"
#import "TickerManager.h"
#import "TradesManager.h"
#import "FruitIAPHelper.h"

#import "LoadingViewController.h"
#import "TOPasscodeViewController.h"
#import "TOPasscodeViewControllerConstants.h"
#import <LocalAuthentication/LocalAuthentication.h>

#import "PinSettingViewController.h"
#import "WexNavigationController.h"
#import "CommonUtil.h"
#import "DialogUtil.h"

@import Firebase;

@interface AppDelegate () <TOPasscodeViewControllerDelegate>

@property (nonatomic, strong) LAContext *authContext;
@end


@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    // Init authentication context
    self.authContext = [[LAContext alloc] init];
    
    // Init firebase
    [FIRApp configure];
    
    // Check if user input the PIN number or not
    self.isPINInputed = NO;
    
    // Init iAP value
    [FruitIAPHelper sharedInstance];
    
    // start loadingVC if guideview viewed
    if ([[NSUserDefaults standardUserDefaults] stringForKey:USERDEFAULT_FIRST_STARTED]) {
        UIStoryboard *storyboard = self.window.rootViewController.storyboard;
        UIViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"loadingVC"];
        self.window.rootViewController = rootViewController;
        [self.window makeKeyAndVisible];
    }
    
    return YES;
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    self.isPINInputed = FALSE;
}
//
//
- (void)applicationWillEnterForeground:(UIApplication *)application {
    UIViewController *currentTopVC  = [CommonUtil topMostController];
    if ([CommonUtil isPINCheckAlready] == FALSE) {
        [self displayPINScreen: currentTopVC];
        return;
    };
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


- (void) displayPINScreen: (UIViewController*)viewController {
    // Do not display if current view controller is TOPPasscodeViewController
    if ([viewController isKindOfClass:TOPasscodeViewController.class] ||
        [viewController isKindOfClass:LoadingViewController.class]) {
        return;
    }
    
    TOPasscodeType passcodeType = [[[NSUserDefaults standardUserDefaults] objectForKey:USERDEFAULT_KEY_PASSCODE_TYPE] integerValue];
    TOPasscodeViewController *passcodeViewController = [[TOPasscodeViewController alloc] initWithStyle:TOPasscodeViewStyleOpaqueLight passcodeType:passcodeType];
    passcodeViewController.delegate = self;
    
    BOOL faceIDAvailable = NO;
    BOOL biometricsAvailable = [self.authContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:nil];
    // Show 'Touch ID' button if it's available
    if (@available(iOS 11.0, *)) {
        faceIDAvailable = (self.authContext.biometryType == LABiometryTypeFaceID);
    }
    
    passcodeViewController.allowBiometricValidation = biometricsAvailable;
    passcodeViewController.biometryType = faceIDAvailable ? TOPasscodeBiometryTypeFaceID : TOPasscodeBiometryTypeTouchID;
    [viewController presentViewController:passcodeViewController animated:YES completion:nil];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"WEX"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    LogTrace(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        LogTrace(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

#pragma mark - TOPasscode delegate

- (BOOL)passcodeViewController:(TOPasscodeViewController *)passcodeViewController isCorrectCode:(NSString *)code
{
    return [code isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:USERDEFAULT_KEY_PASSCODE]];
}

- (void)didTapCancelInPasscodeViewController:(TOPasscodeViewController *)passcodeViewController {
    [DialogUtil showDialogWithError:@"Error" withError:@"You must input PIN to continue. Otherwise, please restart application!" withButtonName:@"OK" buttonHandler:^{
        [DialogUtil dismissDialog];
    }];
}

- (void)didInputCorrectPasscodeInPasscodeViewController:(TOPasscodeViewController *)passcodeViewController {
    self.isPINInputed = TRUE;
    [passcodeViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)didPerformBiometricValidationRequestInPasscodeViewController:(TOPasscodeViewController *)passcodeViewController {
    __weak typeof(self) weakSelf = self;
    NSString *reason = @"Touch ID to continue using this app";
    id reply = ^(BOOL success, NSError *error) {
        
        // Touch ID validation was successful
        // (Use this to dismiss the passcode controller and display the protected content)
        if (success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // Create a new Touch ID context for next time
                [weakSelf.authContext invalidate];
                weakSelf.authContext = [[LAContext alloc] init];
                
                // Dismiss the passcode controller
                [passcodeViewController dismissViewControllerAnimated:YES completion:nil];
                weakSelf.isPINInputed = YES;
            });
            return;
        }
        
        // Actual UI changes need to be made on the main queue
        dispatch_async(dispatch_get_main_queue(), ^{
            [passcodeViewController setContentHidden:NO animated:YES];
        });
        
        // The user hit 'Enter Password'. This should probably do nothing
        // but make sure the passcode controller is visible.
        if (error.code == kLAErrorUserFallback) {
            NSLog(@"User tapped 'Enter Password'");
            return;
        }
        
        // The user hit the 'Cancel' button in the Touch ID dialog.
        // At this point, you could either simply return the user to the passcode controller,
        // or dismiss the protected content and go back to a safer point in your app (Like the login page).
        if (error.code == LAErrorUserCancel) {
            NSLog(@"User tapped cancel.");
            return;
        }
        
        // There shouldn't be any other potential errors, but just in case
        NSLog(@"%@", error.localizedDescription);
    };
    
    [passcodeViewController setContentHidden:YES animated:YES];
    
    [self.authContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics localizedReason:reason reply:reply];
}

- (void)passcodeViewController:(TOPasscodeViewController *)passcodeViewController didResizePasscodeViewToWidth:(CGFloat)width {
    
}
@end
