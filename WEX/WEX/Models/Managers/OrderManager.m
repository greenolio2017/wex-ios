//
//  OrderManager.m
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/29.
//  Copyright © 2017年 Ta Duong Ngoc. All rights reserved.
//

#import "OrderManager.h"
#import "WexApiHandler.h"
#import "Const.h"
#import "DialogUtil.h"
#import "OrderInfo.h"
#import "ProgressDialogUtil.h"
#import "MarketsManager.h"

@implementation OrderManager
{
    NSTimer *tracker;
}

+ (OrderManager *)sharedManager {
    static OrderManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


// Initialization

- (id)init {
    self = [super init];
    
    if (self) {
        self.apiHandler = [[WexApiHandler alloc] init];
    }
    self.currentPair = DEFAULT_PAIR;
    
    self.activeOrders = [[NSMutableArray alloc] init];
    self.ordersInfo = [[NSMutableArray alloc] init];
    
    return self;
}

//- (void)setTimerActive: (BOOL) isStart {
//    if (isStart){
//        tracker = [NSTimer scheduledTimerWithTimeInterval:5.0f
//                                                   target:self
//                                                 selector:@selector(updateActiveOrders)
//                                                 userInfo:nil repeats:YES];
//        
//    } else {
//        [tracker invalidate];
//    }
//}

//- (void)update {
//    [self updateActiveOrders];
//    //    [self updateOrderInfo];
//}

- (void)updateActiveOrders {
    dispatch_async(kBgQueue, ^{
        NSMutableDictionary *post = [[NSMutableDictionary alloc] init];
        [post setObject:@"ActiveOrders" forKey:@"method"];
        self.apiHandler.orderDelegate = self;
        [self.apiHandler getResponseFromServerForPost:post withApi: METHOD_ACTIVE_ORDERS];
    });
}

- (void)updateActiveOrders: (NSString *)pair {
    dispatch_async(kBgQueue, ^{
        NSMutableDictionary *post = [[NSMutableDictionary alloc] init];
        [post setObject:@"ActiveOrders" forKey:@"method"];
        self.apiHandler.orderDelegate = self;
        [self.apiHandler getResponseFromServerForPost:post withApi: METHOD_ACTIVE_ORDERS];
    });
}


- (void)updateOrderInfo: (NSString *) orderId {
    dispatch_async(kBgQueue, ^{
        NSMutableDictionary *post = [[NSMutableDictionary alloc] init];
        [post setObject:@"OrderInfo" forKey:@"method"];
        [post setObject: orderId forKey:@"order_id"];
        self.apiHandler.orderDelegate = self;
        [self.apiHandler getResponseFromServerForPost:post withApi: @"OrderInfo"];
    });
}

- (void)cancelOrder: (NSString *) orderId {
    dispatch_async(kBgQueue, ^{
        NSMutableDictionary *post = [[NSMutableDictionary alloc] init];
        [post setObject:@"CancelOrder" forKey:@"method"];
        [post setObject: orderId forKey:@"order_id"];
        self.apiHandler.orderDelegate = self;
        [self.apiHandler getResponseFromServerForPost:post withApi: METHOD_CANCEL_ORDER];
    });
}

- (void)cancelOrders: (NSArray<NSString*> *) orderIds {
    dispatch_async(kBgQueue, ^{
        NSMutableDictionary *post = [[NSMutableDictionary alloc] init];
        [post setObject:METHOD_CANCEL_ORDER_ALL forKey:@"method"];
        NSString *orderIdsString = @"";
        for (NSString *string in orderIds) {
            orderIdsString = [[orderIdsString stringByAppendingString:string] stringByAppendingString:@","];
        }
        
        NSString *newOrderStrs =  [orderIdsString substringToIndex:[orderIdsString length]-1];
//        NSString *newOrderStrs =  [orderIdsString substringToIndex:([orderIdsString length]-1)];
        
        [post setObject: newOrderStrs forKey:@"order_id"];
        self.apiHandler.orderDelegate = self;
        [self.apiHandler getResponseFromServerForPost:post withApi: METHOD_CANCEL_ORDER_ALL];
    });
}

#pragma Delegates
- (void)activeOrders: (NSData*) data {
    // hide loading
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ProgressDialogUtil sharedManager] hideLoading];
    });
    dispatch_async(kBgQueue, ^{
        if (!data) {
            return;
        }
        NSError *error;
        NSDictionary *json = [NSJSONSerialization
                              JSONObjectWithData:data
                              options:kNilOptions
                              error:&error];
        
        [self.activeOrders removeAllObjects];
        
        // Server error
        if (error != nil && ![error  isEqual: @""]) {
            [DialogUtil showDialogWithError:@"Error" withError:error.domain withButtonName:@"OK" buttonHandler:^{
                [DialogUtil dismissDialog];
            }];
            return;
        }
        
        // Order error
        NSString *errorInResponse = [json objectForKey:@"error"];
        if (errorInResponse != nil &&
            ![errorInResponse  isEqual: @""] &&
            ![[errorInResponse uppercaseString] containsString:@"NO ORDER"]) {
            [DialogUtil showDialogWithError:@"Error" withError:errorInResponse withButtonName:@"OK" buttonHandler:^{
                [DialogUtil dismissDialog];
            }];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:
                 ORDER_ACTIVE_ORDERS object:nil userInfo:nil];
            });
            return;
        }
        
        NSDictionary *jsonInfo = [json objectForKey:@"return"];
        for (NSDictionary *dict in jsonInfo) {
            NSString *key = [NSString stringWithString:(NSString*)dict];
            OrderInfo *orderInfo = [[OrderInfo alloc] init];
            NSDictionary *infoDict = [jsonInfo objectForKey:key];
            
            orderInfo.orderId = key;
            orderInfo.type = [infoDict objectForKey:@"type"];
            orderInfo.start_amount = [infoDict objectForKey:@"amount"];
            orderInfo.pair = [infoDict objectForKey:@"pair"];
            orderInfo.rate = [infoDict objectForKey:@"rate"];
            orderInfo.timestamp_created = [infoDict objectForKey:@"timestamp_created"];
            
            [self.activeOrders addObject:orderInfo];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:
             ORDER_ACTIVE_ORDERS object:nil userInfo:nil];
        });
        
    });
    
}

- (void)orderInfo: (NSData*) data {
    // hide loading
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ProgressDialogUtil sharedManager] hideLoading];
    });
    dispatch_async(kBgQueue, ^{
        if (!data) {
            return;
        }
        NSError *error;
        NSDictionary *json = [NSJSONSerialization
                              JSONObjectWithData:data
                              options:kNilOptions
                              error:&error];
        
        // Server error
        if (error != nil && ![error  isEqual: @""]) {
            [DialogUtil showDialogWithError:@"Error" withError:error.domain withButtonName:@"OK" buttonHandler:^{
                [DialogUtil dismissDialog];
            }];
            return;
        }
        
        // Order error
        NSString *errorInResponse = [json objectForKey:@"error"];
        if (errorInResponse != nil && ![errorInResponse  isEqual: @""]) {
            [DialogUtil showDialogWithError:@"Error" withError:errorInResponse withButtonName:@"OK" buttonHandler:^{
                [DialogUtil dismissDialog];
            }];
            return;
        }
        
        //NSDictionary *jsonInfo = [json objectForKey:@"return"];
        
    });
    
}

- (void)cancelOrderDelegate: (NSData*) data {
    dispatch_async(kBgQueue, ^{
        if (!data) {
            return;
        }
        NSError *error;
        NSDictionary *json = [NSJSONSerialization
                              JSONObjectWithData:data
                              options:kNilOptions
                              error:&error];
        
        // Server error
        if (error != nil && ![error  isEqual: @""]) {
            // hide loading
            dispatch_async(dispatch_get_main_queue(), ^{
                [[ProgressDialogUtil sharedManager] hideLoading];
            });
            [DialogUtil showDialogWithError:@"Error" withError:error.domain withButtonName:@"OK" buttonHandler:^{
                [DialogUtil dismissDialog];
            }];
            return;
        }
        
        // Order error
        NSString *errorInResponse = [json objectForKey:@"error"];
        if (errorInResponse != nil && ![errorInResponse  isEqual: @""]) {
            // hide loading
            dispatch_async(dispatch_get_main_queue(), ^{
                [[ProgressDialogUtil sharedManager] hideLoading];
            });
            [DialogUtil showDialogWithError:@"Error" withError:errorInResponse withButtonName:@"OK" buttonHandler:^{
                [DialogUtil dismissDialog];
            }];
            return;
        }
        
        [DialogUtil showDialogWithError:@"Done" withError:@"Order is cancelled succesfully" withButtonName:@"OK" buttonHandler:^{
            [DialogUtil dismissDialog];
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:
             ORDER_CANCEL_ORDER object:nil userInfo:nil];
        });
        
    });
    
}

- (void)cancelOrderAllDelegate: (NSData*) data {
    dispatch_async(kBgQueue, ^{
        if (!data) {
            return;
        }
        NSError *error;
        NSDictionary *json = [NSJSONSerialization
                              JSONObjectWithData:data
                              options:kNilOptions
                              error:&error];
        
        // Server error
        if (error != nil && ![error  isEqual: @""]) {
            // hide loading
            dispatch_async(dispatch_get_main_queue(), ^{
                [[ProgressDialogUtil sharedManager] hideLoading];
            });
            [DialogUtil showDialogWithError:@"Error" withError:error.domain withButtonName:@"OK" buttonHandler:^{
                [DialogUtil dismissDialog];
            }];
            return;
        }
        
        // Order error
        NSString *errorInResponse = [json objectForKey:@"error"];
        if (errorInResponse != nil && ![errorInResponse  isEqual: @""]) {
            // hide loading
            dispatch_async(dispatch_get_main_queue(), ^{
                [[ProgressDialogUtil sharedManager] hideLoading];
            });
            [DialogUtil showDialogWithError:@"Error" withError:errorInResponse withButtonName:@"OK" buttonHandler:^{
                [DialogUtil dismissDialog];
            }];
            return;
        }
        
        [DialogUtil showDialogWithError:@"Done" withError:@"Orders have been cancelled succesfully" withButtonName:@"OK" buttonHandler:^{
            [DialogUtil dismissDialog];
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:
             ORDER_CANCEL_ORDER object:nil userInfo:nil];
        });
        
    });
    
}

@end
