//
//  TradeHistoryApiProtocol.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/25/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TradeHistoryApiProtocol <NSObject>

@required
- (void)tradeHistoryUpdate: (NSData*) data;

@end

