//
//  PublicOrdersTableViewCell.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/16/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "PublicOrdersTableViewCell.h"
#import "Const.h"

@implementation PublicOrdersTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    //[super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)sellSectionButtonTouchUpInside:(id)sender {
//    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
//        [dict setValue: self.sellPriceLabel.text forKey:@"price"];
//        [dict setValue: self.sellAmountLabel.text forKey:@"quantity"];
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [[NSNotificationCenter defaultCenter] postNotificationName:PRESENT_TRADE_VC_FROM_TABLE_CELL object:self userInfo:dict];
//    });
    if (self.delegate) {
        [self.delegate sellSectionTouchedUp:self];
    }
}

- (IBAction)buySectionButtonTouchUpInside:(id)sender {
//    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
//    [dict setValue: self.buyPriceLabel.text forKey:@"price"];
//    [dict setValue: self.buyAmountLabel.text forKey:@"quantity"];
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [[NSNotificationCenter defaultCenter] postNotificationName:PRESENT_TRADE_VC_FROM_TABLE_CELL object:self userInfo:dict];
//    });
    if (self.delegate) {
        [self.delegate buySectionTouchedUp:self];
    }
}
@end
