//
//  FirebaseAnalytics.h
//  WEX
//
//  Created by Ngoc on 7/5/18.
//  Copyright © 2018 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>

@import Firebase;

// Firebase Analytics
static NSString *const FIR_DISPLAY_SCREEN_PAIR_LIST = @"display_pair_list_screen";
static NSString *const FIR_DISPLAY_SCREEN_DETAIL_INFO = @"display_detail_info_screen";
static NSString *const FIR_DISPLAY_SCREEN_BUY_SELL_INFO = @"display_buy_sell_screen";

static NSString *const FIR_ACTION_TOUCH_EVENT_ON_PIN_SETTING = @"touched_pin_setting";
static NSString *const FIR_ACTION_TOUCH_EVENT_ON_API_KEY_SETTING = @"touched_api_key_setting";
static NSString *const FIR_ACTION_TOUCH_EVENT_ON_ACTIVE_ORDER = @"touched_active_order_menu";
static NSString *const FIR_ACTION_TOUCH_EVENT_ON_TRADE_HISTORY = @"touched_trade_history_menu";
static NSString *const FIR_ACTION_TOUCH_EVENT_ON_BALANCE = @"touched_balances_menu";
static NSString *const FIR_ACTION_TOUCH_EVENT_ON_ABOUT_US= @"touched_about_us_menu";

static NSString *const FIR_ACTION_TOUCH_EVENT_ON_CHANGE_PAIR_TAB= @"touched_change_pair_tab";
static NSString *const FIR_ACTION_TOUCH_EVENT_ON_SELECT_PAIR= @"touched_select_a_pair_on_tab";

static NSString *const FIR_ACTION_TOUCH_EVENT_ON_BUY_SELL_BUTTON= @"touched_buy_sell_button";
static NSString *const FIR_ACTION_TOUCH_EVENT_ON_CELL_ON_ORDER_TABLE= @"touched_cell_on_order_table";
static NSString *const FIR_ACTION_TOUCH_EVENT_ON_BUY_BUTTON= @"touched_buy_button";
static NSString *const FIR_ACTION_TOUCH_EVENT_ON_SELL_BUTTON= @"touched_sell_button";

@interface FirebaseAnalytics : NSObject

+ (FirebaseAnalytics *)sharedManager;
- (void)logEventWithName:(NSString *_Nullable)eventName parameters:(NSDictionary *_Nullable)parameters;

@end
