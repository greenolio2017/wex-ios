//
//  ReachabilityUtil.h
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/28.
//  Copyright © 2017年 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface ReachabilityUtil : NSObject

+ (BOOL)canNetworkConnected;

@end
