//
//  FundsTableViewCell.m
//  WEX
//
//  Created by Ta Duong Ngoc on 11/16/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "FundsTableViewCell.h"

@implementation FundsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setUserInteractionEnabled:NO];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
