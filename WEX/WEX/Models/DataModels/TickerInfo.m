//
//  TickerInfo.m
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "TickerInfo.h"

@implementation TickerInfo

- (instancetype)initWithPairString: (NSString *) pairString
                              high: (NSNumber *) high
                               low: (NSNumber *) low
                               avg: (NSNumber *) avg
                               vol: (NSNumber *) vol
                           vol_cur: (NSNumber *) vol_cur
                              last: (NSNumber *) last
                               buy: (NSNumber *) buy
                              sell: (NSNumber *) sell
                           updated: (NSNumber *) updated {
    self.pairString  = pairString;
    self.high  = high;
    self.low  = low;
    self.avg  = avg;
    self.vol  = vol;
    self.vol_cur  = vol_cur;
    self.last  = last;
    self.buy  = buy;
    self.sell  = sell;
    self.updated  = updated;
    
    return self;
}

- (void)updateTickerInfo: (NSNumber *) high
                               low: (NSNumber *) low
                               avg: (NSNumber *) avg
                               vol: (NSNumber *) vol
                           vol_cur: (NSNumber *) vol_cur
                              last: (NSNumber *) last
                               buy: (NSNumber *) buy
                              sell: (NSNumber *) sell
                           updated: (NSNumber *) updated {
    self.high  = high;
    self.low  = low;
    self.avg  = avg;
    self.vol  = vol;
    self.vol_cur  = vol_cur;
    self.last  = last;
    self.buy  = buy;
    self.sell  = sell;
    self.updated  = updated;
}

@end
