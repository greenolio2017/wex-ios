//
//  LoadingViewController.h
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/27.
//  Copyright © 2017年 Ta Duong Ngoc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MarketsManager.h"

@interface LoadingViewController : UIViewController <MarketsManagerDelegate>

@end
