//
//  TradesManager.m
//  WEX
//
//  Created by Ta Duong Ngoc on 11/19/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "TradesManager.h"
#import "WexApiHandler.h"
#import "Const.h"
#import "LoggingCommon.h"
#import "TradesInfo.h"
#import "MarketsManager.h"
#import "DialogUtil.h"

@implementation TradesManager
{
    WexApiHandler *apiHandler;
}

+ (TradesManager *)sharedManager {
    static TradesManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


// Initialization

- (id)init {
    self = [super init];
    
    if (self) {
        apiHandler = [[WexApiHandler alloc] init];
    }
//    self.currentPair = DEFAULT_PAIR;
    self.tradesInfo = [[NSMutableArray alloc] init];
    
    [self updateTrades];
    
    self.isLoading = NO;
    
    return self;
}

- (void)setTimerActive: (BOOL) isStart {
    if (isStart){
        self.tracker = [NSTimer scheduledTimerWithTimeInterval:2.0f
                                                   target:self
                                                 selector:@selector(updateTrades)
                                                 userInfo:nil repeats:YES];
        
    } else {
        [self.tracker invalidate];
    }
}

- (void)updateTrades {
    LogTrace(@"IN");
    // do nothing if currentPair is nil
    if (!self.currentPair) {
        return;
    }
    // start timer if it is not active
    if (![self.tracker isValid]) {
        [self setTimerActive:YES];
    }
    
    // do not call API if it is waiting
    if (self.isLoading) {
        return;
    }
    
    self.isLoading = YES;
    
    dispatch_async(kBgQueue, ^{
        NSString * urlString = [[WEX_TRADES_URL stringByAppendingString:self.currentPair] stringByAppendingString:@"?ignore_invalid=1&limit=50"];
        [self->apiHandler getResponseFromPublicServerUrl: urlString withHandler:^(NSData *response) {
            
            self.isLoading = NO;
            
            if (response == nil) {
                return;
            }
            
            NSError* error;
            NSDictionary* json = [NSJSONSerialization
                                  JSONObjectWithData:response
                                  options:kNilOptions
                                  error:&error];
            
            if (error) {
                [DialogUtil showDialogWithError:@"Error" withError:error.domain withButtonName:@"OK" buttonHandler:^{
                    [DialogUtil dismissDialog];
                }];
//                [self updateTrades];
            }
            else {
                [self updateTradesInfoWithJson: self.currentPair withJson: json];
            }
        }];
    });
    LogTrace(@"OUT");
    
}

- (void)updateTradesInfoWithJson: (NSString *)pairString withJson: (NSDictionary *) json {
    
    self.tradesInfo = [json objectForKey:pairString];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:
         TRADE_HISTORY_MANAGER_UPDATE_TRADES object:nil userInfo:nil];
    });

}
@end
