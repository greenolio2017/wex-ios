//
//  FavoriteManager.m
//  WEX
//
//  Created by Ta Duong Ngoc on 6/19/18.
//  Copyright © 2018 Ta Duong Ngoc. All rights reserved.
//

#import "FavoriteManager.h"
#import "Const.h"

@interface FavoriteManager ()

@end

@implementation FavoriteManager
+ (FavoriteManager *)sharedManager {
    static FavoriteManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


// Initialization

- (id)init {
    self = [super init];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:USERDEFAULT_KEY_FAVORITE_LIST] == nil) {
        // init default favorite pairs
        self.favoriteArray = [[NSMutableArray alloc] init];
        [self.favoriteArray addObject:@"btc_usd"];
        [self.favoriteArray addObject:@"eth_usd"];
        [self.favoriteArray addObject:@"ltc_usd"];
        [self.favoriteArray addObject:@"bch_usd"];
    }
    else {
        self.favoriteArray = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:USERDEFAULT_KEY_FAVORITE_LIST]];
    }

    return self;
}

- (BOOL) isExistInList: (NSString*) pair {
    BOOL isExist = NO;
    // check if pair is exist in list
    for (NSString* str in self.favoriteArray) {
        if ([str isEqualToString:pair]) {
            isExist = YES;
            break;
        }
    }
    return isExist;
}

- (void) toggleFavoriteList: (NSString*) pairToToggle {
    BOOL isExist = NO;
    // check if pair is exist in list
    for (NSString* str in self.favoriteArray) {
        if ([str isEqualToString:pairToToggle]) {
            isExist = YES;
            break;
        }
    }
    
    // remove or add
    if (isExist) {
        [self.favoriteArray removeObject:pairToToggle];
    }
    else {
        [self.favoriteArray addObject:pairToToggle];
    }
    
    // store new array to user default
    [[NSUserDefaults standardUserDefaults] setObject:self.favoriteArray forKey:USERDEFAULT_KEY_FAVORITE_LIST];
    
    // Post notification to update table view
    [[NSNotificationCenter defaultCenter] postNotificationName:
         NOTIFICATION_UPDATE_FAVORITE object:nil userInfo:nil];
    
}

@end
