//
//  TradeHistoryManager.m
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/29.
//  Copyright © 2017年 Ta Duong Ngoc. All rights reserved.
//

#import "TradeHistoryManager.h"
#import "WexApiHandler.h"
#import "Const.h"
#import "DialogUtil.h"
#import "TradeHistoryInfo.h"
#import "ProgressDialogUtil.h"
#import "MarketsManager.h"

@implementation TradeHistoryManager
{
    NSTimer *tracker;
}

+ (TradeHistoryManager *)sharedManager {
    static TradeHistoryManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


// Initialization

- (id)init {
    self = [super init];
    
    if (self) {
        self.apiHandler = [[WexApiHandler alloc] init];
    }
    self.currentPair = DEFAULT_PAIR;
    
    self.tradeHistories = [[NSMutableArray alloc] init];
    
    return self;
}


- (void)updateTradeHistory {
    dispatch_async(kBgQueue, ^{
        NSMutableDictionary *post = [[NSMutableDictionary alloc] init];
        [post setObject:@"TradeHistory" forKey:@"method"];
        self.apiHandler.tradeHistoryDelegate = self;
        [self.apiHandler getResponseFromServerForPost:post withApi: @"TradeHistory"];
    });
}

#pragma Delegates
- (void)tradeHistoryUpdate: (NSData*) data {
    // hide loading
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ProgressDialogUtil sharedManager] hideLoading];
    });
    dispatch_async(kBgQueue, ^{
        if (!data) {
            return;
        }
        NSError *error;
        NSDictionary *json = [NSJSONSerialization
                              JSONObjectWithData:data
                              options:kNilOptions
                              error:&error];
        
        [self.tradeHistories removeAllObjects];
        
        // Server error
        if (error != nil && ![error  isEqual: @""]) {
            [DialogUtil showDialogWithError:@"Error" withError:error.domain withButtonName:@"OK" buttonHandler:^{
                [DialogUtil dismissDialog];
            }];
            return;
        }
        
        // Order error
        NSString *errorInResponse = [json objectForKey:@"error"];
        if (errorInResponse != nil && ![errorInResponse  isEqual: @""]) {
            [DialogUtil showDialogWithError:@"Error" withError:errorInResponse withButtonName:@"OK" buttonHandler:^{
                [DialogUtil dismissDialog];
            }];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:
                 TRADE_HISTORY_UPDATE object:nil userInfo:nil];
            });
            return;
        }
        
        NSDictionary *jsonInfo = [json objectForKey:@"return"];
        for (NSDictionary *dict in jsonInfo) {
            NSString *key = [NSString stringWithString:(NSString*)dict];
            TradeHistoryInfo *tradeHistoryInfo = [[TradeHistoryInfo alloc] init];
            NSDictionary *infoDict = [jsonInfo objectForKey:key];
            
            tradeHistoryInfo.orderId = key;
            tradeHistoryInfo.type = [infoDict objectForKey:@"type"];
            tradeHistoryInfo.amount = [infoDict objectForKey:@"amount"];
            tradeHistoryInfo.pair = [infoDict objectForKey:@"pair"];
            tradeHistoryInfo.rate = [infoDict objectForKey:@"rate"];
            tradeHistoryInfo.timestamp = [infoDict objectForKey:@"timestamp"];
            
            [self.tradeHistories addObject:tradeHistoryInfo];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:
             TRADE_HISTORY_UPDATE object:nil userInfo:nil];
        });
        
    });
}

@end
