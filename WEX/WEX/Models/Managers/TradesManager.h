//
//  TradesManager.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/19/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TradesManager : NSObject
@property NSMutableArray *tradesInfo;
@property NSString *currentPair;
@property NSTimer *tracker;
@property BOOL isLoading;

+ (TradesManager *)sharedManager;

//- (void)setTimerActive: (BOOL) isStart;
- (void)updateTrades;

@end
