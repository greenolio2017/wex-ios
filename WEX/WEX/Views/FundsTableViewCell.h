//
//  FundsTableViewCell.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/16/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FundsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblPair;
@property (weak, nonatomic) IBOutlet UILabel *lblAmount;

@end
