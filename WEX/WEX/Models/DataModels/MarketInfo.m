//
//  MarketInfo.m
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "MarketInfo.h"

@implementation MarketInfo

- (instancetype)initWithPairString: (NSString *) pairString
                    decimal_places: (NSNumber *) decimal_places
                         min_price: (NSNumber *) min_price
                         max_price: (NSNumber *) max_price
                        min_amount: (NSNumber *) min_amount
                            hidden: (NSNumber *) hidden
                               fee: (NSNumber *) fee {
    self.pairString  = pairString;
    self.decimal_places  = decimal_places;
    self.min_price  = min_price;
    self.max_price  = max_price;
    self.min_amount  = min_amount;
    self.hidden  = hidden;
    self.fee  = fee;
    
    return self;
}

//- (void)updateTickerInfo: (NSNumber *) decimal_places
//               min_price: (NSNumber *) min_price
//               max_price: (NSNumber *) max_price
//              min_amount: (NSNumber *) min_amount
//                  hidden: (NSNumber *) hidden
//                     fee: (NSNumber *) fee {
//    self.decimal_places  = decimal_places;
//    self.min_price  = min_price;
//    self.max_price  = max_price;
//    self.min_amount  = min_amount;
//    self.hidden  = hidden;
//    self.fee  = fee;
//}

@end
