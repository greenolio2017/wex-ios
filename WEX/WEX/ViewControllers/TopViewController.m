//
//  AppDelegate.m
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "TopViewController.h"
#import "MainViewController.h"
#import "LeftMenuViewController.h"
#import "LoggingCommon.h"
#import "TickerManager.h"
#import "CommonUtil.h"
#import "Const.h"

#define CENTER_TAG 1
#define LEFT_PANEL_TAG 2
#define CORNER_RADIUS 0
#define SLIDE_TIMING 0.25

CGFloat LEFT_MENU_TRIGGER_WIDTH = -1;
NSInteger PANEL_WIDTH = 60;


#pragma mark - TopViewController
@interface TopViewController ()

//@property (nonatomic, strong) MainViewController *mainVC;
@property (nonatomic, strong) MainViewController *mainVC;
@property (nonatomic, strong) LeftMenuViewController *leftMenuVC;
@property (nonatomic) BOOL showingLeftPanel;
@property (nonatomic, assign) BOOL showingRightPanel;
@property (nonatomic, assign) BOOL showPanel;
@property (nonatomic) UIPanGestureRecognizer *leftEdgeGesture;
@property (nonatomic) UIPanGestureRecognizer *panGestureLeftMenu, *panGestureMainView;
@property (nonatomic) CGPoint draggingPoint;
@property (nonatomic) NSArray *mainVCConstraintH;
@property (nonatomic) NSArray *mainVCConstraintV;
@property (nonatomic) NSArray *leftMenuVCConstraintH;
@property (nonatomic) NSArray *leftMenuVCConstraintV;

//new propertiest for show/hide left menu
@property (nonatomic) BOOL isPanelShownFromTheBegining;


@end

@implementation TopViewController

- (void)viewDidLoad {
    LogTrace(@"IN >> viewDidLoad");
    [super viewDidLoad];
    
    PANEL_WIDTH = self.view.frame.size.width - 300;
    
    self.mainView.clipsToBounds = YES;
    
    
    self.navigationController.navigationBar.hidden = YES;
    self.mainView.hidden = YES;
    [self setupView];
    
    
    if (LEFT_MENU_TRIGGER_WIDTH < 0) {
        LEFT_MENU_TRIGGER_WIDTH = CGRectGetWidth([UIScreen mainScreen].bounds) / 8;
    }
    
    LogTrace(@"OUT");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)viewWillAppear:(BOOL)animated {
    LogTrace(@"IN");
    [super viewWillAppear:animated];
    LogTrace(@"OUT");
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}


- (void)showCaseFailure {
    LogTrace(@"IN");
    self.mainView.hidden = NO;
    
    LogTrace(@"OUT");
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}


- (void)dealloc {
    LogTrace(@"dealloc : %@", NSStringFromClass(self.class));
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (BOOL)shouldAutorotate {
    return NO;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    LogTrace(@"key = %@", keyPath);
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    
}

#pragma mark Setup View

- (void)setupView {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hamburgerButtonClicked:)
                                                 name:TOOGLE_LEFT_MENU
                                               object:nil];
    
    
    self.mainVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"mainVC"];
    self.mainVC.view.tag = CENTER_TAG;
    
    [self.view addSubview:self.mainVC.view];
    [self addChildViewController:self.mainVC];
    [self.mainVC didMoveToParentViewController:self];
    
    self.mainVC.view.translatesAutoresizingMaskIntoConstraints = NO;
    //[self mainVCUpdateConstraintH:0.0f];
    
    NSDictionary *viewsDictionary = @{
                                      @"mainVC" : self.mainVC.view
                                      };
    self.mainVCConstraintH = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[mainVC]|" options:0 metrics:nil views:viewsDictionary];
    self.mainVCConstraintV = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[mainVC]|" options:0 metrics:nil views:viewsDictionary];
    [self.view addConstraints:self.mainVCConstraintH];
    [self.view addConstraints:self.mainVCConstraintV];
    
    
    // Add leftMenuViewController
    self.leftMenuVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"leftMenuViewController"];
    self.leftMenuVC.view.tag = LEFT_PANEL_TAG;
    
    [self.view addSubview:self.leftMenuVC.view];
    [self addChildViewController:self.leftMenuVC];
    self.leftMenuVC.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    viewsDictionary = @{
                        @"leftMenuVC" : self.leftMenuVC.view
                        };
    NSString *leftMenuVCConstraintHFormat = [NSString stringWithFormat:@"H:[leftMenuVC(%f)]-(%f)-|", self.view.frame.size.width, self.view.frame.size.width];
    
    self.leftMenuVCConstraintH = [NSLayoutConstraint constraintsWithVisualFormat:leftMenuVCConstraintHFormat options:0 metrics:nil views:viewsDictionary];
    self.leftMenuVCConstraintV = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[leftMenuVC]|" options:0 metrics:nil views:viewsDictionary];
    [self.view addConstraints:self.leftMenuVCConstraintH];
    [self.view addConstraints:self.leftMenuVCConstraintV];
    
    [self setupGestures];
}

/**
 ------------------------------------------
 * @brief mainVC update contraints
 * @param leftMargin CGFloat
 ------------------------------------------
 */
- (void)leftMenuViewControllerUpdateConstraintH:(CGFloat)leftMargin {
    LogTrace(@"IN");
    [self.view removeConstraints:self.leftMenuVCConstraintH];
    [self.view removeConstraints:self.leftMenuVCConstraintV];
    self.mainVC.view.translatesAutoresizingMaskIntoConstraints = NO;
    NSDictionary *viewsDictionary = @{
                                      @"leftMenuVC" : self.leftMenuVC.view
                                      };
    NSString *leftMenuVCConstraintHFormat = [NSString stringWithFormat:@"H:[leftMenuVC(%f)]-%f-|", self.view.frame.size.width, leftMargin];
    self.leftMenuVCConstraintH = [NSLayoutConstraint constraintsWithVisualFormat:leftMenuVCConstraintHFormat options:0 metrics:nil views:viewsDictionary];
    self.leftMenuVCConstraintV = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[leftMenuVC]|" options:0 metrics:nil views:viewsDictionary];
    
    [self.view addConstraints:self.leftMenuVCConstraintH];
    [self.view addConstraints:self.leftMenuVCConstraintV];
    
    LogTrace(@"OUT");
}

/**
 ------------------------------------------
 * @brief Remove All constraints for leftMenuVC
 ------------------------------------------
 */
- (void)removeConstraintsleftMenuVC {
    LogTrace(@"IN");
    [self.view removeConstraints:self.leftMenuVCConstraintH];
    [self.view removeConstraints:self.leftMenuVCConstraintV];
    self.leftMenuVC.view.translatesAutoresizingMaskIntoConstraints = YES;
    LogTrace(@"OUT");
}

#pragma mark Swipe Gesture Setup/Actions

-(void)setupGestures {
    self.panGestureMainView = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureMainView:)];
    [self.panGestureMainView setMinimumNumberOfTouches:1];
    [self.panGestureMainView setMaximumNumberOfTouches:1];
    
    self.panGestureLeftMenu = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureLeftMenu:)];
    [self.panGestureLeftMenu setMinimumNumberOfTouches:1];
    [self.panGestureLeftMenu setMaximumNumberOfTouches:1];
    [self.leftMenuVC.view addGestureRecognizer:self.panGestureLeftMenu];
    
    self.leftEdgeGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(leftEdgeGesture:)];
    //self.leftEdgeGesture.edges = UIRectEdgeLeft;
    [self.mainVC.view addGestureRecognizer:self.leftEdgeGesture];
}

- (void)panGestureMainView:(id)sender {
    LogTrace(@"IN");
    [self movePanel:sender];
    LogTrace(@"OUT");
}

- (void)panGestureLeftMenu:(id)sender {
    LogTrace(@"IN");
    [self movePanel:sender];
    LogTrace(@"OUT");
}

- (void)leftEdgeGesture:(id)sender {
    LogTrace(@"IN");
    [self movePanel:sender];
    LogTrace(@"OUT");
}

-(void)movePanel:(id)sender {
    [[[(UITapGestureRecognizer*)sender view] layer] removeAllAnimations];
    
    [self.mainVC addGestureForBlackOutView];
    
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:self.view];
    CGFloat movement = translatedPoint.x - self.draggingPoint.x;
    
    if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan) {
        
        self.draggingPoint = translatedPoint;
        
        self.isPanelShownFromTheBegining = self.showPanel;
    }
    
    if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded ||
       [(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateCancelled) {
        
        if (!self.showPanel) {
            [self movePanelToOriginalPosition];
        } else {
            [self movePanelRight];
        }
    }
    
    if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateChanged) {
        
        if (self.isPanelShownFromTheBegining) {
            self.showPanel = CGRectGetMaxX(self.leftMenuVC.view.frame) > (CGRectGetWidth(self.view.frame) - PANEL_WIDTH - LEFT_MENU_TRIGGER_WIDTH);
        } else {
            self.showPanel = CGRectGetMaxX(self.leftMenuVC.view.frame) > LEFT_MENU_TRIGGER_WIDTH;
        }
        
        CGFloat newHorizontalLocation = self.leftMenuVC.view.frame.origin.x + movement;
        if (newHorizontalLocation <  -PANEL_WIDTH)
        {
            [self move:self.leftMenuVC.view toLocation:newHorizontalLocation];
            [self.mainVC changeBlackOutViewAlpha:(self.view.frame.size.width + newHorizontalLocation) / self.view.frame.size.width / 2];
        }
        
        self.draggingPoint = translatedPoint;
    }
}

- (void)move:(UIView *)view toLocation:(CGFloat)location {
    if (location >= -self.view.frame.size.width  && location <= 0) {
        CGRect rect = view.frame;
        
        rect.origin.x = location;
        
        view.frame = rect;
    }
}

-(void)movePanelToOriginalPosition {
    [self leftMenuViewControllerUpdateConstraintH: self.view.frame.size.width];
    CGRect originalPosition = CGRectMake(-self.view.frame.size.width, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    // current blackout view alpha
    self.mainVC.blackOutView.alpha = (self.leftMenuVC.view.frame.origin.x + self.view.frame.size.width) / self.view.frame.size.width;
    [self.mainVC changeBlackOutViewAlpha:(self.leftMenuVC.view.frame.origin.x + self.view.frame.size.width) / self.view.frame.size.width / 2];
    
    [UIView animateWithDuration:SLIDE_TIMING delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.leftMenuVC.view.frame = originalPosition;
        [self.leftMenuVC.tbviewContent setContentOffset:CGPointZero animated:NO];
        
        // after blackout view alpha
        self.mainVC.blackOutView.alpha = 0;
        [self.mainVC changeBlackOutViewAlpha:0];
        [self.view layoutIfNeeded];
    }
                     completion:^(BOOL finished) {
                         self.showPanel = NO;
                         [self.mainVC removeGestureForBlackOutView];
                         [self.mainVC.view removeGestureRecognizer:self.panGestureMainView];
                     }];
}

-(void)movePanelRight {
    [self leftMenuViewControllerUpdateConstraintH: PANEL_WIDTH];
    CGRect leftPanelPosition = CGRectMake(-PANEL_WIDTH, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    // current blackout view alpha
    [self.mainVC changeBlackOutViewAlpha:(self.leftMenuVC.view.frame.origin.x + self.view.frame.size.width) / self.view.frame.size.width / 2];
    
    [UIView animateWithDuration:SLIDE_TIMING delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.leftMenuVC.view.frame = leftPanelPosition;
        // current blackout view alpha
        [self.mainVC changeBlackOutViewAlpha:(-PANEL_WIDTH + self.view.frame.size.width) / self.view.frame.size.width / 2];
        [self.view layoutIfNeeded];
    }
                     completion:^(BOOL finished) {
                         if (finished) {
                             self.showPanel = YES;
                             [self.mainVC addGestureForBlackOutView];
                             [self.mainVC.view addGestureRecognizer:self.panGestureMainView];
                         }
                     }];
}

#pragma mark Hamburger button click
-(void)hamburgerButtonClicked: (id)sender {
    if (_showPanel) {
        [self movePanelToOriginalPosition];
    } else {
        [self movePanelRight];
    }
}

- (void)handleForceCloseLeftMenuNotification:(NSNotification *)notification {
    if (self.showPanel) {
        [self movePanelToOriginalPosition];
    }
}

@end
