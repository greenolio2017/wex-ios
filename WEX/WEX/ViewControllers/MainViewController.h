//
//  MainViewController.h
//  eExmo
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *blackOutView;
- (void)addGestureForBlackOutView;
- (void)removeGestureForBlackOutView;
- (void)changeBlackOutViewAlpha: (float) alpha;

@end

