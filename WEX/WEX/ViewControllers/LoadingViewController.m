//
//  LoadingViewController.m
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/27.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "LoadingViewController.h"
#import "Const.h"
#import "TopViewController.h"
#import "CommonUtil.h"
#import "Reachability.h"
#import "ProgressDialogUtil.h"
#import "GraphNewDataManager.h"
#import "TickerManager.h"

@interface LoadingViewController ()
@property (nonatomic) BOOL isDataTickerDone;
@end

@implementation LoadingViewController
{
    NSTimer *tracker;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.defaultPair = @"btc_usd";
    
    // add observer
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(marketManagerLoadSuccess) name:MARKETS_MANAGER_UPDATE_MARKETS object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataTickerDone) name:TICKER_MANAGER_UPDATE_TICKERS object:nil];
//
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataDepthDone) name:DEPTH_MANAGER_UPDATE_DEPTH object:nil];
    [MarketsManager sharedManager].delegate = self;
    [[MarketsManager sharedManager] updateAllMarkets];
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    
    [super viewDidAppear:animated];
    
    //[[ProgressDialogUtil sharedManager] showLoading:self.view alpha:0];
    
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
//    [self setTimerActive:NO];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)marketsUpdated {
    TickerManager *tickerManager = [TickerManager sharedManager];
    [tickerManager updateAllTickers];
//    [tickerManager setTimerActive:YES];
}

//- (void)setTimerActive: (BOOL) isStart {
//    if (isStart){
//        tracker = [NSTimer scheduledTimerWithTimeInterval:1.0f
//                                                   target:self
//                                                 selector:@selector(checkLoadingDone)
//                                                 userInfo:nil repeats:YES];
//
//    } else {
//        [tracker invalidate];
//    }
//}

//- (void)marketManagerLoadSuccess {
//
//    //dispatch_async(dispatch_get_main_queue(), ^{
//        //[CommonUtil initAllManagersAndWKWebviewCache: self.defaultPair];
//        // Init ticker manager
//        TickerManager *tickerManager = [TickerManager sharedManager];
//        [tickerManager updateAllTickers];
//        [tickerManager setTimerActive:YES];
//    //});
//
////    [self checkLoadingDone];
////    [self setTimerActive:YES];
//}

- (void) dataTickerDone {
//    self.isDataTickerDone = YES;
    [[ProgressDialogUtil sharedManager] hideLoading];
    TopViewController *topVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"topVC"];
    [self presentViewController:topVC animated:NO completion:nil];
}
//
//- (void) dataDepthDone {
//    self.isDataDepthDone = YES;
//}

//- (void) checkLoadingDone {
//    if (self.isDataTickerDone && self.isDataDepthDone) {
//        [[ProgressDialogUtil sharedManager] hideLoading];
//        TopViewController *topVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"topVC"];
//        [self presentViewController:topVC animated:NO completion:nil];
//    }
//}

@end
