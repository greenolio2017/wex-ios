//
//  FavoriteManager.h
//  WEX
//
//  Created by Ta Duong Ngoc on 6/19/18.
//  Copyright © 2018 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FavoriteManager : NSObject

@property (nonatomic) NSMutableArray<NSString*> *favoriteArray;

+ (FavoriteManager *)sharedManager;
- (BOOL) isExistInList: (NSString*) pair;
- (void) toggleFavoriteList: (NSString*) pairToToggle;

@end
