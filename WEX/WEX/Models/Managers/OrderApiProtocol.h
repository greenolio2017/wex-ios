//
//  OrderApiProtocol.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/25/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OrderApiProtocol <NSObject>

@required
- (void)activeOrders: (NSData*) data;

@optional
- (void)orderInfo: (NSData*) data;
- (void)cancelOrderDelegate: (NSData*) data;
- (void)cancelOrderAllDelegate: (NSData*) data;

@end

