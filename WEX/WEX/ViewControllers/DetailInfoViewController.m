//
//  DetailInfoViewController.m
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "DetailInfoViewController.h"
#import "Const.h"
#import "CommonUtil.h"
#import "TickerManager.h"
#import "LoggingCommon.h"
#import "MarketsManager.h"
#import "TradeManager.h"
#import "CAPSPageMenu.h"

#import "MarketsViewController.h"
#import "PublicOrdersViewController.h"
#import "PublicTradesViewController.h"
#import "ChartViewController.h"
#import "TradeViewController.h"

#import "ProgressDialogUtil.h"

#import "OrderCreatedNotificationViewController.h"
#import "PublicOrdersTableViewCell.h"
#import "FirebaseAnalytics.h"

// for PIN
#import "TOPasscodeViewController.h"
#import "TOPasscodeViewControllerConstants.h"
#import <LocalAuthentication/LocalAuthentication.h>
#import "AppDelegate.h"

@interface DetailInfoViewController () <TOPasscodeViewControllerDelegate, PublicOrdersViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *titleButton;

@property (weak, nonatomic) IBOutlet UILabel *lastPrice;
@property (weak, nonatomic) IBOutlet UILabel *lowPrice;
@property (weak, nonatomic) IBOutlet UILabel *highPrice;
@property (weak, nonatomic) IBOutlet UILabel *volumnAskCurrency;
@property (weak, nonatomic) IBOutlet UILabel *volumnBidCurrency;

@property (nonatomic) NSString *lowestAsk;
@property (nonatomic) NSString *highestBid;

//@property (nonatomic) NSString *priceForPresentTradeFromTableView;
//@property (nonatomic) NSString *amountForPresentTradeFromTableView;
//@property (nonatomic) BOOL isPresentTradeFromTableView;

@property (nonatomic) CAPSPageMenu *pageMenu;

//@property (nonatomic) int currentPairIndex;

@property NSMutableArray *depthAsks;
@property double maxDepthAskAmount;
@property NSMutableArray *depthBids;
@property double maxDepthBidAmount;
@property NSMutableArray *tradesInfo;

@property (nonatomic, strong) LAContext *authContext;
@property (nonatomic, assign) BOOL biometricsAvailable;
@property (nonatomic, assign) BOOL faceIDAvailable;

@end

@implementation DetailInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Init authentication context
    self.authContext = [[LAContext alloc] init];
    
    // Observer
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateCurrentPair:) name:PAIRS_CHANGED object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDataTicker) name:TICKER_MANAGER_UPDATE_TICKERS object:nil];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentTradeViewControllerFromTableView:) name:PRESENT_TRADE_VC_FROM_TABLE_CELL object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tradeInfoUpdateSuccess) name:TRADE_INFO_UPDATE object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tradeOrderCreatedFromTradeVC:) name:TRADE_ORDER_CREATED_FROM_TRADE_VC object:nil];
    
//    // set default currency
//    for (int i = 0; i < [[MarketsManager sharedManager].marketInfoArray count]; i++) {
//        if ([[[MarketsManager sharedManager].marketInfoArray objectAtIndex:i].pairString isEqualToString:@"btc_usd"]) {
//            self.currentPairIndex = i;
//        }
//    }
    
    // init title
    self.titleLabel.text = [CommonUtil pairStringToStringDisplay: self.currentPair];
    
    [self.titleButton setTitle:[CommonUtil pairStringToStringDisplay: self.currentPair] forState: UIControlStateNormal];
    
    // init data
    [self updateDataTicker];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[FirebaseAnalytics sharedManager] logEventWithName:FIR_DISPLAY_SCREEN_DETAIL_INFO parameters:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!self.pageMenu) {
        [self createPageMenu];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
    
- (void) createPageMenu {
    // Btc Market ViewController
    PublicOrdersViewController *publicOrdersVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"publicOrdersVC"];
    publicOrdersVC.title = @"ORDERS";
    publicOrdersVC.delegate = self;
    
    // Eth Market ViewController
    PublicTradesViewController *publicTradesVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"publicTradesVC"];
    publicTradesVC.title = @"TRADES";
    
    // Favourites market
    ChartViewController *chartVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"chartVC"];
    chartVC.title = @"CHART";
    chartVC.currentPair = self.currentPair;
    chartVC.widthForChart = self.pageViewArea.frame.size.width;
    chartVC.heightForChart = self.pageViewArea.frame.size.height - 20;
    
    
    NSArray *controllerArray = @[chartVC, publicOrdersVC, publicTradesVC];
    
    float menuHeight = PAGE_MENU_HEIGHT;
    float itemWidth = (self.view.frame.size.width - ([controllerArray count] + 1)*15) / [controllerArray count];
    
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionSelectionIndicatorColor: COLOR_BLACK_COMMON,
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor clearColor],
                                 CAPSPageMenuOptionMenuItemFont: [UIFont systemFontOfSize:14.0f weight:UIFontWeightBold],
                                 CAPSPageMenuOptionMenuHeight: @(menuHeight),
                                 CAPSPageMenuOptionMenuItemWidth: @(itemWidth),
                                 CAPSPageMenuOptionCenterMenuItems: @(YES)
                                 };
    
    self.pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.pageViewArea.frame.size.width, self.pageViewArea.frame.size.height) options:parameters];
    [self.pageViewArea addSubview:_pageMenu.view];
    
    // Fix the constrains
    _pageMenu.view.translatesAutoresizingMaskIntoConstraints = NO;
    [self addChildViewController:_pageMenu];
    [self.pageViewArea addSubview:_pageMenu.view];
    NSDictionary *viewsDictionary = @{
                                      @"pageMenu" : _pageMenu.view
                                      };
    
    NSArray *controllerScrollView_constraint_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[pageMenu]|" options:0 metrics:nil views:viewsDictionary];
    NSString *controllerScrollView_constraint_V_Format = [NSString stringWithFormat:@"V:|[pageMenu]|"];
    NSArray *controllerScrollView_constraint_V = [NSLayoutConstraint constraintsWithVisualFormat:controllerScrollView_constraint_V_Format options:0 metrics:nil views:viewsDictionary];
    
    [self.pageViewArea addConstraints:controllerScrollView_constraint_H];
    [self.pageViewArea addConstraints:controllerScrollView_constraint_V];
    [_pageMenu didMoveToParentViewController:self];
}

//-(void) updateCurrentPair:(NSNotification *) notification
//{
//    LogTrace("IN");
//    NSDictionary *dict = notification.userInfo;
//    NSNumber *index = [dict valueForKey:@"Index"];
//    if (index != nil) {
//        self.currentPairIndex = [index intValue];
//    }
//
//    self.titleLabel.text = [CommonUtil pairStringToStringDisplay: [[MarketsManager sharedManager].marketInfoArray objectAtIndex:self.currentPairIndex].pairString];
//    [self.titleButton setTitle:[CommonUtil pairStringToStringDisplay: [[MarketsManager sharedManager].marketInfoArray objectAtIndex:self.currentPairIndex].pairString] forState: UIControlStateNormal];
//
//    LogTrace("OUT");
//}

- (void) updateDataTicker {
    LogTrace("IN");
    dispatch_async(dispatch_get_main_queue(), ^{

        //TickerInfo *tickerInfo = [[TickerManager sharedManager].tickerInfoArray objectAtIndex:self.currentPairIndex];
        TickerInfo *tickerInfo = [[TickerInfo alloc] init];
        for (TickerInfo *ticker in [TickerManager sharedManager].tickerInfoArray) {
            if (ticker.pairString == self.currentPair) {
                tickerInfo = ticker;
            }
        }
        
        if (!tickerInfo) {
            return;
        }
        
        NSString * bidCurrencyStrForDisplaying = [CommonUtil bidCurrencyFromPairStringForDisplaying:tickerInfo.pairString];
        NSString * askCurrencyStrForDisplaying = [CommonUtil askCurrencyFromPairStringForDisplaying:tickerInfo.pairString];
        
        self.lastPrice.text = [CommonUtil dealWithDoubleWithFiveDecimal:[tickerInfo.last doubleValue]];
        self.lowPrice.text = [[CommonUtil dealWithDoubleWithFiveDecimal:[tickerInfo.low doubleValue]] stringByAppendingString:bidCurrencyStrForDisplaying];
        self.highPrice.text = [[CommonUtil dealWithDoubleWithFiveDecimal:[tickerInfo.high doubleValue]] stringByAppendingString:bidCurrencyStrForDisplaying];
        
        self.volumnAskCurrency.text = [[CommonUtil dealWithDoubleWithEightDecimal:[tickerInfo.vol_cur intValue]] stringByAppendingString:askCurrencyStrForDisplaying];
        self.volumnBidCurrency.text = [[CommonUtil dealWithDoubleWithEightDecimal:[tickerInfo.vol intValue]] stringByAppendingString:bidCurrencyStrForDisplaying];
        
        self.lowestAsk = [[CommonUtil dealWithDoubleWithFiveDecimal:[tickerInfo.buy doubleValue]] stringByAppendingString:bidCurrencyStrForDisplaying];
        self.highestBid = [[CommonUtil dealWithDoubleWithFiveDecimal:[tickerInfo.sell doubleValue]] stringByAppendingString:bidCurrencyStrForDisplaying];
        
    });
    
    LogTrace("OUT");
}


//- (void)presentTradeViewControllerFromTableView: (NSString*) price andAmount: (NSString*) amount {
//    // Display PIN screen if user did not input it
//    if ([CommonUtil isPINCheckAlready] == FALSE) {
//        [CommonUtil displayPINScreen:self withContext:self.authContext];
//        return;
//    }
//
//    self.priceForPresentTradeFromTableView = price;
//    self.amountForPresentTradeFromTableView = amount;
//    self.isPresentTradeFromTableView = YES;
//    [[ProgressDialogUtil sharedManager] showLoading:self.view alpha:0.0];
//    [[TradeManager sharedManager] updateAccountInfo];
//}

-(void) tradeOrderCreatedFromTradeVC:(NSNotification *) notification
{
    LogTrace("IN");
    NSDictionary *dict = notification.userInfo;
    //NSString *orderId = [dict valueForKey:@"order_id"];
    NSString *type = [dict valueForKey:@"type"];
    NSString *price = [dict valueForKey:@"price"];
    NSString *quantity = [dict valueForKey:@"quantity"];
    NSString *total = [dict valueForKey:@"total"];
    
    [self presentOrderCreatedNotificationViewController:type withPrice:price withQuantity:quantity withTotal:total];
}

- (void)presentTradeViewController:(NSString*) priceAsk withPriceBid: (NSString*) priceBid withBuyAmount: (NSString*) buyAmount andSellAmount: (NSString*) sellAmount {
     self.definesPresentationContext = YES;
    TradeViewController *tradeVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"tradeVC"];
    tradeVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    tradeVC.currentPair = self.currentPair;
    tradeVC.priceAsk = priceAsk;
    tradeVC.priceBid = priceBid;
    tradeVC.buyAmount = buyAmount;
    tradeVC.sellAmount = sellAmount;
    [self presentViewController:tradeVC animated:NO completion:nil];
}

- (void)presentOrderCreatedNotificationViewController:(NSString*) type withPrice: (NSString*) price withQuantity: (NSString*) quantity withTotal: (NSString*) total {
    self.definesPresentationContext = YES;
    OrderCreatedNotificationViewController *orderCreatedNotificationVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"orderCreatedNotificationVC"];
    orderCreatedNotificationVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    orderCreatedNotificationVC.type = type;
    orderCreatedNotificationVC.price = price;
    orderCreatedNotificationVC.amount = quantity;
    orderCreatedNotificationVC.total = total;
    [self presentViewController:orderCreatedNotificationVC animated:NO completion:nil];
}

- (IBAction)touchUpInsideMenuButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)marketButtonTouchUpInside:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)buySellButtonTouchUpInside:(id)sender {
    
    // Display PIN screen if user did not input it
    if ([CommonUtil isPINCheckAlready] == FALSE) {
        [CommonUtil displayPINScreen:self withContext:self.authContext];
        return;
    }
    [self presentTradeViewController:self.lowestAsk withPriceBid: self.highestBid withBuyAmount:@"0" andSellAmount:@"0"];
    
    [[FirebaseAnalytics sharedManager] logEventWithName:FIR_ACTION_TOUCH_EVENT_ON_BUY_SELL_BUTTON parameters:nil];
    
//    self.isPresentTradeFromTableView = NO;
//    [[ProgressDialogUtil sharedManager] showLoading:self.view alpha:0.0];
//    [[TradeManager sharedManager] updateAccountInfo];
}

- (IBAction)zoomGraphButtonTouchUpInside:(id)sender {
// Future functionality
//    GraphDetailViewController *graphDetailVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"graphDetailVC"];
//    [self presentViewController:graphDetailVC animated:NO completion:nil];
}

//- (void)tradeInfoUpdateSuccess {
//    if (!self.isPresentTradeFromTableView) {
//        [self presentTradeViewController:@"" withQuantity:@""];
//    }
//    else {
//        [self presentTradeViewController:self.priceForPresentTradeFromTableView withQuantity:_amountForPresentTradeFromTableView];
//    }
//    self.isPresentTradeFromTableView = NO;
//}

#pragma mark - Section Public Order button delegate
- (void) buySectionTouchedUp:(NSString *)price andAmount:(NSString *)amount {
    //[self presentTradeViewControllerFromTableView:price andAmount:amount];
    [self presentTradeViewController:price withPriceBid:price withBuyAmount:@"0" andSellAmount:amount];
    
}
- (void) sellSectionTouchedUp:(NSString *)price andAmount:(NSString *)amount {
    [self presentTradeViewController:price withPriceBid:price withBuyAmount:amount andSellAmount:@"0"];
}

#pragma mark - TOPasscode delegate

- (BOOL)passcodeViewController:(TOPasscodeViewController *)passcodeViewController isCorrectCode:(NSString *)code
{
    return [code isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:USERDEFAULT_KEY_PASSCODE]];
}

- (void)didTapCancelInPasscodeViewController:(TOPasscodeViewController *)passcodeViewController {
    [passcodeViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)didInputCorrectPasscodeInPasscodeViewController:(TOPasscodeViewController *)passcodeViewController {
    ((AppDelegate*)(UIApplication.sharedApplication.delegate)).isPINInputed = YES;
    [passcodeViewController dismissViewControllerAnimated:YES completion:nil];
    
//    self.isPresentTradeFromTableView = NO;
    [[ProgressDialogUtil sharedManager] showLoading:self.view alpha:0.0];
    [[TradeManager sharedManager] updateAccountInfo];
}

- (void)didPerformBiometricValidationRequestInPasscodeViewController:(TOPasscodeViewController *)passcodeViewController {
    __weak typeof(self) weakSelf = self;
    NSString *reason = @"Touch ID to continue using this app";
    id reply = ^(BOOL success, NSError *error) {
        
        // Touch ID validation was successful
        // (Use this to dismiss the passcode controller and display the protected content)
        if (success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // Create a new Touch ID context for next time
                [weakSelf.authContext invalidate];
                weakSelf.authContext = [[LAContext alloc] init];
                
                // Dismiss the passcode controller
                [weakSelf dismissViewControllerAnimated:YES completion:nil];
                ((AppDelegate*)(UIApplication.sharedApplication.delegate)).isPINInputed = YES;
            });
            return;
        }
        
        // Actual UI changes need to be made on the main queue
        dispatch_async(dispatch_get_main_queue(), ^{
            [passcodeViewController setContentHidden:NO animated:YES];
        });
        
        // The user hit 'Enter Password'. This should probably do nothing
        // but make sure the passcode controller is visible.
        if (error.code == kLAErrorUserFallback) {
            NSLog(@"User tapped 'Enter Password'");
            return;
        }
        
        // The user hit the 'Cancel' button in the Touch ID dialog.
        // At this point, you could either simply return the user to the passcode controller,
        // or dismiss the protected content and go back to a safer point in your app (Like the login page).
        if (error.code == LAErrorUserCancel) {
            NSLog(@"User tapped cancel.");
            return;
        }
        
        // There shouldn't be any other potential errors, but just in case
        NSLog(@"%@", error.localizedDescription);
    };
    
    [passcodeViewController setContentHidden:YES animated:YES];
    
    [self.authContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics localizedReason:reason reply:reply];
}

@end
