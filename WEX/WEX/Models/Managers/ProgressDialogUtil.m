//
//  ProgressDialogUtil.m
//  WEX
//
//  Created by Ta Duong Ngoc on 12/1/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "ProgressDialogUtil.h"
#import "LoadingView.h"

@implementation ProgressDialogUtil

+ (ProgressDialogUtil *)sharedManager {
    static ProgressDialogUtil *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}



- (void)showLoading:(UIView *)parentView alpha: (CGFloat)alphaBackgroundColor{
//    LoadingView *loadingView;
//    for(UIView *aView in parentView.subviews){
//        if([aView isKindOfClass:[LoadingView class]]){
//            loadingView = (LoadingView *)aView;
//            break;
//        }
//    }
    if (self.isLoading) {
        return;
    }
    
    //if (!self.loadingView) {
        self.loadingView = [[LoadingView alloc] initWithFrame:CGRectMake(0, 0, parentView.frame.size.width, parentView.frame.size.height)];
        self.loadingView.translatesAutoresizingMaskIntoConstraints = false;
        [parentView addSubview:self.loadingView];
        [self.loadingView setupConstraints];
    //}
    self.loadingView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:alphaBackgroundColor];
    [self.loadingView loading];
    [parentView bringSubviewToFront:self.loadingView];
    self.isLoading = YES;
}

- (void)hideLoading {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.loadingView removeFromSuperview];
        self.isLoading = NO;
    });
}

@end
