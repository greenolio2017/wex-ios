//
//  ReachabilityUtil.m
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/28.
//  Copyright © 2017年 Ta Duong Ngoc. All rights reserved.
//

#import "ReachabilityUtil.h"
#import "UIAlertDialog.h"

@implementation ReachabilityUtil


+ (BOOL)canNetworkConnected {
    Reachability *curReach = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    if (netStatus == NotReachable) {
        return NO;
    }
    else {
        return YES;
    }
}

@end
