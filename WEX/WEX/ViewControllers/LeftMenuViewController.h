//
//  LeftMenuViewController.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftMenuViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *tbviewContent;

@end
