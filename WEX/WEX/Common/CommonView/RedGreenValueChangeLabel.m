//
//  RedGreenValueChangeLabel.m
//  BittrexTrade
//
//  Created by Ta Duong Ngoc on 1/9/18.
//  Copyright © 2018 Ta Duong Ngoc. All rights reserved.
//

#import "RedGreenValueChangeLabel.h"
#import "Const.h"

@implementation RedGreenValueChangeLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void) setTextNumber:(NSString *)oldText withNewText: (NSString *)newText withTypeBackground: (BOOL)isBackground {

    NSComparisonResult result = [newText compare:oldText];
    
    self.text = newText;
    
    if (result == NSOrderedAscending) { // text < oldText
        if (isBackground) {
            self.backgroundColor = COLOR_RED;
        }
        else {
            self.textColor = COLOR_RED;
        }
    }
    else if (result == NSOrderedDescending) { // stringOne > stringTwo
        if (isBackground) {
            self.backgroundColor = COLOR_GREEN;
        }
        else {
            self.textColor = COLOR_GREEN;
        }
    }
    else {
        if (isBackground) {
            self.backgroundColor = COLOR_BLACK_COMMON;
        }
        else {
            self.textColor = COLOR_BLACK_COMMON;
        }
    }
}

@end
