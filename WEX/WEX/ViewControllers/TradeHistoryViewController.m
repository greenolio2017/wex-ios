//
//  TradesViewController.m
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/29.
//  Copyright © 2017年 Ta Duong Ngoc. All rights reserved.
//

#import "TradeHistoryViewController.h"
#import "CommonUtil.h"
#import "Const.h"
#import "ProgressDialogUtil.h"
#import "TradeHistoryTableViewCell.h"
#import "TradeHistoryManager.h"
#import "TradeHistoryInfo.h"

@interface TradeHistoryViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tradHistoryTableView;
@property (weak, nonatomic) IBOutlet UILabel *noTradeHistoryLabel;

@property (nonatomic) NSArray *tradeHistories;

@end

@implementation TradeHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tradeHistoryUpdate) name:TRADE_HISTORY_UPDATE object:nil];
    
    
    [[ProgressDialogUtil sharedManager] showLoading:self.view alpha:0.2f];
    [[TradeHistoryManager sharedManager] updateTradeHistory];
    
    // tableView setup
    [self.tradHistoryTableView registerNib:[UINib nibWithNibName:@"TradeHistoryTableViewCell" bundle:nil] forCellReuseIdentifier:@"TradeHistoryTableViewCell"];
    self.tradHistoryTableView.tableFooterView = [[UIView alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)tradeHistoryUpdate {
    NSSortDescriptor *lastDescriptor =
    [[NSSortDescriptor alloc]
      initWithKey:@"timestamp"
      ascending:NO
      selector:@selector(compare:)];
    
    NSArray * descriptors = [NSArray arrayWithObjects:lastDescriptor, nil];
    self.tradeHistories = [[NSArray alloc] initWithArray:[[TradeHistoryManager sharedManager].tradeHistories sortedArrayUsingDescriptors:descriptors]];
    
    if ([self.tradeHistories count] <= 0) {
        self.tradHistoryTableView.hidden = YES;
        self.noTradeHistoryLabel.hidden = NO;
    }
    else {
        self.tradHistoryTableView.hidden = NO;
        self.noTradeHistoryLabel.hidden = YES;
    }
    
    [self.tradHistoryTableView reloadData];
}


#pragma mark <UITableViewDelegate>
/**
 ----------------------------------------------------------------------------------------------------
 * @brief heightForRowAtIndexPath
 ----------------------------------------------------------------------------------------------------
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

/**
 ----------------------------------------------------------------------------------------------------
 * @brief didSelectRowAtIndexPath
 ----------------------------------------------------------------------------------------------------
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}

#pragma mark <UITableViewDataSource>
/**
 ----------------------------------------------------------------------------------------------------
 * @brief numberOfSectionsInTableView
 ----------------------------------------------------------------------------------------------------
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


/**
 ----------------------------------------------------------------------------------------------------
 * @brief numberOfRowsInSection
 ----------------------------------------------------------------------------------------------------
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.tradeHistories count];
}


/**
 ----------------------------------------------------------------------------------------------------
 * @brief cellForRowAtIndexPath
 ----------------------------------------------------------------------------------------------------
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TradeHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TradeHistoryTableViewCell"];
    if (cell == nil) {
        cell = [[TradeHistoryTableViewCell alloc] init];
    }
    
    if ([self.tradeHistories count] <= 0) {
        return cell;
    }
  
    TradeHistoryInfo *tradeHistoryInfo = [self.tradeHistories objectAtIndex:indexPath.row];
    cell.pairLabel.text = [CommonUtil pairStringToStringDisplay: tradeHistoryInfo.pair];
    cell.typeLabel.text = tradeHistoryInfo.type;
    cell.amountLabel.text = [CommonUtil dealWithDoubleWithEightDecimal:[tradeHistoryInfo.amount doubleValue]];
    cell.priceLabel.text = [CommonUtil dealWithDoubleWithFiveDecimal:[tradeHistoryInfo.rate doubleValue]];
    
    if ([[cell.typeLabel.text uppercaseString]  isEqual: @"SELL"]) {
        cell.typeLabel.text = @"SELL";
        cell.typeLabel.textColor = COLOR_RED;
    }
    else {
        cell.typeLabel.text = @"BUY";
        cell.typeLabel.textColor = COLOR_GREEN;
    }
    
    NSTimeInterval timeInterval=[tradeHistoryInfo.timestamp doubleValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
    [dateformatter setLocale:[NSLocale currentLocale]];
    
    // date
    [dateformatter setDateFormat:@"yyyy/MM/dd"];
    NSString *dateString=[dateformatter stringFromDate:date];
    cell.dateLabel.text = dateString;
    
    [dateformatter setDateFormat:@"HH:mm:ss"];
    NSString *timeString=[dateformatter stringFromDate:date];
    cell.timeLabel.text = timeString;
    
    cell.indexPathCell = indexPath;
    return cell;
}

- (IBAction)backButtonTouchUpInside:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
