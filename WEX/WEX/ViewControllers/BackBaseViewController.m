//
//  BackBaseViewController.m
//  WEX
//
//  Created by Ngoc on 6/15/18.
//  Copyright © 2018 Ta Duong Ngoc. All rights reserved.
//

#import "BackBaseViewController.h"

@interface BackBaseViewController ()

@end

@implementation BackBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // update navigation bar
    // update navigation bar
    UIImage *myImage = [[UIImage imageNamed:@"icon_back_left"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:myImage style:UIBarButtonItemStylePlain target:self action:@selector(goPrevious)];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem.tintColor = [UIColor blackColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)goPrevious {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
