//
//  MainViewController.m
//  eExmo
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "MainViewController.h"
#import <WebKit/WebKit.h>
#import "Const.h"
#import "CommonUtil.h"
#import "MarketsManager.h"
#import "LoggingCommon.h"
#import "CommonUtil.h"
#import "MarketsViewController.h"
#import "DetailInfoViewController.h"
#import "CAPSPageMenu.h"
#import "FirebaseAnalytics.h"

#import "FruitIAPHelper.h"

@interface MainViewController () <CAPSPageMenuDelegate>
@property (nonatomic) CAPSPageMenu *pageMenu;
@property (nonatomic) UITapGestureRecognizer *singleTap;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *pageViewArea;


@property NSMutableArray *depthAsks;
@property double maxDepthAskAmount;
@property NSMutableArray *depthBids;
@property double maxDepthBidAmount;
@property NSMutableArray *tradesInfo;


@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Observer
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDataTicker) name:TICKER_MANAGER_UPDATE_TICKERS object:nil];
    
    self.blackOutView.hidden = YES;
    
    // create a tap gesture recognizer
    self.singleTap = [[UITapGestureRecognizer alloc]  initWithTarget:self action:@selector(tapOnBlackOutView)];
    self.singleTap.numberOfTouchesRequired = 1;
    self.singleTap.numberOfTapsRequired = 1;
    
    // init data
    [self updateDataTicker];
    
    // Favorite Market ViewController
    MarketsViewController *favoriteMarketVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"marketsVC"];
    favoriteMarketVC.title = @"FAV";
    
    // Btc Market ViewController
    MarketsViewController *btcMarketVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"marketsVC"];
    btcMarketVC.title = @"BTC";
    
    // Eth Market ViewController
    MarketsViewController *ethMarketVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"marketsVC"];
    ethMarketVC.title = @"ETH";
    
    // Usd market ViewController
    MarketsViewController *usdMarketsVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"marketsVC"];
    usdMarketsVC.title = @"USD";
    
    // All market ViewController
    MarketsViewController *allMarketsVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"marketsVC"];
    allMarketsVC.title = @"ALL";
    
    NSArray *controllerArray = @[favoriteMarketVC, btcMarketVC, ethMarketVC, usdMarketsVC, allMarketsVC];
    
    float menuHeight = 40;
    float itemWidth = (self.view.frame.size.width - ([controllerArray count] + 1)*10) / [controllerArray count];
    
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionSelectionIndicatorColor: COLOR_BLACK_COMMON,
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor clearColor],
                                 CAPSPageMenuOptionMenuItemFont: [UIFont systemFontOfSize:16.0f weight:UIFontWeightBold],
                                 CAPSPageMenuOptionMenuHeight: @(menuHeight),
                                 CAPSPageMenuOptionMenuItemWidth: @(itemWidth),
                                 CAPSPageMenuOptionCenterMenuItems: @(YES)
                                 };
    
    _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.pageViewArea.frame.size.width, self.pageViewArea.frame.size.height) options:parameters];
    [self.pageViewArea addSubview:_pageMenu.view];
    
    _pageMenu.delegate = self;
    
    // Fix the constrains
    _pageMenu.view.translatesAutoresizingMaskIntoConstraints = NO;
    [self addChildViewController:_pageMenu];
    [self.pageViewArea addSubview:_pageMenu.view];
    NSDictionary *viewsDictionary = @{
                                      @"pageMenu" : _pageMenu.view
                                      };
    
    NSArray *controllerScrollView_constraint_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[pageMenu]|" options:0 metrics:nil views:viewsDictionary];
    NSString *controllerScrollView_constraint_V_Format = [NSString stringWithFormat:@"V:|[pageMenu]|"];
    NSArray *controllerScrollView_constraint_V = [NSLayoutConstraint constraintsWithVisualFormat:controllerScrollView_constraint_V_Format options:0 metrics:nil views:viewsDictionary];
    
    [self.pageViewArea addConstraints:controllerScrollView_constraint_H];
    [self.pageViewArea addConstraints:controllerScrollView_constraint_V];
    [_pageMenu didMoveToParentViewController:self];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[FirebaseAnalytics sharedManager] logEventWithName:FIR_DISPLAY_SCREEN_PAIR_LIST parameters:nil];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) updateCurrentPair:(NSNotification *) notification
{
    LogTrace("IN");
    
    LogTrace("OUT");
}

- (void) updateDataTicker {
    LogTrace("IN");
    dispatch_async(dispatch_get_main_queue(), ^{

        /*
        TickerInfo *tickerInfo = [[TickerManager sharedManager].ticketInfoArray objectAtIndex:self.currentPairIndex];
        
        if (!tickerInfo) {
            return;
        }
        
        self.lastPrice.text = [[CommonUtil dealWithDoubleWithFiveDecimal:[tickerInfo.last doubleValue]] stringByAppendingString:[CommonUtil bidCurrencyFromPairStringForDisplaying:tickerInfo.pairString]];
        self.lowPrice.text = [[CommonUtil dealWithDoubleWithFiveDecimal:[tickerInfo.low doubleValue]] stringByAppendingString:[CommonUtil bidCurrencyFromPairStringForDisplaying:tickerInfo.pairString]];
        self.highPrice.text = [[CommonUtil dealWithDoubleWithFiveDecimal:[tickerInfo.high doubleValue]] stringByAppendingString:[CommonUtil bidCurrencyFromPairStringForDisplaying:tickerInfo.pairString]];
        
        self.volumnAskCurrency.text = [[CommonUtil dealWithDoubleWithEightDecimal:[tickerInfo.vol_cur intValue]] stringByAppendingString:[CommonUtil askCurrencyFromPairStringForDisplaying:tickerInfo.pairString]];
        self.volumnBidCurrency.text = [[CommonUtil dealWithDoubleWithEightDecimal:[tickerInfo.vol intValue]] stringByAppendingString:[CommonUtil bidCurrencyFromPairStringForDisplaying:tickerInfo.pairString]];
         */
    });
    LogTrace("OUT");
}



- (void)presentTradeViewController:(NSString*) price withQuantity: (NSString*) quantity {
//    self.definesPresentationContext = YES;
//    TradeViewController *tradeVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"tradeVC"];
//    tradeVC.price = price;
//    tradeVC.quantity = quantity;
//    tradeVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//    [self presentViewController:tradeVC animated:NO completion:nil];
}

- (IBAction)touchUpInsideMenuButton:(id)sender {
    [self tapOnBlackOutView];
}

#pragma Left Menu process
- (void)addGestureForBlackOutView {
    //[self.pageMenu setTapGesture];
    self.blackOutView.hidden = NO;
    [self.view bringSubviewToFront:self.blackOutView];
    if (![[self.blackOutView gestureRecognizers] containsObject:self.singleTap]) {
        [self.blackOutView addGestureRecognizer:self.singleTap];
    }
}

- (void)removeGestureForBlackOutView {
    //[self.pageMenu setTapGesture];
    self.blackOutView.hidden = YES;
    [self.view sendSubviewToBack:self.blackOutView];
    [self.blackOutView addGestureRecognizer:self.singleTap];
    
}

- (void)changeBlackOutViewAlpha: (float) alpha {
    //[self.pageMenu setTapGesture];
    [self.view bringSubviewToFront:self.blackOutView];
    self.blackOutView.alpha = alpha;
    
}

- (void)tapOnBlackOutView {
    [[NSNotificationCenter defaultCenter] postNotificationName:TOOGLE_LEFT_MENU object:nil];
}

#pragma: Page menu delegate
//- (void)didTapGoToLeft {
//    NSInteger currentIndex = self.pageMenu.currentPageIndex;
//
//    if (currentIndex > 0) {
//        [_pageMenu moveToPage:currentIndex - 1];
//    }
//}
//
//- (void)didTapGoToRight {
//    NSInteger currentIndex = self.pageMenu.currentPageIndex;
//
//    if (currentIndex < self.pageMenu.controllerArray.count) {
//        [self.pageMenu moveToPage:currentIndex + 1];
//    }
//}

- (void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    [[FirebaseAnalytics sharedManager] logEventWithName:FIR_ACTION_TOUCH_EVENT_ON_CHANGE_PAIR_TAB parameters:nil];
}

@end
