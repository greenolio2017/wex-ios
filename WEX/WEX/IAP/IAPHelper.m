//
//  IAPHelper.m
//  BuyFruit
//
//  Created by Michael Beyer on 12.09.13.
//  Copyright (c) 2013 Michael Beyer. All rights reserved.
//

#import "IAPHelper.h"
#import "DialogUtil.h"
#import "LoggingCommon.h"
#import "ProgressDialogUtil.h"

// You need to use StoreKit to access the In-App Purchase APIs, so you import the StoreKit here.
@import StoreKit;

NSString *const IAPHelperProductPurchasedNotification = @"IAPHelperProductPurchasedNotification";
/*
 To receive a list of products from StoreKit, you need to implement the SKProductsRequestDelegate protocol.
 Here you mark the class as implementing this protocol in the class extension.
 For purchasing: modify the class extension to mark the class as implementing the SKPaymentTransactionObserver:
 */
@interface IAPHelper () <SKProductsRequestDelegate, SKPaymentTransactionObserver>
@end

@implementation IAPHelper
{
/*
 You create an instance variable to store the SKProductsRequest you will issue to retrieve a list of products, while it is active.
 */
    SKProductsRequest *_productsRequest;
    // You also keep track of the completion handler for the outstanding products request, ...
    RequestProductsCompletionHandler _completionHandler;
    // ... the list of product identifiers passed in, ...
    NSSet *_productIdentifiers;
    // ... and the list of product identifiers that have been previously purchased.
    NSMutableSet * _purchasedProductIdentifiers;
}

// Initialitzer to check which products have been purchased or not
- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers
{
    self = [super init];
    if (self) {
        // Store product identifiers
        _productIdentifiers = productIdentifiers;
        
        // Check for previously purchased products
        // This is important in order to check if a user already purchased products, so that we can show them to the user ...
        _purchasedProductIdentifiers = [NSMutableSet set];
        for (NSString *productIdentifier in _productIdentifiers) {
            // TODO: create a BOOL value named "productPurchased" and return a BOOL value for a given productIdentifier (boolForKey) for NSUserDefaults' standardUserDefaults method
            // TODO: once you implemented this, uncomment the if-else statement. Everything should build just fine.
            BOOL productPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:productIdentifier];
            if (productPurchased) {
                [_purchasedProductIdentifiers addObject:productIdentifier];
                LogTrace(@"Previously purchased: %@", productIdentifier);
            } else {
                LogTrace(@"Not purchased: %@", productIdentifier);
            }
        }
        // add self as transaction observer
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    }
    return self;
}

- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler
{
    // a copy of the completion handler block inside the instance variable so it can notify the caller when the product request asynchronously completes
    _completionHandler = [completionHandler copy];
    // Create a new instance of SKProductsRequest, which is the Apple-written class that contains the code to pull the info from iTunes Connect
    _productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:_productIdentifiers];
    _productsRequest.delegate = self;
    [_productsRequest start];
}

#pragma mark - SKProductsRequestDelegate

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    LogTrace(@"Loaded products...");
    _productsRequest = nil;
    
    NSArray *skProducts = response.products;
    //NGOC
//    for (SKProduct *skProduct in skProducts) {
//        LogTrace(@"Found product: %@ – Product: %@ – Price: %0.2f", skProduct.productIdentifier, skProduct.localizedTitle, skProduct.price.floatValue);
//    }
    
    // method definition; (BOOL success, NSArray * products) ... success YES, and the array of products is skProducts
    _completionHandler(YES, skProducts);
    _completionHandler = nil;
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
//    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Failed to load list of products."
//                                                      message:nil
//                                                     delegate:nil
//                                            cancelButtonTitle:@"OK"
//                                            otherButtonTitles:nil];
//    [message show];
    
    [DialogUtil showDialogWithError:@"Load failed" withError:@"Failed to load list of products." withButtonName:@"Close" buttonHandler:^{
        [DialogUtil dismissDialog];
    }];
    
    LogTrace(@"Failed to load list of products.");
    
    _productsRequest = nil;
    
    // method definition; (BOOL success, NSArray * products) ... success NO, and the array of products is nil
    _completionHandler(NO, nil);
    _completionHandler = nil;
}

- (BOOL)productPurchased:(NSString *)productIdentifier
{
    return [_purchasedProductIdentifiers containsObject:productIdentifier];
}

- (void)buyProduct:(SKProduct *)product
{
    LogTrace(@"Buying %@ ... (buyProduct ind IAPHelper)", product.productIdentifier);
    
//    TODO: create a SKPayment object ("payment") and call paymentWithProduct that returns a new payment for the specified product ("product)". (hint: 1 LOC)
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    
//    TODO: issue the SKPayment to the SKPaymentQueue: make the SKPaymentQueue class call the defaultQueue method and add a payment request to the queue (addPayment) for a given payment ("payment"). (hint: 1 LOC)
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];                
                [[ProgressDialogUtil sharedManager] hideLoading];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                [[ProgressDialogUtil sharedManager] hideLoading];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
                [[ProgressDialogUtil sharedManager] hideLoading];
            default:
                break;
        }
    };
}

// called when the transaction was successful
- (void)completeTransaction:(SKPaymentTransaction *)transaction
{
    LogTrace(@"completeTransaction...");
    
//    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Bought successfully!"
//                                                      message:@"Thank you for your purchase. Enjoy!"
//                                                     delegate:nil
//                                            cancelButtonTitle:@"OK"
//                                            otherButtonTitles:nil];
//    [message show];
    
    [DialogUtil showDialogWithError:@"Bought successfully!" withError:@"Thank you for your purchase. Enjoy!" withButtonName:@"OK" buttonHandler:^{
        [DialogUtil dismissDialog];
    }];
    
    [self provideContentForProductIdentifier:transaction.payment.productIdentifier];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:transaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

// called when a transaction has been restored and successfully completed
- (void)restoreTransaction:(SKPaymentTransaction *)transaction
{
    LogTrace(@"restoreTransaction...");
    
    [self provideContentForProductIdentifier:transaction.originalTransaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    
    [DialogUtil showDialogWithError:@"Restored successfully!" withError:@"Thank you for your purchase. Enjoy!" withButtonName:@"OK" buttonHandler:^{
        [DialogUtil dismissDialog];
    }];

}

// called when a transaction has failed
- (void)failedTransaction:(SKPaymentTransaction *)transaction
{
    LogTrace(@"failedTransaction...");
    
    if (transaction.error.code != SKErrorPaymentCancelled) {
        LogTrace(@"Transaction error: %@", transaction.error.localizedDescription);
       
//        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Ups!"
//                                                          message:transaction.error.localizedDescription
//                                                         delegate:nil
//                                                cancelButtonTitle:@"OK"
//                                                otherButtonTitles:nil];
//        [message show];
        [DialogUtil showDialogWithError:@"Oops!" withError:transaction.error.localizedDescription withButtonName:@"Close" buttonHandler:^{
            [DialogUtil dismissDialog];
        }];
    }
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void)provideContentForProductIdentifier:(NSString *)productIdentifier
{
    LogTrace(@"provideContentForProductIdentifier");
    
    [_purchasedProductIdentifiers addObject:productIdentifier];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:productIdentifier];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchasedNotification
                                                        object:productIdentifier
                                                      userInfo:nil];
}

- (void)restoreCompletedTransactions
{
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
    [[ProgressDialogUtil sharedManager] hideLoading];
    [DialogUtil showDialogWithError:@"Oops!" withError:error.localizedDescription withButtonName:@"Close" buttonHandler:^{
        [DialogUtil dismissDialog];
    }];
}

@end
