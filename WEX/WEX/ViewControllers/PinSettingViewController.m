//
//  PinSettingViewController.m
//  WEX
//
//  Created by Ta Duong Ngoc on 6/8/18.
//  Copyright © 2018 Ta Duong Ngoc. All rights reserved.
//

#import "PinSettingViewController.h"
#import "TOPasscodeViewControllerConstants.h"
#import "TOPasscodeSettingsViewController.h"
#import "Const.h"

@interface PinSettingViewController ()<TOPasscodeSettingsViewControllerDelegate>
@property (nonatomic, copy) NSString *passcode;
@property (nonatomic, assign) TOPasscodeType passcodeType;
@property (nonatomic, assign) TOPasscodeViewStyle style;

@property (weak, nonatomic) IBOutlet UIButton *createPINBtn;
@property (weak, nonatomic) IBOutlet UIButton *changePINBtn;
@property (weak, nonatomic) IBOutlet UIButton *resetPINBtn;

@end

@implementation PinSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    // update passcode
    self.passcode = [[NSUserDefaults standardUserDefaults] stringForKey:USERDEFAULT_KEY_PASSCODE];
    if (self.passcode == nil) {
        self.passcode = @"";
    }
    
    // update passcode type
    self.passcodeType = [[[NSUserDefaults standardUserDefaults] objectForKey:USERDEFAULT_KEY_PASSCODE_TYPE] integerValue];
    
    self.style = TOPasscodeViewStyleOpaqueLight;
    
    // change button state
    if ([self.passcode  isEqual: @""]) {
        self.createPINBtn.enabled = YES;
        self.changePINBtn.enabled = NO;
        self.resetPINBtn.enabled = NO;
    }
    else {
        self.createPINBtn.enabled = NO;
        self.changePINBtn.enabled = YES;
        self.resetPINBtn.enabled = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)backButtonTouchUpInside:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)createPINButtonTouchUpInside:(id)sender {
    TOPasscodeSettingsViewController *settingsController = [[TOPasscodeSettingsViewController alloc] init];
    settingsController.passcodeType = self.passcodeType;
    settingsController.delegate = self;
    settingsController.requireCurrentPasscode = NO;
    [self.navigationController pushViewController:settingsController animated:YES];
}

- (IBAction)changePINButtonTouchUpInside:(id)sender {
    TOPasscodeSettingsViewController *settingsController = [[TOPasscodeSettingsViewController alloc] init];
    settingsController.passcodeType = self.passcodeType;
    settingsController.delegate = self;
    settingsController.requireCurrentPasscode = YES;
    [self.navigationController pushViewController:settingsController animated:YES];
}

- (IBAction)resetPINButtonTouchUpInside:(id)sender {
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Warning"
                                 message:@"In order to protect your account. If you reset your PIN number, the API KEY will be reset also. You have to re-input your API KEY"
                                 preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = COLOR_BLUE;
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Reset"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:USERDEFAULT_KEY_PASSCODE];
                                    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:USERDEFAULT_KEY_API_KEY];
                                    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:USERDEFAULT_KEY_SECRET_KEY];
                                    [self dismissViewControllerAnimated:YES completion:nil];
                                }];
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   // do nothing
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];

}

- (void)goPrevious {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Passcode Settings Controller Delegate -

- (BOOL)passcodeSettingsViewController:(TOPasscodeSettingsViewController *)passcodeSettingsViewController didAttemptCurrentPasscode:(NSString *)passcode
{
    return [passcode isEqualToString:self.passcode];
}

- (void)passcodeSettingsViewController:(TOPasscodeSettingsViewController *)passcodeSettingsViewController didChangeToNewPasscode:(NSString *)passcode ofType:(TOPasscodeType)type
{
    self.passcode = passcode;
    self.passcodeType = type;
    
    [[NSUserDefaults standardUserDefaults] setValue:self.passcode forKey:USERDEFAULT_KEY_PASSCODE];
    [[NSUserDefaults standardUserDefaults] setObject:@(self.passcodeType) forKey:USERDEFAULT_KEY_PASSCODE_TYPE];
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
