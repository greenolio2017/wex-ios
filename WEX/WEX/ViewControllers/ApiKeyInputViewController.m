//
//  ApiKeyInputViewController.m
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/29.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "ApiKeyInputViewController.h"
#import "Const.h"
#import "DialogUtil.h"
#import "CommonUtil.h"
#import "WEX-Swift.h"

@interface ApiKeyInputViewController ()
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITextField *keyTextField;
@property (weak, nonatomic) IBOutlet UITextField *secretTextField;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;

@end

@implementation ApiKeyInputViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    KeyboardAvoiding.avoidingView = self.contentView;
    self.keyTextField.text = [[NSUserDefaults standardUserDefaults] valueForKey:USERDEFAULT_KEY_API_KEY];
    
    NSString *secretKey= [[NSUserDefaults standardUserDefaults] valueForKey:USERDEFAULT_KEY_SECRET_KEY];
    self.secretTextField.text  = secretKey;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (IBAction)backButtonTouchUpInside:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveButtonTouchUpInside:(id)sender {
    
    [self.keyTextField resignFirstResponder];
    [self.secretTextField resignFirstResponder];
    
    if (![self.keyTextField.text  isEqual: @""] &&
        ![self.secretTextField.text  isEqual: @""]) {
        [[NSUserDefaults standardUserDefaults] setValue:self.keyTextField.text forKey:USERDEFAULT_KEY_API_KEY];
        [[NSUserDefaults standardUserDefaults] setValue:self.secretTextField.text forKey:USERDEFAULT_KEY_SECRET_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];

        [CommonUtil updateApiKeyForAllManagers];
        
        [DialogUtil showDialogWithError:@"Info" withError:@"API key is already saved. You are able to get Active Orders, Trade History, Funds..." withButtonName:@"OK" buttonHandler:^{
            [DialogUtil dismissDialog];
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }];
    }
    else {
        [DialogUtil showDialogWithError:@"Error" withError:@"You must input all Key and Secret to complete setting" withButtonName:@"OK" buttonHandler:^{
            [DialogUtil dismissDialog];
        }];
    }
}

- (IBAction)helpButtonTouchUpInside:(id)sender {
}

- (IBAction)gotoWEXWebsiteTouchUpInside:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://wex.nz/profile#api_keys"]];
}

@end
