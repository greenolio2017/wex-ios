//
//  TradeManager.m
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/22.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "TradeManager.h"
#import "WexApiHandler.h"
#import "Const.h"
#import "TradeApiProtocol.h"
#import "DialogUtil.h"
#import "CommonUtil.h"
#import "ProgressDialogUtil.h"
#import "MarketsManager.h"

@implementation TradeManager 
{
    NSTimer *tracker;
}

+ (TradeManager *)sharedManager {
    static TradeManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


// Initialization

- (id)init {
    self = [super init];
    
    if (self) {
        self.apiHandler = [[WexApiHandler alloc] init];
    }
    //self.currentPair = DEFAULT_PAIR;
    
    self.info = [[NSDictionary alloc] init];
    self.funds = [[NSDictionary alloc] init];
    self.rights = [[NSDictionary alloc] init];
    
    return self;
}

//- (void)setTimerActive: (BOOL) isStart {
// TODO: do not need to set timer for updating Trade Info
//    if (isStart){
//        tracker = [NSTimer scheduledTimerWithTimeInterval:5.0f
//                                                   target:self
//                                                 selector:@selector(updateAccountInfo)
//                                                 userInfo:nil repeats:YES];
//        
//    } else {
//        [tracker invalidate];
//    }
//}

- (void)sell:(NSString *)pair withRate: (NSString *) rate withAmount: (NSString *) amount {
    NSMutableDictionary *post = [[NSMutableDictionary alloc] init];
    [post setObject:@"Trade" forKey:@"method"];
    [post setObject:pair forKey:@"pair"];
    [post setObject:@"sell" forKey:@"type"];
    [post setObject:rate forKey:@"rate"];
    [post setObject:amount forKey:@"amount"];
    self.apiHandler.tradeDelegate = self;
    [self.apiHandler getResponseFromServerForPost:post withApi: @"sell"];
}

- (void)buy:(NSString *)pair withRate: (NSString *) rate withAmount: (NSString *) amount {
    NSMutableDictionary *post = [[NSMutableDictionary alloc] init];
    [post setObject:@"Trade" forKey:@"method"];
    [post setObject:pair forKey:@"pair"];
    [post setObject:@"buy" forKey:@"type"];
    [post setObject:rate forKey:@"rate"];
    [post setObject:amount forKey:@"amount"];
    self.apiHandler.tradeDelegate = self;
    [self.apiHandler getResponseFromServerForPost:post withApi: @"buy"];
}

- (void)updateAccountInfo {
    dispatch_async(kBgQueue, ^{
        NSMutableDictionary *post = [[NSMutableDictionary alloc] init];
        [post setObject:@"getInfo" forKey:@"method"];
        self.apiHandler.tradeDelegate = self;
        [self.apiHandler getResponseFromServerForPost:post withApi: @"info"];
    });
}

#pragma TradeApiProtocol
- (void)updateInfo:(NSData *)data {
    
    dispatch_async(kBgQueue, ^{
        
        // hide loading
        dispatch_async(dispatch_get_main_queue(), ^{
            [[ProgressDialogUtil sharedManager] hideLoading];
        });
        
        if (!data) {
            return;
        }
        NSError *error;
        NSDictionary *json = [NSJSONSerialization
                              JSONObjectWithData:data
                              options:kNilOptions
                              error:&error];
        // Server error
        if (error != nil && ![error  isEqual: @""]) {
            [DialogUtil showDialogWithError:@"Error" withError:error.domain withButtonName:@"OK" buttonHandler:^{
                [DialogUtil dismissDialog];
            }];
            return;
        }
        
        // Trade error
        NSString *errorInResponse = [json objectForKey:@"error"];
        if (errorInResponse != nil && ![errorInResponse  isEqual: @""]) {
            [DialogUtil showDialogWithError:@"Error" withError:errorInResponse withButtonName:@"OK" buttonHandler:^{
                [DialogUtil dismissDialog];
            }];
            return;
        }
        
        self.info = [json objectForKey:@"return"];
        self.funds = [self.info objectForKey:@"funds"];
        self.rights = [self.info objectForKey:@"rights"];
        self.open_orders = [self.info objectForKey:@"open_orders"];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:
             TRADE_INFO_UPDATE object:nil userInfo:nil];
        });
    });
}

- (void)buy:(NSData *)data {
    dispatch_async(kBgQueue, ^{
        // hide loading
        dispatch_async(dispatch_get_main_queue(), ^{
            [[ProgressDialogUtil sharedManager] hideLoading];
        });
        
        if (!data) {
            return;
        }
        
        NSError *error;
        NSDictionary *json = [NSJSONSerialization
                              JSONObjectWithData:data
                              options:kNilOptions
                              error:&error];
        // Server error
        if (error != nil && ![error  isEqual: @""]) {
            [DialogUtil showDialogWithError:@"Error" withError:error.domain withButtonName:@"OK" buttonHandler:^{
                [DialogUtil dismissDialog];
            }];
            return;
        }
        
        // Trade error
        NSString *errorInResponse = [json objectForKey:@"error"];
        if (errorInResponse != nil &&
            ![errorInResponse  isEqual: @""] &&
            ![[errorInResponse uppercaseString]  containsString:@"NO TRADE"]) {
            [DialogUtil showDialogWithError:@"Error" withError:errorInResponse withButtonName:@"OK" buttonHandler:^{
                [DialogUtil dismissDialog];
            }];
            return;
        }
        
        NSDictionary *jsonInfo = [json objectForKey:@"return"];
        NSString *order_id = [jsonInfo objectForKey:@"order_id"];
        
        NSDictionary* userInfo = [NSDictionary dictionaryWithObject:order_id
                                                             forKey:@"order_id"];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[NSNotificationCenter defaultCenter] postNotificationName: TRADE_ORDER_CREATED object:nil userInfo:userInfo];
            
        });
        
    });
}

- (void)sell:(NSData *)data {
    dispatch_async(kBgQueue, ^{
        // hide loading
        dispatch_async(dispatch_get_main_queue(), ^{
            [[ProgressDialogUtil sharedManager] hideLoading];
        });
        
        if (!data) {
            return;
        }
        NSError *error;
        NSDictionary *json = [NSJSONSerialization
                              JSONObjectWithData:data
                              options:kNilOptions
                              error:&error];
        
        // Server error
        if (error != nil && ![error  isEqual: @""]) {
            [DialogUtil showDialogWithError:@"Error" withError:error.domain withButtonName:@"OK" buttonHandler:^{
                [DialogUtil dismissDialog];
            }];
            return;
        }
        
        // Trade error
        NSString *errorInResponse = [json objectForKey:@"error"];
        if (errorInResponse != nil && ![errorInResponse  isEqual: @""]) {
            [DialogUtil showDialogWithError:@"Error" withError:errorInResponse withButtonName:@"OK" buttonHandler:^{
                [DialogUtil dismissDialog];
            }];
            return;
        }
        
        NSDictionary *jsonInfo = [json objectForKey:@"return"];
        NSString *order_id = [jsonInfo objectForKey:@"order_id"];
        
        NSDictionary* userInfo = [NSDictionary dictionaryWithObject:order_id
                                                             forKey:@"order_id"];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[NSNotificationCenter defaultCenter] postNotificationName: TRADE_ORDER_CREATED object:nil userInfo:userInfo];
            
        });
    });
}


@end
