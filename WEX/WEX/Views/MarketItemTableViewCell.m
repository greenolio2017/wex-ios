//
//  MarketItemTableViewCell.m
//  BittrexTrade
//
//  Created by Ta Duong Ngoc on 11/16/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "MarketItemTableViewCell.h"
#import "FavoriteManager.h"

@implementation MarketItemTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    //[super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)starButtonTouchUpInside:(id)sender {
    [[FavoriteManager sharedManager] toggleFavoriteList:self.pair];
}

@end
