//
//  FirebaseAnalytics.m
//  WEX
//
//  Created by Ngoc on 7/5/18.
//  Copyright © 2018 Ta Duong Ngoc. All rights reserved.
//

#import "LoggingCommon.h"
#import "FirebaseAnalytics.h"

@implementation FirebaseAnalytics
+ (FirebaseAnalytics *)sharedManager {
    
    static FirebaseAnalytics *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    
    return sharedMyManager;
}

- (void)logEventWithName:(NSString *_Nullable)eventName parameters:(NSDictionary *_Nullable)parameters {
    LogTrace(@"IN");
    LogDebug(@"logEventWithName: %@ parameters: %@", eventName, parameters);
    
    [FIRAnalytics logEventWithName:eventName
                        parameters:parameters];
    LogTrace(@"OUT");
}

@end
