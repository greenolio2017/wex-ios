//
//  TradeViewController.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TradeViewController : UIViewController

@property (nonatomic) NSString *currentPair;
@property (nonatomic) NSString *priceAsk;
@property (nonatomic) NSString *priceBid;
@property (nonatomic) NSString *buyAmount;
@property (nonatomic) NSString *sellAmount;
@end

