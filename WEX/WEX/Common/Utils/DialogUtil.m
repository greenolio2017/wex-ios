//
//  DialogUtil.m
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/29.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "DialogUtil.h"
#import "UIAlertDialog.h"
#import "CommonUtil.h"
#import "TradeViewController.h"
#import "ActiveOrdersViewController.h"
#import "OrderCreatedNotificationViewController.h"

@implementation DialogUtil

+ (void)showDialogCannotConnectNetwork {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIAlertDialog sharedManager] alertWithTitle:@"Network Error" message:@"Cannot connect to the internet. Please check your connection " buttonTitle:@"OK" buttonHandler:^{
            [[UIAlertDialog sharedManager] dismiss];
            
        }];
        
        [[UIAlertDialog sharedManager] show];
    });
}

+ (void)showDialogWithError: (NSString *) title withError: (NSString *) error withButtonName: (NSString *) buttonName buttonHandler:(void (^)(void))buttonHandler {
    if ([error containsString:@"invalid nonce"]) {
        [self showDialogNounceIssue];
    }
    else {
        [self showCommonDialogWithOKButton:title withError:error withButtonName:buttonName buttonHandler:buttonHandler];
    }
    
}

+ (void)dismissDialog {
    [[UIAlertDialog sharedManager] dismiss];
}


#pragma Error Dialogs
+ (void)showDialogNounceIssue {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if ([[CommonUtil topMostController] isKindOfClass: [TradeViewController class]] ||
            [[CommonUtil topMostController] isKindOfClass: [ActiveOrdersViewController class]] ||
            [[CommonUtil topMostController] isKindOfClass: [OrderCreatedNotificationViewController class]]) {
            
            [[UIAlertDialog sharedManager] alertWithTitle:@"Nounce Error" message:@"There is a problem with your API key.\nPlease check your date/time on your device. Then create a new API Key" buttonTitle:@"OK" buttonHandler:^{
                [[UIAlertDialog sharedManager] dismiss];
                
            }];
            
            [[UIAlertDialog sharedManager] show];
        }
    });
}

+ (void)showCommonDialogWithOKButton: (NSString *) title withError: (NSString *) error withButtonName: (NSString *) buttonName buttonHandler:(void (^)(void))buttonHandler {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[UIAlertDialog sharedManager] alertWithTitle:[title uppercaseString] message:[error uppercaseString] buttonTitle:buttonName buttonHandler:buttonHandler];
        
        [[UIAlertDialog sharedManager] show];
    });
}

@end
