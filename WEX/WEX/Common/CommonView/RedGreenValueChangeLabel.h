//
//  RedGreenValueChangeLabel.h
//  BittrexTrade
//
//  Created by Ta Duong Ngoc on 1/9/18.
//  Copyright © 2018 Ta Duong Ngoc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RedGreenValueChangeLabel : UILabel

- (void) setTextNumber:(NSString *)oldText withNewText: (NSString *)newText withTypeBackground: (BOOL)isBackground;

@end
