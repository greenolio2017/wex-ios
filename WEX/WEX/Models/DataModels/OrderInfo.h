//
//  OrderInfo.h
//  WEX
//
//  Created by Ta Duong Ngoc on 12/4/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderInfo : NSObject
@property NSString *orderId;
@property NSString *pair;
@property NSString *type;
@property NSNumber *start_amount;
@property NSNumber *amount;
@property NSNumber *rate;
@property NSNumber *timestamp_created;
@property NSNumber *status;
@end
