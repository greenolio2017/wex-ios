//
//  CommonUtil.m
//  WEX
//
//  Created by Ta Duong Ngoc on 11/14/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "CommonUtil.h"
#import "TickerManager.h"
#import "DepthManager.h"
#import "TradesManager.h"
#import "TradeManager.h"
#import "GraphDataManager.h"
#import "GraphNewDataManager.h"
#import "TradeHistoryManager.h"
#import "Const.h"
#import "LoadingView.h"
#import "OrderManager.h"
#import "AppDelegate.h"

@implementation CommonUtil

+ (NSString *) pairStringToStringDisplay: (NSString*) pairString {
    NSArray *listItems = [pairString componentsSeparatedByString:@"_"];
    
    NSString * askCurrency = [[listItems objectAtIndex:0] uppercaseString];
    NSString * bidCurrency = [[listItems objectAtIndex:1] uppercaseString];
    
    return [[askCurrency stringByAppendingString:@" / "] stringByAppendingString:bidCurrency];
}

+ (NSString *) askCurrencyFromPairString: (NSString*) pairString {
    NSArray *listItems = [pairString componentsSeparatedByString:@"_"];
    NSString * askCurrency = [listItems objectAtIndex:0];
    return askCurrency;
}

+ (NSString *) askCurrencyFromPairStringForDisplaying: (NSString*) pairString {
    
    NSArray *listItems = [pairString componentsSeparatedByString:@"_"];
    NSString * askCurrency = [[listItems objectAtIndex:0] uppercaseString];
    return [@" " stringByAppendingString: [askCurrency uppercaseString]];
}


+ (NSString *) bidCurrencyFromPairString: (NSString*) pairString {
    NSArray *listItems = [pairString componentsSeparatedByString:@"_"];
    NSString * bidCurrency = [listItems objectAtIndex:1];
    return bidCurrency;
}

+ (NSString *) bidCurrencyFromPairStringForDisplaying: (NSString*) pairString {
    NSArray *listItems = [pairString componentsSeparatedByString:@"_"];
    NSString * bidCurrency = [[listItems objectAtIndex:1] uppercaseString];
    return [@" " stringByAppendingString: [bidCurrency uppercaseString]];
}

+ (NSString *)dealWithDoubleWithFiveDecimal:(double)doubleValue {
    double d2 = doubleValue;
    NSString *d2Str = [NSString stringWithFormat:@"%0.5lf", d2];
    NSDecimalNumber *num = [NSDecimalNumber decimalNumberWithString:d2Str];
    NSString *str2D = [num stringValue];
    return str2D;
}

+ (NSString *)dealWithDoubleWithEightDecimal:(double)doubleValue {
    double d2 = doubleValue;
    NSString *d2Str = [NSString stringWithFormat:@"%.8lf", d2];
    NSDecimalNumber *num = [NSDecimalNumber decimalNumberWithString:d2Str];
    NSString *str2D = [num stringValue];
    return str2D;
}

+ (void) setCornerAndShadow: (UIView *) view withCornerRadius: (float) cornerRadius {
    // border radius
    [view.layer setCornerRadius: cornerRadius];
//    
//    // border
//    [view.layer setBorderColor:[UIColor lightGrayColor].CGColor];
//    [view.layer setBorderWidth:0.2f];
    
    // drop shadow
    [view.layer setShadowColor:[UIColor blackColor].CGColor];
    [view.layer setShadowOpacity:0.1];
    [view.layer setShadowRadius:0.5];
    [view.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
}

//+ (void)initAllManagersAndWKWebviewCache: (NSString *)pair {
//    // Init ticker manager
//    TickerManager *tickerManager = [TickerManager sharedManager];
//    [tickerManager updateAllTickers];
//    [tickerManager setTimerActive:YES];
//    
//    // init depth
//    DepthManager *depthManager = [DepthManager sharedManager];
//    depthManager.currentPair = pair;
//    [depthManager updateDepth];
//    [depthManager setTimerActive:YES];
//    
//    // init depth
//    TradesManager *tradesManager = [TradesManager sharedManager];
//    tradesManager.currentPair = pair;
//    [tradesManager updateTrades];
//    [tradesManager setTimerActive:YES];
//    
//    // Init old graph data manager
////    GraphDataManager *graphDataManager = [GraphDataManager sharedManager];
////    graphDataManager.currentPair = pair;
////    [graphDataManager getHTMLStringFromURL:pair];
////    [graphDataManager setTimerActive:YES];
//    
//    // Init Trade Manager
//    TradeManager *tradeManager = [TradeManager sharedManager];
//    tradeManager.currentPair = pair;
//    
//    // Init Order Manager
//    OrderManager *orderManager = [OrderManager sharedManager];
//    orderManager.currentPair = pair;
//}

+ (void)updateCurrentPairInManagers: (NSString *)pair {
    // Ticker is get all pair. Don't need to update.
    //[TickerManager sharedManager].currentpair = pair;
    
//    [GraphDataManager sharedManager].currentPair = pair;
//    [[GraphDataManager sharedManager] getHTMLStringFromURL: pair];
    
    [DepthManager sharedManager].currentPair = pair;
    [[DepthManager sharedManager] updateDepth];
    [TradesManager sharedManager].currentPair = pair;
    [[TradesManager sharedManager] updateTrades];
    [TradeManager sharedManager].currentPair = pair;
    [OrderManager sharedManager].currentPair = pair;
}

+ (void)updateApiKeyForAllManagers {
    [[TradeManager sharedManager].apiHandler setupInitialValues];
    [[OrderManager sharedManager].apiHandler setupInitialValues];
    [[TradeHistoryManager sharedManager].apiHandler setupInitialValues];
}

+ (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

+ (BOOL) isPINCheckAlready
{
    BOOL ret = YES;
    
    if ([[NSUserDefaults standardUserDefaults] stringForKey:USERDEFAULT_KEY_PASSCODE] != nil &&
        ![[[NSUserDefaults standardUserDefaults] stringForKey:USERDEFAULT_KEY_PASSCODE]  isEqual: @""] &&
        ((AppDelegate*)(UIApplication.sharedApplication.delegate)).isPINInputed == NO) {
        ret = NO;
    }
    return ret;
}

+ (void) displayPINScreen: (UIViewController<TOPasscodeViewControllerDelegate>*)viewController withContext: (LAContext *)authContext {
    
    // Do not display if current view controller is TOPPasscodeViewController
    if ([viewController isKindOfClass:TOPasscodeViewController.class]) {
        return;
    }
    
    TOPasscodeType passcodeType = [[[NSUserDefaults standardUserDefaults] objectForKey:USERDEFAULT_KEY_PASSCODE_TYPE] integerValue];
    TOPasscodeViewController *passcodeViewController = [[TOPasscodeViewController alloc] initWithStyle:TOPasscodeViewStyleOpaqueLight passcodeType:passcodeType];
    passcodeViewController.delegate = viewController;
    
    BOOL faceIDAvailable = NO;
    BOOL biometricsAvailable = [authContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:nil];
    // Show 'Touch ID' button if it's available
    if (@available(iOS 11.0, *)) {
        faceIDAvailable = (authContext.biometryType == LABiometryTypeFaceID);
    }

    passcodeViewController.allowBiometricValidation = biometricsAvailable;
    passcodeViewController.biometryType = faceIDAvailable ? TOPasscodeBiometryTypeFaceID : TOPasscodeBiometryTypeTouchID;
    [viewController presentViewController:passcodeViewController animated:YES completion:nil];
}

@end
