//
//  Pair.m
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "Pair.h"

@implementation Pair

- (instancetype)initWithPairString: (NSString *) pairString {
    NSString *askCurrency = [pairString substringToIndex:3];
    NSString *bidCurrency = [pairString substringFromIndex:4];
    self.askCurrency = askCurrency;
    self.bidCurrency = bidCurrency;
    return self;
}

@end
