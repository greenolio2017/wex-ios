//
//  TradeInfoViewController.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/16/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "FooterViewCell.h"

@interface FooterViewCell()

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@end

@implementation FooterViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.exclusiveTouch = YES;
    //self.lblTitle.text = NSLocalizedString(@"Copyright © 2017 GreenOlio", nil);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
