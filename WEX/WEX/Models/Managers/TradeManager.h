//
//  TradeManager.h
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/22.
//  Copyright © 2017年 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TradeApiProtocol.h"
#import "WexApiHandler.h"

@interface TradeManager : NSObject <TradeApiProtocol>

@property WexApiHandler *apiHandler;
@property NSString *currentPair;

@property NSDictionary *info;
@property NSDictionary *funds;
@property NSDictionary *rights;
@property NSDictionary *open_orders;


+ (TradeManager *)sharedManager;

//- (void)setTimerActive: (BOOL) isStart;
- (void)updateAccountInfo;
- (void)sell:(NSString *)pair withRate: (NSString *) rate withAmount: (NSString *) amount;
- (void)buy:(NSString *)pair withRate: (NSString *) rate withAmount: (NSString *) amount;

@end
