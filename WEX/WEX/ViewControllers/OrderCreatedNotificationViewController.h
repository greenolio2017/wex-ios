//
//  OrderCreatedNotificationViewController.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/30/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderCreatedNotificationViewController : UIViewController
@property (nonatomic) NSString *type;
@property (nonatomic) NSString *price;
@property (nonatomic) NSString *amount;
@property (nonatomic) NSString *total;
@end
