//
//  WexNavigationController.m
//  WEX
//
//  Created by Ta Duong Ngoc on 6/8/18.
//  Copyright © 2018 Ta Duong Ngoc. All rights reserved.
//

#import "WexNavigationController.h"

@interface WexNavigationController ()

@end

@implementation WexNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //bar color
    self.navigationBar.barTintColor = [UIColor whiteColor];
    
    // remove line hair
    [self.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.shadowImage=[UIImage new];
    [self.navigationBar setTranslucent:NO];
    [self.navigationBar setOpaque:YES];
    
    // back buttons
    self.navigationBar.backIndicatorImage = [UIImage imageNamed:@"icon_back_left"];
    self.navigationBar.backIndicatorTransitionMaskImage = [UIImage imageNamed:@"icon_back_left"];
    
    // shadow
//    self.navigationBar.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.14] CGColor];
//    self.navigationBar.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
//    self.navigationBar.layer.shadowOpacity = 1.0f;
    // font
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIFont systemFontOfSize:18.0f weight:UIFontWeightBold], NSFontAttributeName, [UIColor blackColor], NSForegroundColorAttributeName, nil];
    self.navigationBar.titleTextAttributes = navbarTitleTextAttributes;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) goPrevious {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
