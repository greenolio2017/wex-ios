//
//  TickerManager.m
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "TickerManager.h"
#import "WexApiHandler.h"
#import "MarketsManager.h"
#import "Const.h"
#import "DialogUtil.h"

@implementation TickerManager

{
    WexApiHandler *apiHandler;
}

+ (TickerManager*)sharedManager {
    static TickerManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


// Initialization

- (id)init {
    self = [super init];
    if (self) {
        apiHandler = [[WexApiHandler alloc] init];
    }
    
    //self.pairArrayString = [NSArray arrayWithObjects:PAIRS count:GET_ARRAY_SIZE(PAIRS)];
    self.pairArrayString = [[NSMutableArray alloc] init];
    for (MarketInfo *pair in [MarketsManager sharedManager].marketInfoArray) {
        [self.pairArrayString addObject:pair.pairString];
    }
    
    self.pairArray = [[NSMutableArray alloc] init];
    self.tickerInfoArray = [[NSMutableArray alloc] init];
    
    for (NSString *pairString in self.pairArrayString) {
        
        Pair * pair = [[Pair alloc] initWithPairString: pairString];
        [self.pairArray addObject:pair];
        
        TickerInfo *tickerInfo = [[TickerInfo alloc] initWithPairString:pairString high:0 low:0 avg:0 vol:0 vol_cur:0 last:0 buy:0 sell:0 updated:0];
        [self.tickerInfoArray addObject:tickerInfo];
    }
    
    self.isLoading = NO;
    
    return self;
}

- (void)setTimerActive: (BOOL) isStart {
    if (isStart){
        self.tracker = [NSTimer scheduledTimerWithTimeInterval:2.0f
                                                   target:self
                                                 selector:@selector(updateAllTickers)
                                                 userInfo:nil repeats:YES];
        
    } else {
        [self.tracker invalidate];
    }
}

//- (void)update {
//    for (NSString *pairString in self.pairArrayString) {
//
//        [self updateTicker: pairString];
//    }
//}
//
//- (void)updateTicker: (NSString *) pairString {
//    dispatch_async(kBgQueue, ^{
//        NSString * urlString = [[WEX_TICKER_URL stringByAppendingString:pairString] stringByAppendingString:@"?ignore_invalid=1&limit=50"];
//        [apiHandler getResponseFromPublicServerUrl: urlString withHandler:^(NSData *response) {
//            NSError* error;
//            NSDictionary* json = [NSJSONSerialization
//                                  JSONObjectWithData:response
//                                  options:kNilOptions
//                                  error:&error];
//
//            if (error) {
//                [DialogUtil showDialogWithError:@"Error" withError:error.domain withButtonName:@"OK" buttonHandler:^{
//                    [DialogUtil dismissDialog];
//                }];
//                [self updateTicker:pairString];
//            }
//            else {
//                [self updateTickerInfoWithJson: pairString withJson: json];
//            }
//        }];
//    });
//}

- (void)updateAllTickers{
    // start timer if it is invalidate
    if (![self.tracker isValid]) {
        [self setTimerActive:YES];
    }
    
    // do not call API if it is waiting
    if (self.isLoading) {
        return;
    }
    
    dispatch_async(kBgQueue, ^{
        NSString * urlString = WEX_TICKER_URL;
        for (NSString *pairString in self.pairArrayString) {
            urlString = [[urlString stringByAppendingString:pairString] stringByAppendingString:@"-"];
        }
        urlString = [urlString stringByAppendingString:@"?ignore_invalid=1&limit=50"];
        [self->apiHandler getResponseFromPublicServerUrl: urlString withHandler:^(NSData *response) {
            
            self.isLoading = NO;
            
            if (response == nil) {
                return;
            }
            
            NSError* error;
            NSDictionary* json = [NSJSONSerialization
                                  JSONObjectWithData:response
                                  options:kNilOptions
                                  error:&error];
            if (error) {
                [DialogUtil showDialogWithError:@"Error" withError:error.domain withButtonName:@"OK" buttonHandler:^{
                    [DialogUtil dismissDialog];
                }];
                //[self updateAllTickers];
            }
            else {
                [self updateAllTickersInfoWithJson: json];
            }
        }];
        
    });
}

//- (void)updateTickerInfoWithJson: (NSString *)pairString withJson: (NSDictionary *) json {
//
//    NSDictionary* ticker = [json objectForKey:pairString];
//    NSNumber * high = [ticker objectForKey:@"high"];
//    NSNumber * low = [ticker objectForKey:@"low"];
//    NSNumber * avg = [ticker objectForKey:@"avg"];
//    NSNumber * vol = [ticker objectForKey:@"vol"];
//    NSNumber * vol_cur = [ticker objectForKey:@"vol_cur"];
//    NSNumber * last = [ticker objectForKey:@"last"];
//    NSNumber * buy = [ticker objectForKey:@"buy"];
//    NSNumber * sell = [ticker objectForKey:@"sell"];
//    NSNumber * updated = [ticker objectForKey:@"updated"];
//
//    for (int i = 0; i < [self.tickerInfoArray count]; i++) {
//        if ([[self.tickerInfoArray objectAtIndex:i].pairString isEqualToString:pairString]) {
//            [[self.tickerInfoArray objectAtIndex:i] updateTickerInfo:high low:low avg:avg vol:vol vol_cur:vol_cur last:last buy:buy sell:sell updated:updated];
//        }
//    }
//}

- (void)updateAllTickersInfoWithJson: (NSDictionary *) json {
    
    NSMutableArray<TickerInfo *> *tickerInfoArray = [[NSMutableArray alloc] initWithArray:self.tickerInfoArray];
    
    for (int i = 0; i < [tickerInfoArray count]; i++) {
        NSDictionary* ticker = [json objectForKey:[tickerInfoArray objectAtIndex:i].pairString];
        NSNumber *high = [ticker objectForKey:@"high"];
        NSNumber *low = [ticker objectForKey:@"low"];
        NSNumber *avg = [ticker objectForKey:@"avg"];
        NSNumber *vol = [ticker objectForKey:@"vol"];
        NSNumber *vol_cur = [ticker objectForKey:@"vol_cur"];
        NSNumber *last = [ticker objectForKey:@"last"];
        NSNumber *buy = [ticker objectForKey:@"buy"];
        NSNumber *sell = [ticker objectForKey:@"sell"];
        NSNumber *updated = [ticker objectForKey:@"updated"];
        
        [[tickerInfoArray objectAtIndex:i] updateTickerInfo:high low:low avg:avg vol:vol vol_cur:vol_cur last:last buy:buy sell:sell updated:updated];
    }
    
    self.tickerInfoArray = tickerInfoArray;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:
         TICKER_MANAGER_UPDATE_TICKERS object:nil userInfo:nil];
    });
}

- (TickerInfo* )getTickerInfo: (NSString *) pairString {
    
    NSMutableArray<TickerInfo *> *tickerInfoArray = [[NSMutableArray alloc] initWithArray:self.tickerInfoArray];
    
    TickerInfo *tickerInfo = [[TickerInfo alloc] init];
    
    for (int i = 0; i < [tickerInfoArray count]; i++) {
        
        TickerInfo *tmp = [tickerInfoArray objectAtIndex:i];
        
        if ([tmp.pairString isEqualToString:pairString]) {
            tickerInfo = tmp;
            break;
        }
    }
    return tickerInfo;
}

@end
