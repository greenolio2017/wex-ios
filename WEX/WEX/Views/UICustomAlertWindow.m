//
//  UICustomAlertViewController.m
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/27.
//  Copyright © 2017年 Ta Duong Ngoc. All rights reserved.
//

#import "UICustomAlertWindow.h"

#import "LoggingCommon.h"
#import "Const.h"

#pragma mark - UICustomAlertViewController

@interface UICustomAlertViewController : UIViewController

@end

@implementation UICustomAlertViewController


- (id)init {
    self = [super init];
    if (self) {
        CGSize size = [UIScreen mainScreen].bounds.size;
        self.view.frame = CGRectMake(0.0f, 0.0f, size.width, size.height);
        self.view.backgroundColor = [UIColor clearColor];
    }
    return self;
}

@end

#pragma mark - UICustomAlertWindow

@interface UICustomAlertWindow ()

@property (nonatomic) NSInteger buttonIndex;
@property (nonatomic) UIAlertController *alert;
@property (nonatomic) UICustomAlertViewController *rootView;
@property (nonatomic) BOOL shown;
@property (nonatomic) UIAlertAction *cancelBtnAction;
@property (nonatomic) UIAlertAction *otherBtnAction;
@property (strong, nonatomic) UIWindow *baseKeyWindow;

@end

@implementation UICustomAlertWindow {
    UIAlertButtonHandler cancelListener;
    UIAlertButtonHandler otherListener;
}

- (id)initWithTitle:(NSString *)title
              message:(NSString *)message
    cancelButtonTitle:(NSString *)cancelButtonTitle
      cancelHandler:(void (^)(void))cancelHandler
     otherButtonTitle:(NSString *)otherButtonTitle
       otherHandler:(void (^)(void))otherHandler
    windowLevelOffset:(NSInteger const)windowLevelOffset {
    CGSize size = [UIScreen mainScreen].bounds.size;
    self = [super initWithFrame:CGRectMake(0.0f, 0.0f, size.width, size.height)];
    if (self) {

        @try {
            self.backgroundColor = [UIColor clearColor];
            self.windowLevel = UIWindowLevelNormal + windowLevelOffset;
            self.hidden = YES;
            self.alpha = 0.0f;

            self.buttonIndex = 0;
            self.tag = -1;
            self.shown = NO;
            self.minTextSize = 0;
            self.maxTextSize = 0;
            self.textField = nil;
            
            self.rootView = [[UICustomAlertViewController alloc] init];
            self.rootViewController = self.rootView;

            cancelListener = cancelHandler;
            otherListener = otherHandler;

            self.alert = [UIAlertController alertControllerWithTitle:(title ? title : @"")message:message preferredStyle:UIAlertControllerStyleAlert];
            self.alert.view.tintColor = COLOR_BLUE;

            if (cancelButtonTitle && ![cancelButtonTitle isEqualToString:@""]) {
                self.cancelBtnAction = [UIAlertAction actionWithTitle:cancelButtonTitle
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction *action) {
                                                                  self.buttonIndex = 0;
                                                                  if (self->cancelListener) {
                                                                      self->cancelListener();
                                                                  }
                                                                  self->cancelListener = nil;
                                                                  self->otherListener = nil;
                                                              }];
                [self.alert addAction:self.cancelBtnAction];
            }
            if (otherButtonTitle && ![otherButtonTitle isEqualToString:@""]) {
                self.otherBtnAction = [UIAlertAction actionWithTitle:otherButtonTitle
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction *action) {
                                                                 self.buttonIndex = 1;
                                                                 if (self->otherListener) {
                                                                     self->otherListener();
                                                                 }
                                                                 self->cancelListener = nil;
                                                                 self->otherListener = nil;
                                                             }];
                [self.alert addAction:self.otherBtnAction];
            }
        } @catch (NSException *exception) {
            LogError(@"exception : %@", exception);
        }
    }
    return self;
}


- (void)setTextField:(BOOL)mask placeHolder:(NSString *)placeHolder {

    if (self.isAppearOtherBtn && self.minTextSize > 0) {
        self.otherBtnAction.enabled = NO;
    } else {
        self.otherBtnAction.enabled = YES;
    }

    __weak typeof(self) weakSelf = self;

    [self.alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.delegate = weakSelf;
        textField.placeholder = placeHolder;
        textField.secureTextEntry = mask;

        weakSelf.textField = textField;
    }];
}


- (void)show {
    @try {
        self.hidden = NO;
        self.alpha = 1.0f;

        self.baseKeyWindow = [UIApplication sharedApplication].keyWindow;

        [self makeKeyAndVisible];

        if (self.alert) {
            [self.rootView presentViewController:self.alert animated:YES completion:nil];
            self.shown = YES;
        }
    } @catch (NSException *exception) {
        LogError(@"exception : %@", exception);
    }
}

- (void)dismiss:(BOOL)animated {
    self.maxTextSize = 0;
    self.minTextSize = 0;
    self.appearOtherBtn = NO;
    self.otherBtnAction = nil;
    self.cancelBtnAction = nil;

    @try {
        if (self.textField) {
            [self.textField resignFirstResponder];
            self.textField = nil;
        }
        if (self.alert) {
            [self.alert dismissViewControllerAnimated:animated completion:nil];
        }

        [self closeWindow];
    } @catch (NSException *exception) {
        LogError(@"exception : %@", exception);
    }
}

- (void)cancel {
    if (cancelListener) {
        cancelListener();
    }
    cancelListener = nil;
    otherListener = nil;
}

- (BOOL)isShown {
    return self.alert && self.shown;
}


- (void)closeWindow {
    self.hidden = YES;
    self.alpha = 0.0f;
    self.rootViewController = nil;

    self.alert = nil;

    self.shown = NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSMutableString *str = [textField.text mutableCopy];
    [str replaceCharactersInRange:range withString:string];
    NSInteger byteLength = [str lengthOfBytesUsingEncoding:kCFStringEncodingUTF8];

    if (self.isAppearOtherBtn) {
        if (self.otherBtnAction != nil && byteLength < self.minTextSize) {
            self.otherBtnAction.enabled = NO;
        } else {
            self.otherBtnAction.enabled = YES;
        }
    } else {
        self.otherBtnAction.enabled = YES;
    }

    if (byteLength <= self.maxTextSize) {
        return YES;
    }
    else {
        return NO;
    }
}

@end
