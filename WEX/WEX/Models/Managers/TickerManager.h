//
//  TickerManager.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pair.h"
#import "TickerInfo.h"

@interface TickerManager : NSObject
@property NSMutableArray<NSString *> *pairArrayString;
@property NSMutableArray<Pair *> *pairArray;
@property NSMutableArray<TickerInfo *> *tickerInfoArray;
@property BOOL isLoading;
@property NSTimer *tracker;

+ (TickerManager *)sharedManager;

//- (void)updateTicker: (NSString *) pairString;
- (void)updateAllTickers;

- (TickerInfo *)getTickerInfo: (NSString *) pairString;

- (void)setTimerActive: (BOOL) isStart;
//- (void)setTimerActive: (BOOL) isStart;

@end
