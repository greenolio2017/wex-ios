//
//  ActiveOrderTableViewCell.m
//  WEX
//
//  Created by Ta Duong Ngoc on 11/16/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "ActiveOrderTableViewCell.h"
#import "OrderManager.h"
#import "OrderInfo.h"
#import "ProgressDialogUtil.h"
@interface ActiveOrderTableViewCell ()


@end
@implementation ActiveOrderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    //[super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)detailButtonTouchUpInside:(id)sender {
    
}

- (IBAction)cancelButtonTouchUpInside:(id)sender {
    [[ProgressDialogUtil sharedManager] showLoading:(UITableView *)self.superview.superview.superview alpha:0.2f];
    OrderInfo *orderInfo = [[OrderManager sharedManager].activeOrders objectAtIndex:self.indexPathCell.row];
    [[OrderManager sharedManager] cancelOrder:orderInfo.orderId];
}


@end
