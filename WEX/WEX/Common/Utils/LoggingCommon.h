//
//  LoggingCommon.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/14/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#ifndef LoggingCommon_h
#define LoggingCommon_h

#define LOGGING_ENABLED     0


#ifdef DEBUG
#	define LOGGING_LEVEL_ERROR		1
#	define LOGGING_LEVEL_INFO		1
#	define LOGGING_LEVEL_WARNING	1
#	define LOGGING_LEVEL_TRACE		1
#	define LOGGING_LEVEL_DEBUG		1
#	define LOGGING_LEVEL_TIME		0
#else
#	define LOGGING_LEVEL_ERROR		1
#	define LOGGING_LEVEL_INFO		1
#	define LOGGING_LEVEL_WARNING	0
#	define LOGGING_LEVEL_TRACE		0
#	define LOGGING_LEVEL_DEBUG		0
#	define LOGGING_LEVEL_TIME		0
#endif


#ifdef DEBUG
#   define LOGGING_INCLUDE_CODE_LOCATION    1
#else
#	define LOGGING_INCLUDE_CODE_LOCATION    0
#endif



#if !(defined(LOGGING_ENABLED) && LOGGING_ENABLED)
#undef LOGGING_LEVEL_ERROR
#undef LOGGING_LEVEL_INFO
#undef LOGGING_LEVEL_WARNING
#undef LOGGING_LEVEL_TRACE
#undef LOGGING_LEVEL_DEBUG
#undef LOGGING_LEVEL_TIME
#endif

#define LOG_FORMAT_NO_LOCATION(fmt, lvl, ...) NSLog((@"[%@] " fmt), lvl, ##__VA_ARGS__)
#define LOG_FORMAT_WITH_LOCATION(fmt, lvl, ...) NSLog((@"[%@]%s[Line %d] " fmt), lvl, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)

#if defined(LOGGING_INCLUDE_CODE_LOCATION) && LOGGING_INCLUDE_CODE_LOCATION
#define LOG_FORMAT(fmt, lvl, ...) LOG_FORMAT_WITH_LOCATION(fmt, lvl, ##__VA_ARGS__)
#else
#define LOG_FORMAT(fmt, lvl, ...) LOG_FORMAT_NO_LOCATION(fmt, lvl, ##__VA_ARGS__)
#endif


#if defined(LOGGING_LEVEL_ERROR) && LOGGING_LEVEL_ERROR
#define LogError(fmt, ...) LOG_FORMAT(fmt, @"ERROR", ##__VA_ARGS__)
#else
#define LogError(...)
#endif


#if defined(LOGGING_LEVEL_INFO) && LOGGING_LEVEL_INFO
#define LogInfo(fmt, ...) LOG_FORMAT(fmt, @"INFO", ##__VA_ARGS__)
#else
#define LogInfo(...)
#endif


#if defined(LOGGING_LEVEL_WARNING) && LOGGING_LEVEL_WARNING
#define LogWarning(fmt, ...) LOG_FORMAT(fmt, @"WARNING", ##__VA_ARGS__)
#else
#define LogWarning(...)
#endif


#if defined(LOGGING_LEVEL_TRACE) && LOGGING_LEVEL_TRACE
#define LogTrace(fmt, ...) LOG_FORMAT(fmt, @"TRACE", ##__VA_ARGS__)
#else
#define LogTrace(...)
#endif


#if defined(LOGGING_LEVEL_DEBUG) && LOGGING_LEVEL_DEBUG
#define LogDebug(fmt, ...) LOG_FORMAT(fmt, @"DEBUG", ##__VA_ARGS__)
#else
#define LogDebug(...)
#endif


#if defined(LOGGING_LEVEL_TIME) && LOGGING_LEVEL_TIME
#define LogTime(fmt, ...) LOG_FORMAT(fmt, @"TIME", ##__VA_ARGS__); \
    NSLog(@"[TIME] epoch Time : %f [msec]", ([[NSDate date] timeIntervalSince1970] * 1000))
#else
#define LogTime(...)
#endif


#if defined(LOGGING_LEVEL_DEBUG) && LOGGING_LEVEL_DEBUG
#define LogViewHierarchy(v) [DebugLogView showViews:v]
#define LogWindows [DebugLogView showWindows]
#define LogWindowsAndViewH [DebugLogView showWindowsAndViews]
#define LogWindowViewH(w) [DebugLogView showWindowViews:w]
#else
#define LogViews(v)
#define LogWindows
#define LogWindowsAndViewH
#define LogWindowViewH(w)
#endif

#define ClassName(a)    NSStringFromClass([a class])
#define NilCheck(a)     (nil==a?@"nil":@"not nil")
#define BoolCheck(a)     (a?@"YES":@"NO")

#endif
