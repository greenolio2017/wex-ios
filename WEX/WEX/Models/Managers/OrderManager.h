//
//  OrderManager.h
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/29.
//  Copyright © 2017年 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderApiProtocol.h"
#import "WexApiHandler.h"

@interface OrderManager : NSObject <OrderApiProtocol>
@property NSString *currentPair;
@property NSMutableArray *activeOrders;
@property WexApiHandler *apiHandler;
@property NSMutableArray *ordersInfo;

+ (OrderManager *)sharedManager;

//- (void)update;
//- (void)setTimerActive: (BOOL) isStart;
- (void)updateActiveOrders;
- (void)updateOrderInfo: (NSString *) orderId;
- (void)cancelOrder: (NSString *) orderId;
- (void)cancelOrders: (NSArray<NSString*> *) orderIds;

@end
