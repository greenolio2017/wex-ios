//
//  MarketsManager.h
//  WEX
//
//  Created by Ta Duong Ngoc on 11/9/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pair.h"
#import "MarketInfo.h"

@protocol MarketsManagerDelegate <NSObject>

@required
- (void)marketsUpdated;

@end

@interface MarketsManager : NSObject
@property NSMutableArray<MarketInfo *> *marketInfoArray;
@property (nonatomic, weak) id<MarketsManagerDelegate> delegate;

+ (MarketsManager *)sharedManager;
- (void)updateAllMarkets;

@end
