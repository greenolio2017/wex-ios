//
//  FundsViewController.h
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/29.
//  Copyright © 2017年 Ta Duong Ngoc. All rights reserved.
//

#import "FundsViewController.h"
#import "FundsTableViewCell.h"
#import "TradeManager.h"
#import "CommonUtil.h"
#import "Const.h"
#import "ProgressDialogUtil.h"

@interface FundsViewController ()
@property (weak, nonatomic) IBOutlet UITableView *fundListTableView;
@property (weak, nonatomic) IBOutlet UILabel *noBalanceLabel;
@property NSMutableDictionary *funds;

@end

@implementation FundsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFunds) name:TRADE_INFO_UPDATE object:nil];
    
    // funds init
    self.funds = [[NSMutableDictionary alloc] init];
    
    // tableView setup
    [self.fundListTableView registerNib:[UINib nibWithNibName:@"FundsTableViewCell" bundle:nil] forCellReuseIdentifier:@"FundsTableViewCell"];
    self.fundListTableView.tableFooterView = [[UIView alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[ProgressDialogUtil sharedManager] showLoading:self.view alpha:0.0f];
    [[TradeManager sharedManager] updateAccountInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)updateFunds {
    
    NSDictionary *funds = [TradeManager sharedManager].funds;
    NSArray *keys = [[TradeManager sharedManager].funds allKeys];
    
    self.funds = [[NSMutableDictionary alloc] init];
    for (int i = 0; i < [funds count]; i++) {
        NSString *key = [keys objectAtIndex:i];
        NSString *amount = [CommonUtil dealWithDoubleWithEightDecimal:[[[TradeManager sharedManager].funds objectForKey:key] doubleValue]];
        if (![amount isEqual: @"0"]) {
            [self.funds setObject:amount forKey:key];
        }
    }
    
    if ([self.funds count] <= 0) {
        self.fundListTableView.hidden = YES;
        self.noBalanceLabel.hidden = NO;
    }
    else {
        self.fundListTableView.hidden = NO;
        self.noBalanceLabel.hidden = YES;
    }
    [self.fundListTableView reloadData];
}


#pragma mark <UITableViewDelegate>
/**
 ----------------------------------------------------------------------------------------------------
 * @brief heightForRowAtIndexPath
 ----------------------------------------------------------------------------------------------------
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

/**
 ----------------------------------------------------------------------------------------------------
 * @brief didSelectRowAtIndexPath
 ----------------------------------------------------------------------------------------------------
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark <UITableViewDataSource>
/**
 ----------------------------------------------------------------------------------------------------
 * @brief numberOfSectionsInTableView
 ----------------------------------------------------------------------------------------------------
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


/**
 ----------------------------------------------------------------------------------------------------
 * @brief numberOfRowsInSection
 ----------------------------------------------------------------------------------------------------
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.funds count];
}


/**
 ----------------------------------------------------------------------------------------------------
 * @brief cellForRowAtIndexPath
 ----------------------------------------------------------------------------------------------------
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FundsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FundsTableViewCell"];
    if (cell == nil) {
        cell = [[FundsTableViewCell alloc] init];
    }
    
    if ([self.funds count] <= 0) {
        return cell;
    }
    
    NSArray *keys = [self.funds allKeys];
    NSString *key = [keys objectAtIndex:indexPath.row];
    cell.lblPair.text = [key uppercaseString];
    cell.lblAmount.text = [CommonUtil dealWithDoubleWithEightDecimal:[[self.funds objectForKey:key] doubleValue]];
    return cell;
}

- (IBAction)backButtonTouchUpInside:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
