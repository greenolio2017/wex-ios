//
//  TradesInfo.m
//  WEX
//
//  Created by Ta Duong Ngoc on 11/19/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import "TradesInfo.h"

@implementation TradesInfo
- (instancetype)initWith: (NSString *) type
                              price: (NSNumber *) price
                               amount: (NSNumber *) amount
                               tid: (NSNumber *) tid
                               timestamp: (NSNumber *) timestamp {
    self.type  = type;
    self.price  = price;
    self.amount  = amount;
    self.tid  = tid;
    self.timestamp  = timestamp;
    
    return self;
}
@end
