//
//  MarketItemTableViewCell.h
//  BittrexTrade
//
//  Created by Ta Duong Ngoc on 11/16/17.
//  Copyright © 2017 Ta Duong Ngoc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RedGreenValueChangeLabel.h"

@interface MarketItemTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *marketCurrency;

@property (weak, nonatomic) IBOutlet UILabel *baseCurrency;

@property (weak, nonatomic) IBOutlet UILabel *volumnLabel;

@property (weak, nonatomic) IBOutlet RedGreenValueChangeLabel *lastPriceLabel;

@property (weak, nonatomic) IBOutlet UILabel *lowLabel;

@property (weak, nonatomic) IBOutlet UILabel *highLabel;

@property (weak, nonatomic) IBOutlet RedGreenValueChangeLabel *changePrevLabel;

@property (weak, nonatomic) IBOutlet UIImageView *favoriteImage;
@property (nonatomic) NSString *pair;

@end
