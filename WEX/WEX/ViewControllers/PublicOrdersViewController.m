//
//  PublicOrdersViewController.h
//  WEX
//
//  Created by Ta Duong Ngoc on 2017/11/29.
//  Copyright © 2017年 Ta Duong Ngoc. All rights reserved.
//

#import "PublicOrdersViewController.h"
#import "PublicOrdersTableViewCell.h"
#import "CommonUtil.h"
#import "Const.h"
#import "ProgressDialogUtil.h"
#import "DepthManager.h"
#import "LoggingCommon.h"

@interface PublicOrdersViewController () <PublicOrdersTableViewCellDelegate>
@property (weak, nonatomic) IBOutlet UITableView *publicOrdersTableView;
@property NSMutableArray *depthAsks;
@property double maxDepthAskAmount;
@property NSMutableArray *depthBids;
@property double maxDepthBidAmount;
@property NSMutableArray *tradesInfo;

@end

@implementation PublicOrdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateCurrentPair:) name:PAIRS_CHANGED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDepth) name:DEPTH_MANAGER_UPDATE_DEPTH object:nil];
    
    // tableView setup
    [self.publicOrdersTableView registerNib:[UINib nibWithNibName:@"PublicOrdersTableViewCell" bundle:nil] forCellReuseIdentifier:@"PublicOrdersTableViewCell"];
    self.publicOrdersTableView.tableFooterView = [[UIView alloc] init];
    
    // Init data
    [self updateDepth];
    
    [[ProgressDialogUtil sharedManager] showLoading:self.view alpha:0.2f];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) updateCurrentPair:(NSNotification *) notification
{
    LogTrace("IN");
    [self.publicOrdersTableView setContentOffset:CGPointZero animated:YES];
    LogTrace("OUT");
}

-(void)updateDepth{
    LogTrace("IN");
    dispatch_async(dispatch_get_main_queue(), ^{
        self.depthAsks = [[NSMutableArray alloc] initWithArray:[DepthManager sharedManager].depthInfo.asks];
        self.depthBids = [[NSMutableArray alloc] initWithArray:[DepthManager sharedManager].depthInfo.bids];
//        if ([self.depthAsks count] > 0  [self.depthBids count] > 0) {
            [self getMaxDepthAmount: @"maxAskAmount"];
            [[ProgressDialogUtil sharedManager] hideLoading];
            [self.publicOrdersTableView reloadData];
//        }
    });
    
    LogTrace("OUT");
}

- (void) getMaxDepthAmount: (NSString *) type {
    if ([type isEqualToString:@"maxAskAmount"]) {
        self.maxDepthAskAmount = -MAXFLOAT;
        
        if (self.depthAsks != nil && [self.depthAsks count] > 0) {
            for (NSInteger i = 0; i < self.depthAsks.count; i++) {
                double amount = [[[self.depthAsks objectAtIndex:i] objectAtIndex:1] doubleValue];
                if (amount > self.maxDepthAskAmount) self.maxDepthAskAmount = amount;
            }
        }
    }
    
    else if ([type isEqualToString:@"maxBidAmount"]) {
        self.maxDepthBidAmount = -MAXFLOAT;
        
        if (self.depthBids != nil && [self.depthBids count] > 0) {
            for (NSInteger i = 0; i< self.depthBids.count; i++) {
                double amount = [[[self.depthBids objectAtIndex:i] objectAtIndex:1] doubleValue];
                if (amount > self.maxDepthBidAmount) self.maxDepthBidAmount = amount;
            }
        }
    }
}

#pragma mark <UITableViewDelegate>
/**
 ----------------------------------------------------------------------------------------------------
 * @brief heightForRowAtIndexPath
 ----------------------------------------------------------------------------------------------------
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 36;
}

/**
 ----------------------------------------------------------------------------------------------------
 * @brief didSelectRowAtIndexPath
 ----------------------------------------------------------------------------------------------------
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Do nothing
}

#pragma mark <UITableViewDataSource>
/**
 ----------------------------------------------------------------------------------------------------
 * @brief numberOfSectionsInTableView
 ----------------------------------------------------------------------------------------------------
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


/**
 ----------------------------------------------------------------------------------------------------
 * @brief numberOfRowsInSection
 ----------------------------------------------------------------------------------------------------
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 50;
}


/**
 ----------------------------------------------------------------------------------------------------
 * @brief cellForRowAtIndexPath
 ----------------------------------------------------------------------------------------------------
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PublicOrdersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PublicOrdersTableViewCell"];
    if (cell == nil) {
        cell = [[PublicOrdersTableViewCell alloc] init];
    }
    if (self.depthAsks != nil && [self.depthAsks count] > indexPath.row) {
        double sellPrice = [[[self.depthAsks objectAtIndex:indexPath.row] objectAtIndex:0] doubleValue];
        double sellAmount = [[[self.depthAsks objectAtIndex:indexPath.row] objectAtIndex:1] doubleValue];
        cell.sellPriceLabel.text = [CommonUtil dealWithDoubleWithFiveDecimal:sellPrice];
        cell.sellAmountLabel.text = [CommonUtil dealWithDoubleWithEightDecimal:sellAmount];
    }
    else {
        cell.sellPriceLabel.text = @"";
        cell.sellAmountLabel.text = @"";
    }
    
    if (self.depthBids != nil && [self.depthBids count] > indexPath.row) {
        double buyPrice = [[[self.depthBids objectAtIndex:indexPath.row] objectAtIndex:0] doubleValue];
        double buyAmount = [[[self.depthBids objectAtIndex:indexPath.row] objectAtIndex:1] doubleValue];
        cell.buyPriceLabel.text = [CommonUtil dealWithDoubleWithFiveDecimal:buyPrice];
        cell.buyAmountLabel.text = [CommonUtil dealWithDoubleWithEightDecimal:buyAmount];
    }
    else {

        cell.buyPriceLabel.text = @"";
        cell.buyAmountLabel.text = @"";
    }
    
    cell.delegate = self;
    return cell;
}

#pragma mark - Section Public Order button delegate
- (void) buySectionTouchedUp:(UITableViewCell *)cell {
    NSIndexPath *indexPath = [self.publicOrdersTableView indexPathForCell:cell];

    if ([self.depthBids count] < indexPath.row + 1) {
        return;
    }
    
    // calculate price
    double buyPrice = [[[self.depthBids objectAtIndex:indexPath.row] objectAtIndex:0] doubleValue];
    // calculate sum amount
    double buyAmount = 0;
    for (int i = 0; i <= indexPath.row; i++) {
        if ([self.depthBids count] >= i) {
            buyAmount += [[[self.depthBids objectAtIndex:i] objectAtIndex:1] doubleValue];
        }
    }
    if (self.delegate) {
        [self.delegate buySectionTouchedUp:[CommonUtil dealWithDoubleWithFiveDecimal:buyPrice] andAmount:[CommonUtil dealWithDoubleWithEightDecimal:buyAmount]];
    }
    
}
- (void) sellSectionTouchedUp:(UITableViewCell *)cell {
    NSIndexPath *indexPath = [self.publicOrdersTableView indexPathForCell:cell];
    
    if ([self.depthAsks count] < indexPath.row + 1) {
        return;
    }
    
    // calculate price
    double sellPrice = [[[self.depthAsks objectAtIndex:indexPath.row] objectAtIndex:0] doubleValue];
    // calculate sum amount
    double sellAmount = 0;
    for (int i = 0; i <= indexPath.row; i++) {
        if ([self.depthAsks count] >= i) {
            sellAmount += [[[self.depthAsks objectAtIndex:i] objectAtIndex:1] doubleValue];
        }
    }
    if (self.delegate) {
        [self.delegate sellSectionTouchedUp:[CommonUtil dealWithDoubleWithFiveDecimal:sellPrice] andAmount:[CommonUtil dealWithDoubleWithEightDecimal:sellAmount]];
    }
}

@end
